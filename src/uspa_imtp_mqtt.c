/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <imtp/imtp_message.h>
#include <imtp/imtp_connection.h>
#include <uspi/uspi_connection.h>

#include "uspa_imtp.h"
#include "uspa_imtp_mqtt.h"
#include "uspa_msghandler.h"
#include "uspa_controller_mtp_mqtt.h"

/* Unpacks the incoming tlv in a tlv_protobuf and optional response-topic. The topic is only
 * specified if the MQTT client already had this information.
 */
static int unpack_frame(imtp_frame_t* frame, char** topic, const imtp_tlv_t** protobuf) {
    int retval = -1;
    const imtp_tlv_t* tlv_next = NULL;

    tlv_next = imtp_frame_get_first_tlv(frame, imtp_tlv_type_topic);
    if(tlv_next != NULL) {
        (*topic) = (char*) calloc(1, tlv_next->length + 1);
        when_null(*topic, exit);
        strncpy((*topic), (char*) tlv_next->value + tlv_next->offset, tlv_next->length);
    } else {
        SAH_TRACEZ_NOTICE(ME, "Did not find topic in TLV chain");
    }

    tlv_next = imtp_frame_get_first_tlv(frame, imtp_tlv_type_protobuf_bytes);
    if(tlv_next != NULL) {
        *protobuf = tlv_next;
    } else {
        SAH_TRACEZ_WARNING(ME, "Did not find protobuf in TLV chain");
    }

    tlv_next = imtp_frame_get_first_tlv(frame, imtp_tlv_type_handshake);
    if(tlv_next != NULL) {
        SAH_TRACEZ_INFO(ME, "Received TLV of type handshake on MQTT connection, ignoring");
    }

    retval = 0;
exit:
    return retval;
}

static void uspa_imtp_add_response_topic(amxc_var_t* props, uspa_mtp_mqtt_t* mqtt_data) {
    const char* resp_topic = NULL;
    int retval = 0;
    amxc_var_t ret;
    amxc_string_t obj_path;

    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);

    when_null(mqtt_data, exit);

    if(mqtt_data->response_topic != NULL) {
        amxc_var_add_key(cstring_t, props, "response-topic", mqtt_data->response_topic);
        goto exit;
    }

    amxc_string_setf(&obj_path, "LocalAgent.MTP.[MQTT.Reference == '%s'].MQTT.",
                     mqtt_data->reference);

    retval = amxb_get(amxb_be_who_has("LocalAgent."), amxc_string_get(&obj_path, 0), 0, &ret, 5);
    when_failed(retval, exit);

    resp_topic = GETP_CHAR(&ret, "0.0.ResponseTopicDiscovered");
    if((resp_topic == NULL) || (*resp_topic == 0)) {
        resp_topic = GETP_CHAR(&ret, "0.0.ResponseTopicConfigured");
        when_str_empty(resp_topic, exit);
    }
    mqtt_data->response_topic = strdup(resp_topic);
    amxc_var_add_key(cstring_t, props, "response-topic", resp_topic);

exit:
    amxc_string_clean(&obj_path);
    amxc_var_clean(&ret);
}

static void uspa_imtp_add_mqtt_props(imtp_frame_t* frame,
                                     char* to_id,
                                     char* usp_msg_id,
                                     int32_t msg_type,
                                     uspa_mtp_mqtt_t* mqtt_data) {
    imtp_tlv_t* tlv_json = NULL;
    amxc_var_t usp_props;
    amxc_var_t* user_props = NULL;
    amxc_string_t usp_err_id;
    const char* json_str;

    amxc_string_init(&usp_err_id, 0);
    amxc_var_init(&usp_props);
    amxc_var_set_type(&usp_props, AMXC_VAR_ID_HTABLE);
    user_props = amxc_var_add_key(amxc_htable_t, &usp_props, "user-props", NULL);

    amxc_string_setf(&usp_err_id, "%s/%s", to_id, usp_msg_id);
    amxc_var_add_key(cstring_t, user_props, "usp-err-id", amxc_string_get(&usp_err_id, 0));

    if(msg_type == USP__HEADER__MSG_TYPE__ERROR) {
        amxc_var_add_key(cstring_t, &usp_props, "content-type", "application/vnd.bbf.usp.error");
    } else {
        amxc_var_add_key(cstring_t, &usp_props, "content-type", "application/vnd.bbf.usp.msg");
    }
    uspa_imtp_add_response_topic(&usp_props, mqtt_data);

    amxc_var_cast(&usp_props, AMXC_VAR_ID_JSON);
    json_str = amxc_var_constcast(jstring_t, &usp_props);

    imtp_tlv_new(&tlv_json, imtp_tlv_type_mqtt_props, strlen(json_str),
                 (char*) json_str, 0, IMTP_TLV_COPY);
    imtp_frame_tlv_add(frame, tlv_json);

    amxc_string_clean(&usp_err_id);
    amxc_var_clean(&usp_props);
}

void uspa_imtp_mqtt_add_tlv(imtp_frame_t* frame,
                            uspa_mtp_mqtt_t* mqtt_data,
                            uspl_tx_t* usp_tx,
                            amxc_var_t* mtp_info) {
    imtp_tlv_t* tlv_topic = NULL;
    const char* topic = GET_CHAR(mtp_info, "topic");

    imtp_tlv_new(&tlv_topic, imtp_tlv_type_topic, strlen(topic), (char*) topic, 0, IMTP_TLV_COPY);
    imtp_frame_tlv_add(frame, tlv_topic);
    uspa_imtp_add_mqtt_props(frame, usp_tx->rendpoint_id, usp_tx->msg_id,
                             usp_tx->msg_type, mqtt_data);

    SAH_TRACEZ_INFO(ME, "Sending TLV chain to %s with topic: [%s], payloadlen: [%d]",
                    usp_tx->rendpoint_id, topic, (uint32_t) usp_tx->pbuf_len);
}

int uspa_imtp_mqtt_handle_frame(uspi_con_t* con, imtp_frame_t* frame) {
    int retval = -1;
    const imtp_tlv_t* tlv_protobuf = NULL;
    char* topic = NULL;

    retval = unpack_frame(frame, &topic, &tlv_protobuf);
    when_failed(retval, exit);

    if((tlv_protobuf != NULL) && (topic != NULL)) {
        amxc_var_t mtp_info;
        amxc_var_init(&mtp_info);
        amxc_var_set_type(&mtp_info, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &mtp_info, "protocol", "MQTT");
        amxc_var_add_key(cstring_t, &mtp_info, "topic", topic);
        uspa_msghandler_process_binary_record(tlv_protobuf->value + tlv_protobuf->offset,
                                              tlv_protobuf->length,
                                              con,
                                              &mtp_info);
        amxc_var_clean(&mtp_info);
    }

    retval = 0;
exit:
    free(topic);
    return retval;
}
