/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>

#include <uspi/uspi_connection.h>
#include <uspi/uspi_prefix.h>

#include "uspa_mtp_mqtt.h"
#include "uspa_controller.h"
#include "uspa_controller_mtp.h"
#include "uspa_controller_mtp_mqtt.h"
#include "uspa_mtp.h"
#include "uspa_imtp.h"
#include "uspa_imtp_mqtt.h"
#include "uspa_object.h"
#include "uspa_agent.h"
#include "uspa_utils.h"

static void uspa_mtp_mqtt_enable_toggled(UNUSED const char* const sig_name,
                                         const amxc_var_t* const data,
                                         UNUSED void* const priv) {
    char* mtp = NULL;
    const char* client = GET_CHAR(data, "path");
    bool enabled = GETP_BOOL(data, "parameters.Enable.to");

    SAH_TRACEZ_INFO(ME, "Enable of %s is %d", client, enabled);

    mtp = uspa_mtp_mqtt_get_enabled_mtp(client);
    when_str_empty(mtp, exit);

    if(enabled) {
        uspa_mtp_mqtt_configure(mtp);
    } else {
        uspa_mtp_mqtt_disable(mtp);
    }

exit:
    free(mtp);
}

static void uspa_mtp_mqtt_status_changed(UNUSED const char* const sig_name,
                                         const amxc_var_t* const data,
                                         UNUSED void* const priv) {
    char* mtp = NULL;
    const char* client = GET_CHAR(data, "path");
    const char* status = GETP_CHAR(data, "parameters.Status.to");

    SAH_TRACEZ_INFO(ME, "Status of %s is %s", client, status);
    mtp = uspa_mtp_mqtt_get_enabled_mtp(client);
    when_str_empty(mtp, exit);

    if(strcmp(status, "Connected") == 0) {
        uspa_object_update_status(mtp, "Up");
    } else if(strcmp(status, "Disabled") == 0) {
        uspa_object_update_status(mtp, "Down");
    }

exit:
    free(mtp);
}

static int uspa_mtp_mqtt_add_connect_props(amxc_string_t* props_path) {
    int retval = -1;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t path;
    char* eid = NULL;
    const char* props = amxc_string_get(props_path, 0);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    eid = uspa_agent_get_eid();
    when_str_empty(eid, exit);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "Enable", true);
    amxc_var_add_key(cstring_t, &args, "Name", "usp-endpoint-id");
    amxc_var_add_key(cstring_t, &args, "Value", eid);
    amxc_var_add_key(cstring_t, &args, "PacketType", "CONNECT");

    retval = amxb_add(amxb_be_who_has(props), props, 0, NULL, &args, &ret, 5);
    if(retval != 0) {
        SAH_TRACEZ_NOTICE(ME, "Add %s with EndpointID [%s]: failed with error code [%d]. Already exists?",
                          props, eid, retval);
        goto exit;
    }
    SAH_TRACEZ_NOTICE(ME, "Add %s with EndpointID [%s]", props, eid);

exit:
    free(eid);
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    return retval;
}

static amxc_string_t* uspa_mtp_mqtt_set_response_topic(const char* mtp) {
    int retval = 0;
    const char* param = "ResponseTopicConfigured";
    char* resp_topic = NULL;
    char* eid = uspa_agent_get_eid();
    const char* cpath = NULL;
    amxc_var_t ret;
    amxc_var_t args;
    amxc_string_t* topic = NULL;
    amxc_string_t path;

    amxc_string_init(&path, 0);
    amxc_string_new(&topic, 0);
    amxc_var_init(&ret);
    amxc_var_init(&args);

    amxc_string_setf(&path, "%sMQTT.", mtp);
    cpath = amxc_string_get(&path, 0);
    resp_topic = uspa_object_get_value(cstring_t, cpath, param);
    if((resp_topic != NULL) && (*resp_topic != 0)) {
        SAH_TRACEZ_INFO(ME, "%s is already set to %s", param, resp_topic);
        amxc_string_setf(topic, "%s", resp_topic);
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Set %s%s to agent/%s", cpath, param, eid);
    amxc_string_setf(topic, "agent/%s", eid);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, param, amxc_string_get(topic, 0));

    retval = amxb_set(amxb_be_who_has(cpath), cpath, &args, &ret, 5);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "Set %s%s to %s failed with error code [%d]",
                           cpath, param, amxc_string_get(topic, 0), retval);
        goto exit;
    }

exit:
    free(eid);
    free(resp_topic);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&path);
    return topic;
}

static bool uspa_mtp_mqtt_subscription_exists(const char* reference, const char* topic) {
    bool retval = false;
    amxb_bus_ctx_t* bus_ctx = amxb_be_who_has(reference);
    amxc_var_t ret;
    amxc_string_t path;

    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    amxc_string_setf(&path, "%s.Subscription.[Topic=='%s'].", reference, topic);
    amxb_get(bus_ctx, amxc_string_get(&path, 0), 0, &ret, 5);

    if(GETP_ARG(&ret, "0.0") != NULL) {
        retval = true;
    }

    amxc_var_clean(&ret);
    amxc_string_clean(&path);
    return retval;
}

static int uspa_mtp_mqtt_add_subscription(const char* reference, amxc_string_t* topic) {
    int retval = -1;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t* subscription = uspa_utils_add_dot(reference);
    const char* subs = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_append(subscription, "Subscription.", 13);
    subs = amxc_string_get(subscription, 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Topic", amxc_string_get(topic, 0));
    amxc_var_add_key(bool, &args, "Enable", true);

    retval = amxb_add(amxb_be_who_has(subs), subs, 0, NULL, &args, &ret, 5);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "Add %s with topic [%s]: failed with error code [%d]",
                           subs, amxc_string_get(topic, 0), retval);
    }

    amxc_string_delete(&subscription);
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    return retval;
}

static int uspa_mtp_mqtt_set_client_id(const char* reference) {
    int retval = -1;
    amxc_var_t ret;
    amxc_var_t values;
    amxc_string_t* dot_reference = uspa_utils_add_dot(reference);
    char* agent_eid = uspa_agent_get_eid();
    const char* ref = amxc_string_get(dot_reference, 0);

    amxc_var_init(&ret);
    amxc_var_init(&values);

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "ClientID", agent_eid);

    retval = amxb_set(amxb_be_who_has(ref), ref, &values, &ret, 5);

    free(agent_eid);
    amxc_string_delete(&dot_reference);
    amxc_var_clean(&values);
    amxc_var_clean(&ret);

    return retval;
}

static bool uspa_mtp_mqtt_is_connected(const char* reference) {
    bool connected = false;
    const char* status = NULL;
    int retval = -1;
    amxc_string_t* path = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);

    path = uspa_utils_add_dot(reference);
    amxc_string_append(path, "Status", 6);

    retval = amxb_get(amxb_be_who_has(amxc_string_get(path, 0)), amxc_string_get(path, 0), 0, &ret, 5);
    when_failed(retval, exit);

    status = GETP_CHAR(&ret, "0.0.Status");
    when_str_empty(status, exit);

    if(strcmp(status, "Connected") == 0) {
        connected = true;
    }

exit:
    amxc_string_delete(&path);
    amxc_var_clean(&ret);
    return connected;
}

static void uspa_mtp_mqtt_force_reconnect(UNUSED const char* const sig_name,
                                          UNUSED const amxc_var_t* const data,
                                          void* const priv) {
    uspa_mtp_mqtt_t* mtp_mqtt = (uspa_mtp_mqtt_t*) priv;
    amxc_string_t* dotted_ref = uspa_utils_add_dot(mtp_mqtt->reference);
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    SAH_TRACEZ_INFO(ME, "Auto reconnect parameters of %s changed", amxc_string_get(dotted_ref, 0));
    if(amxb_call(amxb_be_who_has(amxc_string_get(dotted_ref, 0)), amxc_string_get(dotted_ref, 0), "ForceReconnect", &args, &ret, 5) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to call %sForceReconnect()", amxc_string_get(dotted_ref, 0));
    }

    amxc_string_delete(&dotted_ref);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static void uspa_mtp_mqtt_auto_reconnect_subscribe(uspa_mtp_mqtt_t* mtp_mqtt) {
    amxc_string_t expr;
    amxc_string_t* dotted_ref = uspa_utils_add_dot(mtp_mqtt->reference);
    SAH_TRACEZ_INFO(ME, "Enable auto reconnect for %s", amxc_string_get(dotted_ref, 0));

    amxc_string_init(&expr, 0);
    amxc_string_setf(&expr, "path == '%s' && \
                             (contains('parameters.BrokerAddress') || \
                             contains('parameters.BrokerPort') || \
                             contains('parameters.TransportProtocol') || \
                             contains('parameters.ProtocolVersion') || \
                             contains('parameters.WillMessageExpiryInterval') || \
                             contains('parameters.Username') || \
                             contains('parameters.Password')) && \
                             !(contains('parameters.Enable'))",
                     amxc_string_get(dotted_ref, 0));


    amxb_subscribe(amxb_be_who_has(amxc_string_get(dotted_ref, 0)), amxc_string_get(dotted_ref, 0), amxc_string_get(&expr, 0),
                   uspa_mtp_mqtt_force_reconnect, mtp_mqtt);

    amxc_string_delete(&dotted_ref);
    amxc_string_clean(&expr);
}

static void uspa_mtp_mqtt_auto_reconnect_unsubscribe(uspa_mtp_mqtt_t* mtp_mqtt) {
    amxc_string_t* dotted_ref = uspa_utils_add_dot(mtp_mqtt->reference);

    SAH_TRACEZ_INFO(ME, "Disable auto reconnect for %s", amxc_string_get(dotted_ref, 0));
    amxb_unsubscribe(amxb_be_who_has(amxc_string_get(dotted_ref, 0)), amxc_string_get(dotted_ref, 0), uspa_mtp_mqtt_force_reconnect, mtp_mqtt);

    amxc_string_delete(&dotted_ref);
}

static void uspa_mtp_mqtt_auto_reconnect_changed(UNUSED const char* const sig_name,
                                                 const amxc_var_t* const data,
                                                 void* const priv) {
    uspa_mtp_mqtt_t* mtp_mqtt = (uspa_mtp_mqtt_t*) priv;
    amxo_parser_t* parser = uspa_get_parser();
    const char* prefix = NULL;
    amxc_string_t param_path;

    amxc_string_init(&param_path, 0);
    when_null(parser, exit);

    prefix = GET_CHAR(&parser->config, "prefix_");
    amxc_string_setf(&param_path, "parameters.%sAutoReconnect.to", prefix == NULL ? "" : prefix);
    if(GETP_BOOL(data, amxc_string_get(&param_path, 0))) {
        uspa_mtp_mqtt_auto_reconnect_subscribe(mtp_mqtt);
    } else {
        uspa_mtp_mqtt_auto_reconnect_unsubscribe(mtp_mqtt);
    }

exit:
    amxc_string_clean(&param_path);
    return;
}

static void uspa_mtp_mqtt_auto_reconnect_configure(uspa_mtp_mqtt_t* mtp_mqtt, const char* la_mtp) {
    amxo_parser_t* parser = uspa_get_parser();
    const char* prefix = NULL;
    amxc_var_t ret;
    amxc_string_t path;
    amxc_string_t expr;

    amxc_var_init(&ret);
    amxc_string_init(&path, 0);
    amxc_string_init(&expr, 0);

    when_null(parser, exit);

    amxc_string_setf(&path, "%sMQTT.", la_mtp);
    prefix = GET_CHAR(&parser->config, "prefix_");
    amxc_string_setf(&expr, "(notification == 'dm:object-changed') && \
                     contains('parameters.%sAutoReconnect')", prefix == NULL ? "" : prefix);

    // Subscribe to changes in AutoReconnect parameter
    amxb_subscribe(amxb_be_who_has("LocalAgent."), amxc_string_get(&path, 0), amxc_string_get(&expr, 0),
                   uspa_mtp_mqtt_auto_reconnect_changed, mtp_mqtt);

    amxb_get(amxb_be_who_has("LocalAgent."), amxc_string_get(&path, 0), 0, &ret, 5);

    amxc_string_setf(&path, "0.0.%sAutoReconnect", prefix == NULL ? "" : prefix);
    if(GETP_BOOL(&ret, amxc_string_get(&path, 0))) {
        uspa_mtp_mqtt_auto_reconnect_subscribe(mtp_mqtt);
    }

exit:
    amxc_string_clean(&expr);
    amxc_string_clean(&path);
    amxc_var_clean(&ret);
    return;
}

char* uspa_mtp_mqtt_get_enabled_mtp(const char* reference) {
    bool enabled = false;
    char* mtp = NULL;
    int retval = -1;
    amxc_string_t path;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    when_str_empty(reference, exit);

    amxc_string_setf(&path, "%s", reference);
    amxc_string_trimr(&path, uspa_utils_is_dot);
    amxc_string_prependf(&path, "%s", "LocalAgent.MTP.[MQTT.Reference matches '");
    amxc_string_appendf(&path, "%s", "'].");

    retval = amxb_get(amxb_be_who_has("LocalAgent."), amxc_string_get(&path, 0), 0, &ret, 5);
    when_failed(retval, exit);

    enabled = GETP_BOOL(&ret, "0.0.Enable");
    when_false(enabled, exit);

    mtp = strdup(amxc_var_key(GETP_ARG(&ret, "0.0")));

exit:
    amxc_var_clean(&ret);
    amxc_string_clean(&path);
    return mtp;
}

char* uspa_mtp_mqtt_get_reference(const char* mtp) {
    int retval = -1;
    const char* reference = NULL;
    char* ref_copy = NULL;
    amxc_string_t path;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "%sMQTT.Reference", mtp);

    retval = amxb_get(amxb_be_who_has("LocalAgent."), amxc_string_get(&path, 0), 0, &ret, 5);
    when_failed(retval, exit);

    reference = GETP_CHAR(&ret, "0.0.Reference");
    when_str_empty(reference, exit);

    ref_copy = strdup(reference);

exit:
    amxc_var_clean(&ret);
    amxc_string_clean(&path);
    return ref_copy;
}

int uspa_mtp_mqtt_enable_reference(const char* reference) {
    int retval = -1;
    amxc_var_t ret;
    amxc_var_t values;
    amxc_string_t* dot_reference = uspa_utils_add_dot(reference);

    amxc_var_init(&ret);
    amxc_var_init(&values);

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &values, "Enable", true);

    retval = amxb_set(amxb_be_who_has(amxc_string_get(dot_reference, 0)), amxc_string_get(dot_reference, 0), &values, &ret, 5);

    amxc_string_delete(&dot_reference);
    amxc_var_clean(&values);
    amxc_var_clean(&ret);

    return retval;
}

int uspa_mtp_mqtt_add_user_props(const char* reference) {
    int retval = -1;
    amxc_string_t* props_path = uspa_utils_add_dot(reference);

    amxc_string_append(props_path, "UserProperty.", 13);

    retval = uspa_mtp_mqtt_add_connect_props(props_path);

    amxc_string_delete(&props_path);
    return retval;
}

int uspa_mtp_mqtt_configure(const char* la_mtp) {
    int retval = -1;
    char* reference = NULL;
    amxc_string_t* topic = NULL;
    amxo_parser_t* parser = uspa_get_parser();
    bool set_client_id = GETP_BOOL(&parser->config, "mqtt.set_client_id");

    reference = uspa_mtp_mqtt_get_reference(la_mtp);
    if((reference == NULL) || (*reference == 0)) {
        SAH_TRACEZ_INFO(ME, "No MQTT reference found for %s", la_mtp);
        goto exit;
    }

    uspa_mtp_mqtt_add_user_props(reference);

    topic = uspa_mtp_mqtt_set_response_topic(la_mtp);
    if(!uspa_mtp_mqtt_subscription_exists(reference, amxc_string_get(topic, 0))) {
        uspa_mtp_mqtt_add_subscription(reference, topic);
    }

    if(set_client_id) {
        uspa_mtp_mqtt_set_client_id(reference);
    }

    if(!uspa_mtp_mqtt_is_connected(reference)) {
        SAH_TRACEZ_INFO(ME, "%s not Connected yet, try to enable it", reference);
        retval = uspa_mtp_mqtt_enable_reference(reference);
        when_failed(retval, exit);
    } else {
        SAH_TRACEZ_INFO(ME, "%s already Connected, update Status", reference);
        uspa_object_update_status(la_mtp, "Up");
    }

    retval = uspa_imtp_connect(reference, USPA_MTP_ID_MQTT, la_mtp);
    when_failed(retval, exit);

exit:
    amxc_string_delete(&topic);
    free(reference);
    return retval;
}

/**
 * Called if MQTT.Client.{i}. is disabled
 *           LocalAgent.MTP.{i}. is disabled
 */
void uspa_mtp_mqtt_disable(const char* mtp) {
    char* reference = NULL;
    uspi_con_t* con = NULL;

    reference = uspa_mtp_mqtt_get_reference(mtp);
    when_str_empty(reference, exit);

    con = uspa_mtp_mqtt_get_connection(reference);
    when_null(con, exit);

    uspa_imtp_disconnect(con, USPA_MTP_ID_MQTT);

exit:
    free(reference);
    return;
}

int uspa_mtp_mqtt_subscribe(amxb_bus_ctx_t* ctx) {
    int retval = -1;
    const char* path = "MQTT.Client.";
    const char* expr_enabled = "(notification == 'dm:object-changed') && \
                                (contains('parameters.Enable'))";
    const char* expr_status = "(notification == 'dm:object-changed') && \
                               (contains('parameters.Status'))";

    retval = amxb_subscribe(ctx, path, expr_enabled, uspa_mtp_mqtt_enable_toggled, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to changes in Enable parameter of %s", path);
        goto exit;
    }

    retval = amxb_subscribe(ctx, path, expr_status, uspa_mtp_mqtt_status_changed, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to changes in Status parameter of %s", path);
        goto exit;
    }

exit:
    return retval;
}

void uspa_mtp_mqtt_clean(amxb_bus_ctx_t* ctx) {
    const char* path = "MQTT.Client.";

    amxb_unsubscribe(ctx, path, uspa_mtp_mqtt_status_changed, NULL);
    amxb_unsubscribe(ctx, path, uspa_mtp_mqtt_enable_toggled, NULL);
}

uspi_con_t* uspa_mtp_mqtt_get_connection(const char* reference) {
    uspi_con_t* ret = NULL;
    amxc_htable_t* mtp_instances = uspa_mtp_get_instances();
    when_null(reference, exit);

    amxc_htable_for_each(it, mtp_instances) {
        uspa_mtp_instance_t* inst = amxc_container_of(it, uspa_mtp_instance_t, hit);
        uspa_mtp_mqtt_t* mqtt = NULL;

        if(inst->mtp_id != USPA_MTP_ID_MQTT) {
            continue;
        }

        mqtt = inst->mtp_data.mqtt;
        if((mqtt == NULL) || (mqtt->reference == NULL)) {
            continue;
        }

        if(strcmp(mqtt->reference, reference) == 0) {
            ret = mqtt->con;
            break;
        }
    }

exit:
    return ret;
}

uspi_con_t* uspa_mtp_mqtt_get_connection_by_mtp_ref(const char* mtp_ref) {
    uspi_con_t* ret = NULL;
    amxc_htable_t* mtp_instances = uspa_mtp_get_instances();
    amxc_string_t mtp_str;

    amxc_string_init(&mtp_str, 0);

    when_null(mtp_ref, exit);

    // Links to LocalAgent.MTP. are stored without Device prefix
    amxc_string_setf(&mtp_str, "%s", mtp_ref);
    uspi_prefix_strip_device(&mtp_str);

    amxc_htable_for_each(it, mtp_instances) {
        uspa_mtp_instance_t* inst = amxc_container_of(it, uspa_mtp_instance_t, hit);
        uspa_mtp_mqtt_t* mqtt = NULL;

        if(inst->mtp_id != USPA_MTP_ID_MQTT) {
            continue;
        }

        mqtt = inst->mtp_data.mqtt;
        if(mqtt == NULL) {
            continue;
        }

        if(strcmp(inst->la_mtp, amxc_string_get(&mtp_str, 0)) == 0) {
            ret = mqtt->con;
            break;
        }
    }

exit:
    amxc_string_clean(&mtp_str);
    return ret;
}

// Find the corresponding uspa_mtp_instance_t and add it to private data of uspi_con_t
// Create uspa_mtp_mqtt_t struct and link it with uspa_mtp_instance_t struct
// Subscribes to changes in LocalAgent.MTP.{i}.AutoReconnect to see if it changes and
// creates a subscription to the MQTT reference parameters in case the reconnect is needed
int uspa_mtp_mqtt_new(uspi_con_t* con,
                      const char* reference,
                      const char* la_mtp) {
    int retval = -1;
    amxc_htable_t* mtp_instances = uspa_mtp_get_instances();
    uspa_mtp_instance_t* mtp_inst = NULL;
    uspa_mtp_mqtt_t* mtp_mqtt = NULL;
    amxc_htable_it_t* hit = NULL;

    when_null(con, exit);
    when_str_empty(reference, exit);
    when_str_empty(la_mtp, exit);

    hit = amxc_htable_get(mtp_instances, la_mtp);
    when_null(hit, exit);

    mtp_inst = amxc_container_of(hit, uspa_mtp_instance_t, hit);
    con->priv = mtp_inst;

    mtp_mqtt = calloc(1, sizeof(uspa_mtp_mqtt_t));
    if(mtp_mqtt == NULL) {
        goto exit;
    }

    mtp_mqtt->reference = strdup(reference);
    if(mtp_mqtt->reference == NULL) {
        free(mtp_mqtt);
        goto exit;
    }

    mtp_inst->mtp_data.mqtt = mtp_mqtt;

    uspa_mtp_mqtt_auto_reconnect_configure(mtp_mqtt, la_mtp);

    retval = 0;
exit:
    return retval;
}

void uspa_mtp_mqtt_delete(uspi_con_t* con) {
    uspa_mtp_instance_t* mtp_inst = NULL;
    uspa_mtp_mqtt_t* mtp_mqtt = NULL;
    amxc_string_t path;

    amxc_string_init(&path, 0);

    when_null(con, exit);
    when_null(con->priv, exit);

    mtp_inst = (uspa_mtp_instance_t*) con->priv;
    when_true(mtp_inst->mtp_id != USPA_MTP_ID_MQTT, exit);

    mtp_mqtt = mtp_inst->mtp_data.mqtt;
    when_null(mtp_mqtt, exit);

    uspa_mtp_mqtt_auto_reconnect_unsubscribe(mtp_mqtt);
    amxc_string_setf(&path, "%sMQTT", mtp_inst->la_mtp);
    amxb_unsubscribe(amxb_be_who_has("LocalAgent."), amxc_string_get(&path, 0),
                     uspa_mtp_mqtt_auto_reconnect_changed, mtp_mqtt);
    free(mtp_mqtt->response_topic);
    free(mtp_mqtt->reference);
    free(mtp_mqtt);
    mtp_inst->mtp_data.mqtt = NULL;
    con->priv = NULL;

exit:
    amxc_string_clean(&path);
    return;
}
