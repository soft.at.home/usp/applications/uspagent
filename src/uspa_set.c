/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>

#include <usp/uspl.h>

#include <amxa/amxa_set.h>

#include "uspa.h"
#include "uspa_msghandler.h"
#include "uspa_error.h"
#include "uspa_controller.h"

static void uspa_set_convert_multiple(amxc_var_t* requests,
                                      amxc_var_t* set_input) {
    amxc_string_t path_string;
    amxc_string_init(&path_string, 0);

    amxc_var_for_each(request, requests) {
        const char* obj_path = GET_CHAR(request, "object_path");
        amxc_var_t* parameters = GET_ARG(request, "parameters");
        amxc_var_t* single_set = NULL;
        amxc_var_t* params = NULL;
        amxc_var_t* oparams = NULL;

        amxc_string_setf(&path_string, "%s", obj_path);

        single_set = amxc_var_add(amxc_htable_t, set_input, NULL);
        amxc_var_add_key(cstring_t, single_set, "path", amxc_string_get(&path_string, 0));
        params = amxc_var_add_key(amxc_htable_t, single_set, "parameters", NULL);
        oparams = amxc_var_add_key(amxc_htable_t, single_set, "oparameters", NULL);

        amxc_var_for_each(param, parameters) {
            const char* param_name = GET_CHAR(param, "param");
            amxc_var_t* param_value = GET_ARG(param, "value");
            if(GET_BOOL(param, "required")) {
                amxc_var_set_key(params, param_name, param_value, AMXC_VAR_FLAG_COPY);
            } else {
                amxc_var_set_key(oparams, param_name, param_value, AMXC_VAR_FLAG_COPY);
            }
        }
    }
    amxc_string_clean(&path_string);
}

static int uspa_set_build_error(uspl_rx_t* usp_rx,
                                uspl_tx_t** usp_tx,
                                amxc_var_t* set_output) {
    int retval = -1;
    char* msg_id = NULL;
    uint32_t err_code;
    amxc_var_t error;
    amxc_var_t* requested_path = NULL;
    amxc_var_t* result = NULL;

    amxc_var_init(&error);

    // amxa_set_multiple will return a hash table with the first failed result
    requested_path = GETI_ARG(set_output, 0);

    // According to the usp test plan we expect the invalid arguments top level error
    err_code = USP_ERR_INVALID_ARGUMENTS;

    uspa_error_top_level(&error, err_code, NULL);

    result = GET_ARG(requested_path, "result");
    amxc_var_for_each(resolved_path, result) {
        const char* obj_path = amxc_var_key(resolved_path);

        amxc_var_for_each(param, resolved_path) {
            const char* param_path = amxc_var_key(param);
            amxd_status_t status = GET_UINT32(param, "error_code");
            err_code = uspl_set_amxd_status_to_usp_param_error(status);
            uspa_error_append_param(&error, obj_path, param_path, err_code, NULL);
        }
    }

    retval = uspa_msghandler_build_reply_basic(usp_rx, usp_tx);
    when_failed(retval, exit);

    msg_id = uspl_msghandler_msg_id(usp_rx);
    when_null(msg_id, exit);

    retval = uspl_error_resp_new(*usp_tx, &error, msg_id);
    when_failed(retval, exit);

    retval = 0;
exit:
    amxc_var_clean(&error);
    return retval;
}

static void uspa_set_add_param_err(amxc_var_t* param_errs, const char* param_name, uint32_t status) {
    amxc_var_t* error = amxc_var_add(amxc_htable_t, param_errs, NULL);
    uint32_t err_code = uspl_set_amxd_status_to_usp_param_error(status);
    const char* err_msg = uspl_error_code_to_str(err_code);

    amxc_var_add_key(cstring_t, error, "param", param_name);
    amxc_var_add_key(cstring_t, error, "err_msg", err_msg);
    amxc_var_add_key(uint32_t, error, "err_code", err_code);
}

static void uspa_build_set_success(amxc_var_t* single_result, amxc_var_t* set_result) {
    amxc_var_t* success = NULL;
    amxc_var_t* updated_inst_res = NULL;

    amxc_var_add_key(uint32_t, single_result, "oper_status", USP__SET_RESP__UPDATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS);
    success = amxc_var_add_key(amxc_htable_t, single_result, "success", NULL);
    updated_inst_res = amxc_var_add_key(amxc_llist_t, success, "updated_inst_results", NULL);

    amxc_var_for_each(resolved, set_result) {
        const char* affected_path = amxc_var_key(resolved);
        amxc_var_t* updated_inst = amxc_var_add(amxc_htable_t, updated_inst_res, NULL);
        amxc_var_t* updated_params = amxc_var_add_key(amxc_htable_t, updated_inst, "updated_params", NULL);
        amxc_var_t* param_errs = amxc_var_add_key(amxc_llist_t, updated_inst, "param_errs", NULL);
        amxc_var_add_key(cstring_t, updated_inst, "affected_path", affected_path);
        amxc_var_for_each(param, resolved) {
            uint32_t err_code = 0;
            const char* param_name = amxc_var_key(param);
            if(amxc_var_type_of(param) == AMXC_VAR_ID_HTABLE) {
                err_code = GET_UINT32(param, "error_code");
            }
            if(err_code == 0) {
                amxc_var_set_key(updated_params, param_name, param, AMXC_VAR_FLAG_COPY);
            } else {
                uspa_set_add_param_err(param_errs, param_name, err_code);
            }
        }
    }
}

static void uspa_build_set_failure(amxc_var_t* single_result,
                                   amxc_var_t* set_result,
                                   uint32_t status) {
    amxc_var_t* updated_inst_failures = NULL;
    amxc_var_t* failure = amxc_var_add_key(amxc_htable_t, single_result, "failure", NULL);
    uint32_t err_code = USP_ERR_REQUIRED_PARAM_FAILED;
    const char* err_msg = uspl_error_code_to_str(err_code);

    amxc_var_add_key(uint32_t, single_result, "oper_status", USP__SET_RESP__UPDATED_OBJECT_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE);
    amxc_var_add_key(uint32_t, failure, "err_code", err_code);
    amxc_var_add_key(cstring_t, failure, "err_msg", err_msg);
    updated_inst_failures = amxc_var_add_key(amxc_llist_t, failure, "updated_inst_failures", NULL);

    amxc_var_for_each(resolved, set_result) {
        amxc_var_t* inst_failure = amxc_var_add(amxc_htable_t, updated_inst_failures, NULL);
        amxc_var_t* param_errs = amxc_var_add_key(amxc_llist_t, inst_failure, "param_errs", NULL);
        const char* affected_path = amxc_var_key(resolved);
        amxc_var_add_key(cstring_t, inst_failure, "affected_path", affected_path);
        amxc_var_for_each(param, resolved) {
            const char* param_name = amxc_var_key(param);
            uspa_set_add_param_err(param_errs, param_name, status);
        }
    }
}

static int uspa_build_set_response(uspl_rx_t* usp_rx,
                                   uspl_tx_t** usp_tx,
                                   amxc_var_t* set_output) {
    int retval = -1;
    char* msg_id = NULL;
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);

    amxc_var_for_each(requested, set_output) {
        amxc_var_t* single_result = NULL;
        amxc_var_t* set_result = GET_ARG(requested, "result");
        const char* requested_path = GET_CHAR(requested, "path");
        uint32_t status = GET_UINT32(requested, "status");

        amxc_var_new(&single_result);
        amxc_var_set_type(single_result, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, single_result, "requested_path", requested_path);

        if(status == amxd_status_ok) {
            uspa_build_set_success(single_result, set_result);
        } else {
            uspa_build_set_failure(single_result, set_result, status);
        }
        amxc_llist_append(&resp_list, &single_result->lit);
    }

    retval = uspa_msghandler_build_reply_basic(usp_rx, usp_tx);
    when_failed(retval, exit);

    msg_id = uspl_msghandler_msg_id(usp_rx);
    when_null(msg_id, exit);

    retval = uspl_set_resp_new(*usp_tx, &resp_list, msg_id);

exit:
    amxc_llist_clean(&resp_list, variant_list_it_free);
    return retval;
}

int uspa_handle_set(uspl_rx_t* usp_rx,
                    uspl_tx_t** usp_tx,
                    const char* acl_file) {
    SAH_TRACEZ_INFO(ME, "Handle USP message of type Set");
    int retval = -1;
    amxb_bus_ctx_t* ctx = NULL;
    amxc_var_t request;
    amxc_var_t set_input;
    amxc_var_t set_output;
    amxc_var_t* requests = NULL;
    bool allow_partial = false;
    uint32_t flags = 0;
    uint32_t access = AMXB_PUBLIC;

    amxc_var_init(&request);
    amxc_var_init(&set_input);
    amxc_var_init(&set_output);

    amxc_var_set_type(&set_input, AMXC_VAR_ID_LIST);
    amxc_var_set_type(&set_output, AMXC_VAR_ID_HTABLE);

    when_null(usp_rx, exit);
    when_null(usp_tx, exit);

    retval = uspl_set_extract(usp_rx, &request);
    if(retval != 0) {
        SAH_TRACEZ_INFO(ME, "Error extrancting set request");
        retval = uspa_msghandler_build_error(usp_rx, usp_tx, retval);
        goto exit;
    }

    allow_partial = GET_BOOL(&request, "allow_partial");
    flags = allow_partial ? AMXB_FLAG_PARTIAL : 0;

    requests = GET_ARG(&request, "requests");
    uspa_set_convert_multiple(requests, &set_input);

    // TODO: ctx could be different depending on the path
    access = uspa_controller_get_access(uspl_msghandler_from_id(usp_rx));
    ctx = uspa_discovery_get_ctx(amxc_var_constcast(cstring_t, GETP_ARG(&set_input, "0.path")));
    amxb_set_access(ctx, access);
    retval = amxa_set_multiple(ctx, flags, acl_file, &set_input, &set_output, 5);
    amxb_set_access(ctx, AMXB_PROTECTED);

    // If retval is nonzero, build USP error message with provided return variant and possible
    // forbidden set parameters
    if(retval != 0) {
        SAH_TRACEZ_INFO(ME, "Building error response");
        uspa_set_build_error(usp_rx, usp_tx, &set_output);
    } else {
        // If retval is zero, all required parameters passed. Build USP SetResp.
        SAH_TRACEZ_INFO(ME, "Building set response");
        uspa_build_set_response(usp_rx, usp_tx, &set_output);
    }

    retval = 0;
exit:
    amxc_var_clean(&request);
    amxc_var_clean(&set_input);
    amxc_var_clean(&set_output);

    return retval;
}
