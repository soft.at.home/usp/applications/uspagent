/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <usp/uspl.h>

#include "uspa.h"
#include "uspa_msghandler.h"
#include "uspa_subscription.h"
#include "uspa_controller.h"

/**
 * When a Notify response arrives, find the uspa_sub_t struct that corresponds to the notification
 * based on the Subscription ID of the Notify response. Then look at the list of all notifications
 * related to that Subscription to figure out from which notification we received a response based
 * on the USP message ID in the Notify response.
 * Delete the uspa_sub_retry_t struct that is found to prevent further notifications from being
 * sent.
 */
int uspa_handle_notify_resp(uspl_rx_t* usp_rx,
                            UNUSED uspl_tx_t** usp_tx,
                            UNUSED const char* role) {
    SAH_TRACEZ_INFO(ME, "Handle USP message of type Notify Resp");
    int retval = -1;
    amxc_var_t* notif_resp = NULL;
    amxc_llist_t resp_list;
    const char* id = NULL;
    char* msg_id = NULL;
    uspa_sub_t* sub = NULL;
    uspa_sub_retry_t* sub_retry = NULL;

    amxc_llist_init(&resp_list);

    when_null(usp_rx, exit);

    retval = uspl_notify_resp_extract(usp_rx, &resp_list);
    when_failed(retval, exit);

    notif_resp = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    id = amxc_var_constcast(cstring_t, notif_resp);

    msg_id = uspl_msghandler_msg_id(usp_rx);

    SAH_TRACEZ_INFO(ME, "Received Notify response with Subscription ID=%s and msg_id=%s", id, msg_id);
    if(strlen(id) == 0) {
        // ID is only empty for onboard request
        uspa_controller_obr_done(msg_id);
        goto exit;
    }

    sub = uspa_sub_find_from_id(id);
    when_null(sub, exit);

    sub_retry = uspa_sub_retry_find_from_msg_id(sub, msg_id);
    uspa_sub_retry_delete(&sub_retry);

exit:
    amxc_llist_clean(&resp_list, variant_list_it_free);
    return retval;
}

/**
 * Handle Notify request messages. These will only arrive from agents that are connected over the
 * IMTP and need to be forwarded to the subscribed controller based on their subscription ID.
 *
 * A notify response should also be built and sent if send_resp is true.
 */
int uspa_handle_notify(uspl_rx_t* usp_rx,
                       uspl_tx_t** usp_tx,
                       UNUSED const char* role) {
    SAH_TRACEZ_INFO(ME, "Handle USP message of type Notify");
    int retval = -1;
    amxc_var_t request;
    amxc_var_t response;
    amxc_llist_t resp_list;
    const char* sub_id = NULL;
    bool send_resp = false;

    amxc_var_init(&response);
    amxc_var_init(&request);
    amxc_llist_init(&resp_list);

    when_null(usp_rx, exit);
    when_null(usp_tx, exit);

    when_false(uspl_notify_extract(usp_rx, &request) == 0, exit);

    sub_id = GET_CHAR(&request, "subscription_id");
    uspa_sub_notify_controller(sub_id, &request);

    send_resp = GET_BOOL(&request, "send_resp");
    if(!send_resp) {
        SAH_TRACEZ_INFO(ME, "Notify response not needed");
        retval = 0;
        goto exit;
    }

    amxc_var_set(cstring_t, &response, sub_id);
    amxc_llist_append(&resp_list, &response.lit);
    retval = uspa_msghandler_build_reply(usp_rx, &resp_list, uspl_notify_resp_new, usp_tx);

exit:
    amxc_var_clean(&request);
    amxc_llist_clean(&resp_list, variant_list_it_free);

    return retval;
}
