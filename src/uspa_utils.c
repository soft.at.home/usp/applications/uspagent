/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <sys/types.h>
#include <regex.h>

#include "uspa_utils.h"

amxc_string_t* uspa_utils_add_dot(const char* str_in) {
    size_t len = 0;
    amxc_string_t* dotted_path = NULL;

    amxc_string_new(&dotted_path, 0);

    when_str_empty(str_in, exit);

    amxc_string_setf(dotted_path, "%s", str_in);
    len = amxc_string_text_length(dotted_path);

    if(str_in[len - 1] != '.') {
        amxc_string_append(dotted_path, ".", 1);
    }

exit:
    return dotted_path;
}

int uspa_utils_is_dot(int c) {
    return (c == '.') ? 1 : 0;
}

int uspa_utils_is_brace(int c) {
    return ((c == '(') || (c == ')')) ? 1 : 0;
}

bool uspa_utils_matches(const char* regex_str, const char* path) {
    bool retval = false;
    regex_t regex;

    regcomp(&regex, regex_str, REG_EXTENDED);
    if(regexec(&regex, path, 0, NULL, 0) == 0) {
        retval = true;
    } else {
        retval = false;
    }

    regfree(&regex);
    return retval;
}

bool uspa_utils_ignore_allow_partial(void) {
    amxo_parser_t* parser = uspa_get_parser();
    bool retval = false;

    when_null(parser, exit);

    retval = amxc_var_constcast(bool, amxo_parser_get_config(parser, "ignore-allow-partial"));
exit:
    return retval;
}
