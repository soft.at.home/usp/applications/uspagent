/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>

#include <usp/uspl.h>

#include "uspa.h"
#include "uspa_msghandler.h"
#include "uspa_controller.h"

static void uspa_gi_add_inst(amxc_var_t* curr_insts, amxc_var_t* inst) {
    const char* inst_path = amxc_var_key(inst);
    amxc_var_t* entry = amxc_var_add(amxc_htable_t, curr_insts, NULL);
    amxc_var_t* keys = NULL;

    amxc_var_add_key(cstring_t, entry, "inst_path", inst_path);
    keys = amxc_var_add_key(amxc_htable_t, entry, "unique_keys", NULL);

    amxc_var_for_each(el, inst) {
        amxc_var_set_key(keys, amxc_var_key(el), el, AMXC_VAR_FLAG_COPY);
    }
}

static void uspa_gi_resp_list_append(amxc_llist_t* resp_list,
                                     amxc_var_t* ret,
                                     const char* path,
                                     int status) {
    int err_code = USP_ERR_INTERNAL_ERROR;
    amxc_var_t* response = NULL;
    amxc_var_t* curr_insts = NULL;

    amxc_var_new(&response);
    amxc_var_set_type(response, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, response, "requested_path", path);

    err_code = uspl_get_instances_amxd_status_to_usp_err(status);
    amxc_var_add_key(uint32_t, response, "err_code", err_code);
    if(err_code != USP_ERR_OK) {
        const char* err_msg = uspl_error_code_to_str(err_code);
        amxc_var_add_key(cstring_t, response, "err_msg", err_msg);
    }

    when_true(ret == NULL || amxc_var_type_of(ret) == AMXC_VAR_ID_NULL, exit);

    curr_insts = amxc_var_add_key(amxc_llist_t, response, "curr_insts", NULL);
    amxc_var_for_each(inst, ret) {
        uspa_gi_add_inst(curr_insts, inst);
    }

exit:
    amxc_llist_append(resp_list, &response->lit);
}

static int uspa_invoke_get_instances(amxb_bus_ctx_t* ctx,
                                     UNUSED amxc_llist_t* resp_list,
                                     const char* path,
                                     bool first_level,
                                     const char* acl_file,
                                     uint32_t access) {
    SAH_TRACEZ_INFO(ME, "Invoke get_instances on path: [%s]", path);
    int retval = -1;
    int depth = first_level ? 0 : -1;
    amxc_var_t ret;

    amxc_var_init(&ret);

    amxb_set_access(ctx, access);
    retval = amxa_get_instances(ctx, path, acl_file, depth, &ret, 5);

    uspa_gi_resp_list_append(resp_list, GETI_ARG(&ret, 0), path, retval);

    amxb_set_access(ctx, AMXB_PROTECTED);
    amxc_var_clean(&ret);
    return retval;
}

int uspa_handle_get_instances(uspl_rx_t* usp_rx,
                              uspl_tx_t** usp_tx,
                              const char* acl_file) {
    SAH_TRACEZ_INFO(ME, "Handle USP message of type GetInstances");
    int retval = -1;
    amxc_var_t result;
    bool first_level = false;
    amxc_var_t* obj_paths = NULL;
    amxc_llist_t resp_list;
    uint32_t access = AMXB_PUBLIC;

    amxc_var_init(&result);
    amxc_llist_init(&resp_list);

    when_null(usp_rx, exit);
    when_null(usp_tx, exit);

    when_false(uspl_get_instances_extract(usp_rx, &result) == 0, exit);

    access = uspa_controller_get_access(uspl_msghandler_from_id(usp_rx));
    first_level = GET_BOOL(&result, "first_level_only");
    obj_paths = GET_ARG(&result, "obj_paths");

    amxc_var_for_each(var, obj_paths) {
        const char* path = amxc_var_constcast(cstring_t, var);
        amxb_bus_ctx_t* ctx = uspa_discovery_get_ctx(path);
        uspa_invoke_get_instances(ctx, &resp_list, path, first_level, acl_file, access);
    }

    retval = uspa_msghandler_build_reply(usp_rx, &resp_list, uspl_get_instances_resp_new, usp_tx);

exit:
    amxc_var_clean(&result);
    amxc_llist_clean(&resp_list, variant_list_it_free);

    return retval;
}
