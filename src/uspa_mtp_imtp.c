/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <libgen.h>

#include <imtp/imtp_connection.h>
#include <uspi/uspi_connection.h>
#include <uspi/uspi_discovery.h>

#include "uspa_object.h"
#include "uspa_mtp.h"
#include "uspa_mtp_imtp.h"
#include "uspa_imtp.h"
#include "uspa_discovery.h"
#include "uspa_agent.h"
#include "uspa_msghandler.h"
#include "uspa_controller.h"
#include "uspa_prefix_check.h"

static int uspa_mtp_imtp_new(uspa_mtp_imtp_t** imtp,
                             uspi_con_t* con_agent,
                             uspi_con_t* con_controller) {
    int retval = -1;

    when_null(imtp, exit);

    *imtp = (uspa_mtp_imtp_t*) calloc(1, sizeof(uspa_mtp_imtp_t));
    when_null(*imtp, exit);

    (*imtp)->con_agent = con_agent;
    (*imtp)->con_controller = con_controller;

    retval = 0;
exit:
    return retval;
}

static void uspa_mtp_imtp_remove_accepted_con(uspi_con_t* con) {
    amxo_parser_t* parser = uspa_get_parser();

    when_null(con, exit);

    if(uspi_con_flag_is_set(con, USPI_CON_CONTROLLER)) {
        uspa_controller_con_unlink(con);
    } else if(uspi_con_flag_is_set(con, USPI_CON_AGENT)) {
        uspi_discovery_del_service(amxb_be_who_has("LocalAgent."), con->eid);
    }
    amxo_connection_remove(parser, uspi_con_get_fd(con));
    uspi_con_disconnect(&con);

exit:
    return;
}

static void uspa_mtp_imtp_con_remove(uspi_con_t* con) {
    amxo_parser_t* parser = uspa_get_parser();
    if(uspi_con_flag_is_set(con, USPI_CON_LISTEN)) {
        amxc_llist_for_each(it, &con->accepted_cons) {
            uspi_con_t* con_accepted = amxc_container_of(it, uspi_con_t, lit);
            uspa_mtp_imtp_remove_accepted_con(con_accepted);
        }
    }

    amxo_connection_remove(parser, uspi_con_get_fd(con));
    uspi_con_disconnect(&con);
}

static int uspa_mtp_imtp_contr_con_add(uspi_con_t* con) {
    amxb_bus_ctx_t* ctx = amxb_be_who_has("LocalAgent.");
    int retval = -1;
    const char* contr_inst = NULL;
    const char* contr_mtp_inst = NULL;
    amxc_var_t ret_1;
    amxc_var_t ret_2;
    amxc_string_t path;

    amxc_var_init(&ret_1);
    amxc_var_init(&ret_2);
    amxc_string_init(&path, 0);

    amxc_string_setf(&path, "LocalAgent.Controller.[EndpointID == '%s' && Enable == true].",
                     con->eid);
    amxb_get(ctx, amxc_string_get(&path, 0), 0, &ret_1, 5);
    contr_inst = amxc_var_key(GETP_ARG(&ret_1, "0.0"));
    when_str_empty_trace(contr_inst, exit, WARNING, "Failed to find enabled controller with EID = %s", con->eid);

    amxc_string_setf(&path, "%sMTP.[Protocol == 'UDS' && Enable == true].", contr_inst);
    amxb_get(ctx, amxc_string_get(&path, 0), 0, &ret_2, 5);
    contr_mtp_inst = amxc_var_key(GETP_ARG(&ret_2, "0.0"));
    when_str_empty_trace(contr_mtp_inst, exit, WARNING, "Failed to find enabled MTP of type UDS for controller with EID = %s", con->eid);

    retval = uspa_controller_con_add(contr_inst, contr_mtp_inst, con);

exit:
    amxc_var_clean(&ret_2);
    amxc_var_clean(&ret_1);
    amxc_string_clean(&path);
    return retval;
}

static void uspa_mtp_imtp_handle_protobuf(uspi_con_t* con, const imtp_tlv_t* tlv_protobuf) {
    unsigned char* pbuf = (unsigned char*) tlv_protobuf->value + tlv_protobuf->offset;
    int pbuf_len = tlv_protobuf->length;
    amxc_var_t mtp_info;

    amxc_var_init(&mtp_info);
    amxc_var_set_type(&mtp_info, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &mtp_info, "protocol", "UDS");

    uspa_msghandler_process_binary_record(pbuf, pbuf_len, con, &mtp_info);

    amxc_var_clean(&mtp_info);
}

static int uspa_mtp_imtp_handle_handshake(uspi_con_t* con, const imtp_tlv_t* tlv_handshake) {
    int retval = -1;

    retval = uspi_con_add_eid(con, tlv_handshake);
    if(retval == 0) {
        SAH_TRACEZ_INFO(ME, "Add eid '%s' to con with fd %d", con->eid, uspi_con_get_fd(con));
    } else {
        SAH_TRACEZ_WARNING(ME, "Failed to add eid to con with fd %d", uspi_con_get_fd(con));
        goto exit;
    }

    if(uspi_con_flag_is_set(con, USPI_CON_AGENT)) {
        // Note that this will check for uniqueness, since the parameter is unique
        retval = uspi_discovery_add_service(amxb_be_who_has("LocalAgent."), con->eid);
        if(retval != 0) {
            SAH_TRACEZ_ERROR(ME, "Possible duplicate EndpointID: %s", con->eid);
            goto exit;
        }
    }

    if(uspi_con_flag_is_set(con, USPI_CON_CONTROLLER)) {
        // Add connection to list of connections for the controller (if a controller is found)
        retval = uspa_mtp_imtp_contr_con_add(con);
        if(retval != 0) {
            SAH_TRACEZ_WARNING(ME, "Failed to add con for LocalAgent.Controller. instance with EID: %s", con->eid);
            goto exit;
        }
    }

exit:
    return retval;
}

static void uspa_mtp_imtp_handle_sub(uspi_con_t* con, const imtp_tlv_t* tlv_sub) {
    char* pbuf = (char*) tlv_sub->value + tlv_sub->offset;
    char* recipient = uspa_controller_get_instance_from_eid(con->eid);
    amxc_var_t values;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_var_init(&values);

    SAH_TRACEZ_INFO(ME, "Handle TLV of type subscribe for path %s", pbuf);
    when_null(recipient, exit);

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "ReferenceList", pbuf);
    amxc_var_add_key(cstring_t, &values, "Recipient", recipient);
    amxc_var_add_key(cstring_t, &values, "NotifType", "AmxNotification");
    amxc_var_add_key(bool, &values, "Enable", true);

    if(amxb_add(amxb_be_who_has("LocalAgent."), "LocalAgent.Subscription.", 0, NULL, &values, &ret, 5) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to add subscription for path %s", pbuf);
    }

exit:
    free(recipient);
    amxc_var_clean(&ret);
    amxc_var_clean(&values);
}

static void uspa_mtp_imtp_handle_unsub(uspi_con_t* con, const imtp_tlv_t* tlv_unsub) {
    char* pbuf = (char*) tlv_unsub->value + tlv_unsub->offset;
    char* recipient = uspa_controller_get_instance_from_eid(con->eid);
    amxc_var_t ret;
    amxc_string_t path;

    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    SAH_TRACEZ_INFO(ME, "Handle TLV of type unsubscribe for path %s", pbuf);
    when_null(recipient, exit);

    amxc_string_setf(&path, "LocalAgent.Subscription.[Recipient=='%s' && ReferenceList=='%s'].",
                     recipient, pbuf);

    if(amxb_del(amxb_be_who_has("LocalAgent."), amxc_string_get(&path, 0), 0, NULL, &ret, 5) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to del subscription for path %s", amxc_string_get(&path, 0));
    }

exit:
    free(recipient);
    amxc_string_clean(&path);
    amxc_var_clean(&ret);
}

static void uspa_mtp_imtp_handle_msg(uspi_con_t* con, imtp_frame_t* frame) {
    const imtp_tlv_t* tlv_handshake = NULL;
    const imtp_tlv_t* tlv_pbuf = NULL;
    const imtp_tlv_t* tlv_sub = NULL;
    const imtp_tlv_t* tlv_unsub = NULL;
    int retval = 0;

    tlv_handshake = imtp_frame_get_first_tlv(frame, imtp_tlv_type_handshake);
    if(tlv_handshake != NULL) {
        retval = uspa_mtp_imtp_handle_handshake(con, tlv_handshake);
        when_failed(retval, exit);
    }

    if(con->eid == NULL) {
        SAH_TRACEZ_WARNING(ME, "Received IMTP message from connection with unknown EID");
        goto exit;
    }

    tlv_pbuf = imtp_frame_get_first_tlv(frame, imtp_tlv_type_protobuf_bytes);
    if(tlv_pbuf != NULL) {
        uspa_mtp_imtp_handle_protobuf(con, tlv_pbuf);
    }

    tlv_sub = imtp_frame_get_first_tlv(frame, imtp_tlv_type_subscribe);
    if(tlv_sub != NULL) {
        uspa_mtp_imtp_handle_sub(con, tlv_sub);
    }

    tlv_unsub = imtp_frame_get_first_tlv(frame, imtp_tlv_type_unsubscribe);
    if(tlv_unsub != NULL) {
        uspa_mtp_imtp_handle_unsub(con, tlv_unsub);
    }

exit:
    return;
}

static void uspa_mtp_imtp_con_read(UNUSED int fd, void* priv) {
    int retval = -1;
    uspi_con_t* con = (uspi_con_t*) priv;
    imtp_frame_t* frame = NULL;
    SAH_TRACEZ_INFO(ME, "Incoming message on fd: [%d] from %s with eid %s", fd,
                    (con->flags & USPI_CON_AGENT) == USPI_CON_AGENT ? "agent" : "controller",
                    con->eid);

    retval = uspi_con_read_frame(con, &frame);
    when_failed(retval, exit);

    uspa_mtp_imtp_handle_msg(con, frame);

exit:
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Removing accepted connection and Service instance");
        uspa_mtp_imtp_remove_accepted_con(con);
    }
    imtp_frame_delete(&frame);
    return;
}

static void uspa_mtp_imtp_accept(UNUSED int fd, void* priv) {
    SAH_TRACEZ_INFO(ME, "New incoming UDS connection");
    uspi_con_t* con_listen = (uspi_con_t*) priv;
    uspi_con_t* con_accepted = NULL;
    int retval = -1;

    // Note that the agent EndpointID is automatically sent to the other end via the USP back-end when accepting the connection
    retval = uspi_con_accept(con_listen, &con_accepted);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to accept incoming connection");
        goto exit;
    }

    if(uspi_con_flag_is_set(con_listen, USPI_CON_AGENT)) {
        uspi_con_flag_toggle(con_accepted, USPI_CON_CONTROLLER);
    } else {
        uspi_con_flag_toggle(con_accepted, USPI_CON_AGENT);
    }

    amxo_connection_add(uspa_get_parser(),
                        uspi_con_get_fd(con_accepted),
                        uspa_mtp_imtp_con_read,
                        NULL,
                        AMXO_BUS,
                        con_accepted);

exit:
    return;
}

static void uspa_mtp_imtp_subscription_filter_params(amxc_var_t* params) {
    amxc_var_t* ref_list = GET_ARG(params, "ReferenceList");
    amxc_var_t* alias = GET_ARG(params, "Alias");
    amxc_var_t* recipient = GET_ARG(params, "Recipient");
    amxc_var_t* creation_date = GET_ARG(params, "CreationDate");
    amxc_var_t* last_value = GET_ARG(params, "LastValue");
    amxc_var_delete(&ref_list);
    amxc_var_delete(&alias);
    amxc_var_delete(&recipient);
    amxc_var_delete(&creation_date);
    amxc_var_delete(&last_value);
}

uspi_con_t* uspa_mtp_imtp_get_con_from_eid(const char* eid, int flag) {
    uspi_con_t* con = NULL;
    amxc_htable_t* mtp_instances = uspa_mtp_get_instances();

    when_str_empty(eid, exit);

    amxc_htable_for_each(hit, mtp_instances) {
        uspa_mtp_instance_t* inst = amxc_htable_it_get_data(hit, uspa_mtp_instance_t, hit);
        uspi_con_t* con_listen = NULL;

        if((inst->mtp_id != USPA_MTP_ID_IMTP) || (inst->mtp_data.imtp == NULL)) {
            continue;
        }

        if(flag == USPI_CON_AGENT) {
            con_listen = inst->mtp_data.imtp->con_controller;
        } else {
            con_listen = inst->mtp_data.imtp->con_agent;
        }
        amxc_llist_for_each(it, &con_listen->accepted_cons) {
            uspi_con_t* current = (uspi_con_t*) amxc_container_of(it, uspi_con_t, lit);
            if((current->eid != NULL)) {
                if(strcmp(eid, current->eid) == 0) {
                    con = current;
                    goto exit;
                }
            }
        }
    }

exit:
    return con;
}

void uspa_mtp_imtp_delete(uspa_mtp_imtp_t** imtp) {
    when_null(imtp, exit);
    when_null(*imtp, exit);

    uspa_mtp_imtp_con_remove((*imtp)->con_agent);
    uspa_mtp_imtp_con_remove((*imtp)->con_controller);

    free(*imtp);
    *imtp = NULL;

exit:
    return;
}

// Socket path will be a directory + socket
static int uspa_mtp_imtp_create_socket_dir(const char* socket_path) {
    int retval = -1;
    uint32_t slash_last = 0;
    int slash_pos = 0;
    amxc_string_t socket_dir;

    amxc_string_init(&socket_dir, 0);
    amxc_string_setf(&socket_dir, "%s", socket_path);

    slash_pos = amxc_string_search(&socket_dir, "/", slash_last + 1);
    while(slash_pos >= 0) {
        slash_last = slash_pos;
        slash_pos = amxc_string_search(&socket_dir, "/", slash_last + 1);
    }

    amxc_string_remove_at(&socket_dir, slash_last, UINT32_MAX);
    retval = amxp_dir_make(amxc_string_get(&socket_dir, 0), 0744);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to create dir %s", amxc_string_get(&socket_dir, 0));
    }

    amxc_string_clean(&socket_dir);
    return retval;
}

static int uspa_mtp_imtp_fetch_uri(amxc_string_t* uri, const char* mtp_inst) {
    int retval = -1;
    amxb_bus_ctx_t* bus_ctx = amxb_be_who_has("LocalAgent.");
    const char* socket_ref = NULL;
    const char* socket_path = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_string_setf(uri, "%sUDS.UnixDomainSocketRef", mtp_inst);

    retval = amxb_get(bus_ctx, amxc_string_get(uri, 0), 0, &ret, 5);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to get %s", amxc_string_get(uri, 0));
        goto exit;
    }

    socket_ref = GETP_CHAR(&ret, "0.0.UnixDomainSocketRef");
    if((socket_ref == NULL) || (*socket_ref == 0)) {
        SAH_TRACEZ_ERROR(ME, "UnixDomainSocketRef is empty");
        goto exit;
    }

    amxc_string_setf(uri, "%s.Path", socket_ref);
    if(uspa_prefix_check_device(amxc_string_get(uri, 0))) {
        uspa_prefix_device_strip(uri);
    }

    retval = amxb_get(bus_ctx, amxc_string_get(uri, 0), 0, &ret, 5);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to get %s", amxc_string_get(uri, 0));
        goto exit;
    }

    socket_path = GETP_CHAR(&ret, "0.0.Path");
    if((socket_path == NULL) || (*socket_path == 0)) {
        SAH_TRACEZ_ERROR(ME, "UnixDomainSocket Path is empty");
        goto exit;
    }

    retval = uspa_mtp_imtp_create_socket_dir(socket_path);
    when_failed(retval, exit);

    amxc_string_setf(uri, "usp:%s", socket_path);
    retval = 0;
exit:
    amxc_var_clean(&ret);
    return retval;
}

static char* uspa_mtp_get_directory(const char* path) {
    char* path_copy = NULL;
    char* dir = NULL;
    char* dir_copy = NULL;

    path_copy = malloc(strlen(path) + 1);
    when_null(path_copy, exit);
    strcpy(path_copy, path);

    dir = dirname(path_copy);
    dir_copy = malloc(strlen(dir) + 1);
    when_null(dir_copy, exit);

    strcpy(dir_copy, dir);

exit:
    free(path_copy);
    return dir_copy;
}

static int uspa_mtp_imtp_change_permissions(const char* uri) {
    int retval = -1;
    char* path = NULL;
    char* directory = NULL;

    imtp_uri_parse(uri, NULL, NULL, NULL, &path);
    when_null(path, exit);

    directory = uspa_mtp_get_directory(path);
    when_null(directory, exit);

    retval = chmod(directory, 0777);
    when_failed(retval, exit);
    retval = chmod(path, 0777);
    when_failed(retval, exit);

exit:
    free(path);
    free(directory);

    return retval;
}

int uspa_mtp_imtp_configure(uspa_mtp_instance_t* mtp_inst) {
    amxo_parser_t* parser = uspa_get_parser();
    uspi_con_t* con_agent = NULL;
    uspi_con_t* con_controller = NULL;
    uspa_mtp_imtp_t* imtp = NULL;
    int retval = -1;
    amxc_string_t path;
    amxc_string_t uri;

    amxc_string_init(&path, 0);
    amxc_string_init(&uri, 0);

    retval = uspa_mtp_imtp_fetch_uri(&uri, amxc_htable_it_get_key(&mtp_inst->hit));
    when_failed(retval, exit);

    SAH_TRACEZ_INFO(ME, "Listening for connections on uri: %s", amxc_string_get(&uri, 0));
    retval = uspi_con_listen(&con_agent, amxc_string_get(&uri, 0));
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to create listen socket at %s", amxc_string_get(&uri, 0));
        goto exit;
    }

    if(uspa_mtp_imtp_change_permissions(amxc_string_get(&uri, 0))) {
        SAH_TRACEZ_ERROR(ME, "Cannot change access permissions for %s", amxc_string_get(&uri, 0));
    }

    uspi_con_flag_toggle(con_agent, USPI_CON_AGENT);
    amxo_connection_add(parser, uspi_con_get_fd(con_agent), uspa_mtp_imtp_accept,
                        amxc_string_get(&uri, 0), AMXO_CUSTOM, con_agent);

    retval = amxc_string_replace(&uri, "agent", "controller", UINT32_MAX);
    if(retval <= 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to determine controller socket based on agent socket");
        retval = -1;
        amxo_connection_remove(parser, uspi_con_get_fd(con_agent));
        uspi_con_disconnect(&con_agent);
        goto exit;
    }

    retval = uspi_con_listen(&con_controller, amxc_string_get(&uri, 0));
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to create listen socket at %s", amxc_string_get(&uri, 0));
        amxo_connection_remove(parser, uspi_con_get_fd(con_agent));
        uspi_con_disconnect(&con_agent);
        goto exit;
    }

    if(uspa_mtp_imtp_change_permissions(amxc_string_get(&uri, 0))) {
        SAH_TRACEZ_ERROR(ME, "Cannot change access permissions for %s", amxc_string_get(&uri, 0));
    }

    uspi_con_flag_toggle(con_controller, USPI_CON_CONTROLLER);
    amxo_connection_add(parser, uspi_con_get_fd(con_controller), uspa_mtp_imtp_accept,
                        amxc_string_get(&uri, 0), AMXO_CUSTOM, con_controller);

    retval = uspa_mtp_imtp_new(&imtp, con_agent, con_controller);
    when_failed(retval, exit);
    mtp_inst->mtp_data.imtp = imtp;

    uspa_object_update_status(amxc_htable_it_get_key(&mtp_inst->hit), "Up");

exit:
    amxc_string_clean(&uri);
    amxc_string_clean(&path);
    return retval;
}

void uspa_mtp_imtp_remove(const char* la_mtp) {
    uspa_mtp_instance_t* mtp_inst = NULL;
    amxc_htable_t* mtp_instances = uspa_mtp_get_instances();
    amxc_htable_it_t* hit = amxc_htable_get(mtp_instances, la_mtp);

    when_null(hit, exit);

    mtp_inst = amxc_container_of(hit, uspa_mtp_instance_t, hit);
    uspa_mtp_imtp_delete(&mtp_inst->mtp_data.imtp);
    mtp_inst->mtp_data.imtp = NULL;
    mtp_inst->mtp_id = USPA_MTP_ID_NULL;

exit:
    return;
}

int uspa_mtp_imtp_subscription_forward(amxc_var_t* forwarding_list, amxc_var_t* params) {
    amxb_bus_ctx_t* ctx_base = NULL;
    amxc_var_t ret;
    int retval = 0;
    const amxc_llist_t* fw_list = amxc_var_constcast(amxc_llist_t, forwarding_list);

    amxc_var_init(&ret);
    uspa_mtp_imtp_subscription_filter_params(params);

    while(!amxc_llist_is_empty(fw_list)) {
        bool first = true;
        amxc_var_t* ref_list = amxc_var_add_key(amxc_llist_t, params, "ReferenceList", NULL);
        amxc_var_for_each(ref, forwarding_list) {
            const char* path = amxc_var_constcast(cstring_t, ref);
            amxb_bus_ctx_t* ctx_current = uspa_discovery_ctx_from_imtp(path);
            if(first) {
                ctx_base = ctx_current;
                first = false;
            }
            if(ctx_current == ctx_base) {
                amxc_var_add(cstring_t, ref_list, path);
                amxc_var_delete(&ref);
            } else if(ctx_current == NULL) {
                SAH_TRACEZ_ERROR(ME, "Cannot create subscription for %s. Path not found", path);
                amxc_var_delete(&ref);
            }
        }

        SAH_TRACEZ_INFO(ME, "Forwarding subscription on fd: [%d]", amxb_get_fd(ctx_base));
        retval = amxb_add(ctx_base, "Device.LocalAgent.Subscription.", 0, NULL, params, &ret, 5);
        if(retval != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to forward subscription on IMTP");
        }
        amxc_var_delete(&ref_list);
    }

    amxc_var_clean(&ret);
    return retval;
}

int uspa_mtp_imtp_subscription_remove(amxc_var_t* forwarding_list, uspa_sub_t* sub) {
    amxc_string_t sub_path;
    amxc_var_t ret;
    int retval = -1;

    amxc_string_init(&sub_path, 0);
    amxc_var_init(&ret);

    amxc_string_setf(&sub_path, "LocalAgent.Subscription.[ID == '%s'].", sub->id);
    amxc_var_for_each(ref, forwarding_list) {
        amxb_bus_ctx_t* ctx = uspa_discovery_ctx_from_imtp(amxc_var_constcast(cstring_t, ref));
        amxb_del(ctx, amxc_string_get(&sub_path, 0), 0, NULL, &ret, 5);
    }

    amxc_string_clean(&sub_path);
    amxc_var_clean(&ret);
    return retval;
}
