/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <usp/uspl.h>

#include "uspa_msghandler.h"
#include "uspa_prefix_check.h"
#include "uspa_role.h"
#include "uspa_controller.h"
#include "uspa_utils.h"

static int uspa_validate_del(amxb_bus_ctx_t* ctx,
                             const char* obj_path,
                             const char* acl_file) {
    amxd_path_t path;
    amxc_var_t* acls = NULL;
    amxc_var_t resolved;
    char* fixed_part = NULL;
    int retval = -1;
    amxc_string_t resolved_path;

    amxc_string_init(&resolved_path, 0);
    amxc_var_init(&resolved);
    amxd_path_init(&path, obj_path);

    when_null_status(ctx, exit, retval = USP_ERR_OBJECT_DOES_NOT_EXIST);

    acls = amxa_parse_files(acl_file);
    when_null(acls, exit);
    fixed_part = amxd_path_get_fixed_part(&path, false);
    amxa_resolve_search_paths(ctx, acls, fixed_part);

    if(amxd_path_is_search_path(&path)) {
        amxb_resolve(ctx, &path, &resolved);
        amxc_var_for_each(var, &resolved) {
            amxc_string_setf(&resolved_path, "%s", amxc_var_constcast(cstring_t, var));
            // Return amxd_status_permission_denied if the controller doesn't have delete permissions
            // and return 0 in all other situations. The amxb_del will provide the correct return value
            // later on.
            retval = amxa_is_del_allowed_v2(ctx, acls, amxc_string_get(&resolved_path, 0));
            if(retval != 0) {
                SAH_TRACEZ_WARNING(ME, "USP delete not allowed for acl_file '%s', error code [%d, %s]", acl_file, retval, amxd_status_string(retval));
                when_true((retval == amxd_status_permission_denied), exit);
            }
        }
    } else {
        retval = amxa_is_del_allowed_v2(ctx, acls, obj_path);
        if(retval != 0) {
            SAH_TRACEZ_WARNING(ME, "USP delete not allowed for acl_file '%s', error code [%d, %s]", acl_file, retval, amxd_status_string(retval));
            when_true((retval == amxd_status_permission_denied), exit);
        }
    }

    retval = 0;
exit:
    free(fixed_part);
    amxd_path_clean(&path);
    amxc_var_delete(&acls);
    amxc_var_clean(&resolved);
    amxc_string_clean(&resolved_path);

    return retval;
}

static void uspa_delete_fill_del_operation_response(int status, amxc_var_t* result) {
    // Appropriate error codes for the Delete Message include 7000-7008, 7015, 7016, 7018, 7024, 7026 and 7800-7999.
    switch(status) {
    case amxd_status_ok:
        amxc_var_add_key(uint32_t, result, "err_code", USP_ERR_OK);
        break;

    case amxd_status_permission_denied:
    default:
        amxc_var_add_key(uint32_t, result, "err_code", status);
        amxc_var_add_key(cstring_t, result, "err_msg", amxd_status_string(status));
        break;
    }
}

static int uspa_invoke_delete(amxb_bus_ctx_t* ctx,
                              amxc_llist_t* resp_list,
                              const char* path,
                              const char* acl_file,
                              uint32_t access) {
    SAH_TRACEZ_INFO(ME, "Invoke delete on path: [%s]", path);
    int retval = -1;
    amxc_var_t* affected = NULL;
    amxc_var_t* result = NULL;
    amxc_var_t* reply = NULL;
    amxc_string_t path_string;

    amxc_string_init(&path_string, 0);
    amxc_string_setf(&path_string, "%s", path);
    amxc_var_new(&reply);
    amxc_var_set_type(reply, AMXC_VAR_ID_HTABLE);
    affected = amxc_var_add_key(amxc_llist_t, reply, "affected_paths", NULL);
    amxc_var_add_key(amxc_llist_t, reply, "unaffected_paths", NULL);
    result = amxc_var_add_key(amxc_htable_t, reply, "result", NULL);
    amxc_var_add_key(cstring_t, result, "requested_path", path);

    amxb_set_access(ctx, access);
    retval = uspa_validate_del(ctx, amxc_string_get(&path_string, 0), acl_file);
    if(amxd_status_ok == retval) {
        retval = amxb_del(ctx, amxc_string_get(&path_string, 0), 0, NULL, affected, 5);
    }

    uspa_delete_fill_del_operation_response(retval, result);

    amxb_set_access(ctx, AMXB_PROTECTED);
    amxc_llist_append(resp_list, &reply->lit);
    amxc_string_clean(&path_string);
    return retval;
}

static int uspa_del_response(uspl_rx_t* usp_rx,
                             uspl_tx_t** usp_tx,
                             amxc_llist_t* del_output) {
    int retval = -1;
    char* msg_id = NULL;
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    amxc_llist_for_each(it, del_output) {
        amxc_var_t* record = amxc_var_from_llist_it(it);
        amxc_var_t* single_result = NULL;
        amxc_var_t* result = NULL;
        amxc_var_t* del_result = GET_ARG(record, "result");
        const char* requested_path = GET_CHAR(del_result, "requested_path");
        uint32_t error_code = GET_UINT32(del_result, "err_code");
        uint32_t err_code_uspl = uspl_set_amxd_status_to_usp_param_error(error_code);
        const char* err_msg = uspl_error_code_to_str(err_code_uspl);

        amxc_var_new(&single_result);
        amxc_var_set_type(single_result, AMXC_VAR_ID_HTABLE);

        result = amxc_var_add_key(amxc_htable_t, single_result, "result", NULL);
        amxc_var_add_key(cstring_t, result, "requested_path", requested_path);
        amxc_var_add_key(uint32_t, result, "err_code", err_code_uspl);
        if(amxd_status_ok == error_code) {
            amxc_var_move(single_result, record);
            amxc_var_add_key(amxc_llist_t, single_result, "unaffected_paths", NULL);
        } else {
            amxc_var_add_key(cstring_t, result, "err_msg", err_msg);
        }
        amxc_llist_append(&resp_list, &single_result->lit);
    }

    retval = uspa_msghandler_build_reply_basic(usp_rx, usp_tx);
    when_failed(retval, exit);

    msg_id = uspl_msghandler_msg_id(usp_rx);
    when_null(msg_id, exit);

    retval = uspl_delete_resp_new(*usp_tx, &resp_list, msg_id);

exit:
    amxc_llist_clean(&resp_list, variant_list_it_free);
    return retval;
}



int uspa_handle_delete(uspl_rx_t* usp_rx,
                       uspl_tx_t** usp_tx,
                       const char* acl_file) {
    SAH_TRACEZ_INFO(ME, "Handle USP message of type Delete");
    int retval = -1;
    amxc_var_t request;
    const amxc_llist_t* req_list = NULL;
    amxc_llist_t resp_list;
    amxc_var_t* requests = NULL;
    uint32_t access = AMXB_PUBLIC;

    amxc_var_init(&request);
    amxc_llist_init(&resp_list);

    when_null(usp_rx, exit);
    when_null(usp_tx, exit);

    when_false(uspl_delete_extract(usp_rx, &request) == 0, exit);

    if(!GET_BOOL(&request, "allow_partial") && !uspa_utils_ignore_allow_partial()) {
        retval = uspa_msghandler_build_error(usp_rx, usp_tx, USP_ERR_RESOURCES_EXCEEDED);
        goto exit;
    }

    access = uspa_controller_get_access(uspl_msghandler_from_id(usp_rx));
    requests = amxc_var_get_key(&request, "requests", AMXC_VAR_FLAG_DEFAULT);
    req_list = amxc_var_constcast(amxc_llist_t, requests);
    amxc_llist_iterate(it, req_list) {
        const char* path = amxc_var_constcast(cstring_t, amxc_var_from_llist_it(it));
        amxb_bus_ctx_t* ctx = uspa_discovery_get_ctx(path);
        retval = uspa_invoke_delete(ctx, &resp_list, path, acl_file, access);
        if(retval != 0) {
            SAH_TRACEZ_WARNING(ME, "Invoke delete on path: [%s] returned (%d: %s)", path, retval, amxd_status_string(retval));
        }
    }
    retval = uspa_del_response(usp_rx, usp_tx, &resp_list);

exit:
    amxc_var_clean(&request);
    amxc_llist_clean(&resp_list, variant_list_it_free);

    return retval;
}
