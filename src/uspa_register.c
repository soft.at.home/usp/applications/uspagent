/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>

#include <usp/uspl.h>
#include <usp/usp_err_codes.h>
#include <uspi/uspi_discovery.h>

#include "uspa.h"
#include "uspa_msghandler.h"
#include "uspa_mtp_imtp.h"
#include "uspa_discovery.h"
#include "uspa_error.h"

static int register_build_register_response(amxc_var_t* results,
                                            uspl_tx_t* usp_tx,
                                            const char* msg_id) {
    int retval = -1;
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);

    amxc_var_for_each(res, results) {
        amxc_var_t* resp_entry = NULL;
        const char* req_path = amxc_var_key(res);
        int32_t status = amxc_var_constcast(int32_t, res);

        amxc_var_new(&resp_entry);
        amxc_var_set_type(resp_entry, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, resp_entry, "requested_path", req_path);
        if(status == 0) {
            amxc_var_t* oper_success = amxc_var_add_key(amxc_htable_t, resp_entry, "oper_success", NULL);
            // Note that requested and registered path will be identical for now.
            // Can change later when instances are registered...
            amxc_var_add_key(cstring_t, oper_success, "registered_path", req_path);
            amxc_var_add_key(uint32_t, resp_entry, "oper_status_case", USP__REGISTER_RESP__REGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_SUCCESS);
        } else {
            amxc_var_t* oper_failure = amxc_var_add_key(amxc_htable_t, resp_entry, "oper_failure", NULL);
            // TODO update error codes and messages when more information is available in spec
            amxc_var_add_key(uint32_t, oper_failure, "err_code", USP_ERR_REGISTER_FAILURE);
            amxc_var_add_key(cstring_t, oper_failure, "err_msg", "Registration failed");
            amxc_var_add_key(uint32_t, resp_entry, "oper_status_case", USP__REGISTER_RESP__REGISTERED_PATH_RESULT__OPERATION_STATUS__OPER_STATUS_OPER_FAILURE);
        }
        amxc_llist_append(&resp_list, &resp_entry->lit);
    }

    retval = uspl_register_resp_new(usp_tx, &resp_list, msg_id);

    amxc_llist_clean(&resp_list, variant_list_it_free);
    return retval;
}

static void register_rollback_registration(amxb_bus_ctx_t* ctx,
                                           const char* eid,
                                           amxc_var_t* results) {
    amxc_var_for_each(res, results) {
        const char* path = amxc_var_key(res);
        uspi_discovery_del_registration(ctx, eid, path);
    }
}

static int register_build_register_error(amxc_var_t* results,
                                         uspl_tx_t* usp_tx,
                                         const char* msg_id) {
    int retval = -1;
    amxc_var_t error;

    amxc_var_init(&error);

    uspa_error_top_level(&error, USP_ERR_REGISTER_FAILURE, "Registration failed");

    amxc_var_for_each(res, results) {
        const char* requested_path = amxc_var_key(res);
        uspa_error_append_param(&error, requested_path, NULL, USP_ERR_REGISTER_FAILURE,
                                "Registration failed");
    }

    retval = uspl_error_resp_new(usp_tx, &error, msg_id);

    amxc_var_clean(&error);
    return retval;
}

static int register_build_response(uspl_rx_t* usp_rx,
                                   amxc_var_t* results,
                                   int reg_rv,
                                   bool allow_partial,
                                   uspl_tx_t** usp_tx) {
    int retval = -1;
    const char* from_id = uspl_msghandler_from_id(usp_rx);
    const char* to_id = uspl_msghandler_to_id(usp_rx);
    const char* msg_id = uspl_msghandler_msg_id(usp_rx);

    retval = uspl_tx_new(usp_tx, to_id, from_id);
    when_failed(retval, exit);

    // If success or allow_partial==true, send a response
    if((reg_rv == 0) || ((reg_rv != 0) && allow_partial)) {
        retval = register_build_register_response(results, *usp_tx, msg_id);
        when_failed(retval, exit);
    } else {
        retval = register_build_register_error(results, *usp_tx, msg_id);
        when_failed(retval, exit)
    }

exit:
    return retval;
}

int uspa_handle_register(uspl_rx_t* usp_rx,
                         uspl_tx_t** usp_tx,
                         UNUSED const char* acl_file) {
    SAH_TRACEZ_INFO(ME, "Handle USP message of type Register");
    int retval = -1;
    amxc_var_t request;
    amxc_var_t results;
    amxc_var_t* reg_paths = NULL;
    bool allow_partial = false;
    const char* eid = NULL;

    amxc_var_init(&request);
    amxc_var_init(&results);
    amxc_var_set_type(&results, AMXC_VAR_ID_HTABLE);

    when_null(usp_rx, exit);
    when_null(usp_tx, exit);

    retval = uspl_register_extract(usp_rx, &request);
    when_failed(retval, exit);

    eid = uspl_msghandler_from_id(usp_rx);

    reg_paths = GET_ARG(&request, "reg_paths");
    when_null(reg_paths, exit);

    allow_partial = GET_BOOL(&request, "allow_partial");

    amxc_var_for_each(entry, reg_paths) {
        const char* path = GET_CHAR(entry, "path");
        amxb_bus_ctx_t* ctx = amxb_be_who_has("Discovery.");
        retval = uspi_discovery_add_registration(ctx, eid, path);
        if((retval != 0) && !allow_partial) {
            register_rollback_registration(ctx, eid, &results);
            amxc_var_add_key(int32_t, &results, path, retval);
            break;
        } else {
            amxc_var_add_key(int32_t, &results, path, retval);
        }
    }

    retval = register_build_response(usp_rx, &results, retval, allow_partial, usp_tx);

exit:
    amxc_var_clean(&request);
    amxc_var_clean(&results);
    return retval;
}
