/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>

#include <usp/uspl.h>

#include "uspa.h"
#include "uspa_msghandler.h"
#include "uspa_controller.h"

static void uspa_filter_gsdm_resp(const char* requested_path,
                                  amxc_var_t* response,
                                  amxc_var_t* result,
                                  const char* acl_file) {
    amxc_var_t* acls = NULL;
    int rv = 0;

    acls = amxa_parse_files(acl_file);
    when_null_status(acls, exit, rv = amxd_status_permission_denied);

    rv = amxa_filter_get_supported_resp(requested_path, response, acls);

exit:
    if(rv == 0) {
        amxc_var_add_key(uint32_t, result, "err_code", 0);
    } else {
        amxc_var_add_key(uint32_t, result, "err_code", USP_ERR_INVALID_PATH);
    }
    amxc_var_delete(&acls);
}

static void uspa_invoke_get_supported(amxb_bus_ctx_t* ctx,
                                      amxc_llist_t* resp_list,
                                      const char* path,
                                      uint32_t flags,
                                      uint32_t access,
                                      const char* acl_file) {
    SAH_TRACEZ_INFO(ME, "Invoke GSDM on path: [%s]", path);
    int retval = -1;
    amxc_var_t* reply = NULL;
    amxc_var_t* resolved = NULL;
    amxc_var_t* result = NULL;
    amxc_var_t rv;

    amxc_var_init(&rv);
    amxc_var_set_type(&rv, AMXC_VAR_ID_HTABLE);
    amxc_var_new(&resolved);

    amxb_set_access(ctx, access);
    retval = amxb_get_supported(ctx, path, flags, &rv, 5);
    reply = GETI_ARG(&rv, 0);
    result = amxc_var_add_key(amxc_htable_t, reply, "result", NULL);
    amxc_var_add_key(cstring_t, result, "requested_path", path);

    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "amxb_get_supported failed with status: [%d] for path: [%s]",
                           retval, path);
        amxc_var_add_key(uint32_t, result, "err_code", USP_ERR_INVALID_PATH);
    } else {
        uspa_filter_gsdm_resp(path, &rv, result, acl_file);
    }

    amxb_set_access(ctx, AMXB_PROTECTED);
    amxc_var_move(resolved, reply);
    amxc_llist_append(resp_list, &resolved->lit);
    amxc_var_clean(&rv);
}

int uspa_handle_get_supported_dm(uspl_rx_t* usp_rx,
                                 uspl_tx_t** usp_tx,
                                 const char* acl_file) {
    SAH_TRACEZ_INFO(ME, "Handle USP message of type Get Supported DM");
    int retval = -1;
    amxc_var_t result;
    const amxc_llist_t* path_list = NULL;
    amxc_llist_t resp_list;
    uint32_t flags = 0;
    amxc_var_t* requests = NULL;
    amxc_var_t* flag_table = NULL;
    uint32_t access = AMXB_PUBLIC;

    amxc_var_init(&result);
    amxc_llist_init(&resp_list);

    when_null(usp_rx, exit);
    when_null(usp_tx, exit);

    retval = uspl_get_supported_dm_extract(usp_rx, &result);
    if(retval != 0) {
        retval = uspa_msghandler_build_error(usp_rx, usp_tx, retval);
        goto exit;
    }

    access = uspa_controller_get_access(uspl_msghandler_from_id(usp_rx));
    requests = amxc_var_get_key(&result, "paths", AMXC_VAR_FLAG_DEFAULT);
    path_list = amxc_var_constcast(amxc_llist_t, requests);

    flag_table = amxc_var_get_key(&result, "flags", AMXC_VAR_FLAG_DEFAULT);
    flags |= GET_BOOL(flag_table, "first_level_only") ? AMXB_FLAG_FIRST_LVL : 0;
    flags |= GET_BOOL(flag_table, "return_commands") ? AMXB_FLAG_FUNCTIONS : 0;
    flags |= GET_BOOL(flag_table, "return_events") ? AMXB_FLAG_EVENTS : 0;
    flags |= GET_BOOL(flag_table, "return_params") ? AMXB_FLAG_PARAMETERS : 0;

    amxc_llist_iterate(it, path_list) {
        const char* path = amxc_var_constcast(cstring_t, amxc_var_from_llist_it(it));
        amxb_bus_ctx_t* ctx = uspa_discovery_get_ctx(path);
        uspa_invoke_get_supported(ctx, &resp_list, path, flags, access, acl_file);
    }

    retval = uspa_msghandler_build_reply(usp_rx, &resp_list, uspl_get_supported_dm_resp_new, usp_tx);

exit:
    amxc_var_clean(&result);
    amxc_llist_clean(&resp_list, variant_list_it_free);

    return retval;
}
