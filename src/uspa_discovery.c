/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "uspa_discovery.h"
#include "uspa_prefix_check.h"
#include "uspa_mtp_imtp.h"

static char* uspa_discovery_get_reg_inst(amxd_path_t* obj_path) {
    amxc_string_t discover_path;
    amxc_var_t ret;
    amxc_var_t* result = NULL;
    char* instance = 0;
    char* part = amxd_path_get_first(obj_path, true);
    const char* param = NULL;

    amxc_var_init(&ret);
    amxc_string_init(&discover_path, 0);
    amxc_string_setf(&discover_path, "Discovery.Service.*.Registration.[Path == '%s", part);

    if(uspa_prefix_check_device(part)) {
        free(part);
        part = amxd_path_get_first(obj_path, true);
        if(part != NULL) {
            amxc_string_appendf(&discover_path, "%s", part);
            free(part);
            part = NULL;
        } else {
            param = amxd_path_get_param(obj_path);
            if(param != NULL) {
                amxc_string_appendf(&discover_path, "%s", param);
            }
        }
    }

    amxc_string_append(&discover_path, "'].", 3);
    amxb_get(amxb_be_who_has("Discovery."), amxc_string_get(&discover_path, 0), 0, &ret, 5);

    result = GETP_ARG(&ret, "0.0");
    if(result != NULL) {
        instance = strdup(amxc_var_key(result));
    }

    free(part);
    amxc_var_clean(&ret);
    amxc_string_clean(&discover_path);

    return instance;
}

static char* uspa_discovery_get_eid_from_path(const char* reg_inst, bool strip) {
    amxd_path_t path;
    char* eid_str = NULL;
    const char* path_get = NULL;
    amxc_var_t* eid = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);

    amxd_path_init(&path, reg_inst);
    if(strip) {
        free(amxd_path_get_last(&path, true));
        free(amxd_path_get_last(&path, true));
    }

    path_get = amxd_path_get(&path, AMXD_OBJECT_TERMINATE);

    amxb_get(amxb_be_who_has(path_get), path_get, 0, &ret, 5);
    eid = GETP_ARG(&ret, "0.0.EndpointID");
    if(eid != NULL) {
        eid_str = amxc_var_dyncast(cstring_t, eid);
    }

    amxc_var_clean(&ret);
    amxd_path_clean(&path);

    return eid_str;
}

static char* uspa_discovery_get_eid_from_reg_inst(const char* reg_inst) {
    return uspa_discovery_get_eid_from_path(reg_inst, true);
}

static char* uspa_discovery_get_eid_from_service(const char* service_inst) {
    return uspa_discovery_get_eid_from_path(service_inst, false);
}

static amxc_var_t* uspa_discovery_get_service(const char* service_inst) {
    char* eid = uspa_discovery_get_eid_from_service(service_inst);
    amxc_var_t* result = NULL;
    amxc_string_t usp_service;
    amxc_var_t ret;

    amxc_var_new(&result);
    amxc_var_init(&ret);
    amxc_string_init(&usp_service, 0);
    amxc_string_setf(&usp_service, "USPServices.USPService.[EndpointID == '%s'].", eid);

    SAH_TRACEZ_INFO(ME, "Try to get %s", amxc_string_get(&usp_service, 0));
    amxb_get(amxb_be_who_has("USPServices"), amxc_string_get(&usp_service, 0), 0, &ret, 5);
    amxc_var_move(result, GETI_ARG(&ret, 0));

    amxc_var_clean(&ret);
    amxc_string_clean(&usp_service);
    free(eid);
    return result;
}

amxb_bus_ctx_t* uspa_discovery_ctx_from_imtp(const char* path) {
    amxb_bus_ctx_t* be_ctx = NULL;
    char* registered_instance = NULL;
    char* eid = NULL;
    uspi_con_t* con = NULL;
    amxd_path_t obj_path;

    amxd_path_init(&obj_path, path);
    registered_instance = uspa_discovery_get_reg_inst(&obj_path);

    when_str_empty(registered_instance, exit);

    eid = uspa_discovery_get_eid_from_reg_inst(registered_instance);
    when_str_empty(eid, exit);

    con = uspa_mtp_imtp_get_con_from_eid(eid, USPI_CON_AGENT);
    when_null(con, exit);

    be_ctx = con->imtp_ctx;

exit:
    amxd_path_clean(&obj_path);
    free(eid);
    free(registered_instance);
    return be_ctx;
}

bool uspa_discovery_object_is_remote(const char* path) {
    bool retval = false;
    char* reg_inst = NULL;
    amxd_path_t obj_path;

    amxd_path_init(&obj_path, NULL);

    when_str_empty(path, exit);

    amxd_path_setf(&obj_path, false, "%s", path);

    reg_inst = uspa_discovery_get_reg_inst(&obj_path);
    when_str_empty(reg_inst, exit);

    retval = true;
exit:
    amxd_path_clean(&obj_path);
    free(reg_inst);
    return retval;
}

amxb_bus_ctx_t* uspa_discovery_get_ctx(const char* path) {
    amxb_bus_ctx_t* bus_ctx = NULL;
    amxd_path_t obj_path;

    amxd_path_init(&obj_path, path);
    amxd_path_setf(&obj_path, false, "%s", path);

    bus_ctx = uspa_discovery_ctx_from_imtp(amxd_path_get(&obj_path, AMXD_OBJECT_TERMINATE));
    if(bus_ctx == NULL) {
        bus_ctx = amxb_be_who_has(path);
    }

    amxd_path_clean(&obj_path);
    return bus_ctx;
}

void _uspa_discovery_service_added(UNUSED const char* const sig_name,
                                   const amxc_var_t* const data,
                                   UNUSED void* const priv) {
    const char* path = "USPServices.";
    int retval = -1;
    amxc_var_t args;
    amxc_var_t ret;
    SAH_TRACEZ_INFO(ME, "Add USPService entry");

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "EndpointID", GETP_CHAR(data, "parameters.EndpointID"));

    retval = amxb_call(amxb_be_who_has(path), path, "addService", &args, &ret, 5);
    SAH_TRACEZ_INFO(ME, "%saddService returned with %d", path, retval);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);
}

void _uspa_discovery_service_removed(UNUSED const char* const sig_name,
                                     const amxc_var_t* const data,
                                     UNUSED void* const priv) {
    const char* path = "USPServices.";
    int retval = -1;
    amxc_var_t args;
    amxc_var_t ret;
    SAH_TRACEZ_INFO(ME, "Remove USPService entry");

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "EndpointID", GETP_CHAR(data, "parameters.EndpointID"));

    retval = amxb_call(amxb_be_who_has(path), path, "delService", &args, &ret, 5);
    SAH_TRACEZ_INFO(ME, "%sdelService returned with %d", path, retval);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);
}

void _uspa_discovery_service_registered(UNUSED const char* const sig_name,
                                        const amxc_var_t* const data,
                                        UNUSED void* const priv) {
    const char* new_path = GETP_CHAR(data, "parameters.Path");
    const char* reg_paths = NULL;
    char* service_path = NULL;
    amxc_string_t data_model_paths;
    amxd_path_t discovery_path;
    amxc_var_t* reg_paths_var = NULL;
    amxc_var_t* service = NULL;
    amxc_var_t* service_params = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t ret;
    amxc_var_t args;
    int retval = -1;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_string_init(&data_model_paths, 0);
    amxd_path_init(&discovery_path, GET_CHAR(data, "path"));
    free(amxd_path_get_last(&discovery_path, true));

    SAH_TRACEZ_INFO(ME, "Get USPService parameters corresponding with %s", amxd_path_get(&discovery_path, AMXD_OBJECT_TERMINATE));
    service = uspa_discovery_get_service(amxd_path_get(&discovery_path, AMXD_OBJECT_TERMINATE));
    when_null(service, exit);
    service_params = GETI_ARG(service, 0);
    when_null(service_params, exit);

    service_path = strdup(amxc_var_key(service_params));
    SAH_TRACEZ_INFO(ME, "Update %s parameters", service_path);

    params = amxc_var_add_key(amxc_htable_t, &args, "params", NULL);
    amxc_var_move(params, service_params);

    reg_paths_var = GET_ARG(params, "DataModelPaths");
    reg_paths = amxc_var_constcast(cstring_t, reg_paths_var);
    if((reg_paths != NULL) && (*reg_paths != 0)) {
        amxc_string_setf(&data_model_paths, "%s,%s", reg_paths, new_path);
    } else {
        amxc_string_setf(&data_model_paths, "%s", new_path);
    }
    amxc_var_set(cstring_t, reg_paths_var, amxc_string_get(&data_model_paths, 0));

    retval = amxb_call(amxb_be_who_has(service_path), service_path, "setParams", &args, &ret, 5);
    SAH_TRACEZ_INFO(ME, "%ssetParams returned with %d", service_path, retval);

exit:
    free(service_path);
    amxc_string_clean(&data_model_paths);
    amxd_path_clean(&discovery_path);
    amxc_var_delete(&service);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
