/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>

#include "uspa_mtp.h"
#include "uspa_mtp_mqtt.h"
#include "uspa_mtp_imtp.h"
#include "uspa_controller.h"
#include "uspa_imtp.h"
#include "uspa_object.h"
#include "uspa_utils.h"

static amxc_htable_t mtp_instances;        // Track mtp instances: key = LocalAgent.MTP.{i}., value = uspa_mtp_instance_t struct

// Add uspa_mtp_protocol_changed

static void uspa_mtp_enabled(const char* mtp);

static void uspa_mtp_connect_retry(UNUSED amxp_timer_t* timer, void* priv) {
    uspa_mtp_instance_t* mtp_inst = (uspa_mtp_instance_t*) priv;
    const char* la_mtp = amxc_htable_it_get_key(&mtp_inst->hit);

    if(!uspa_object_get_value(bool, la_mtp, "Enable")) {
        SAH_TRACEZ_INFO(ME, "%s disabled, will not reconnect IMTP", la_mtp);
        goto exit;
    }
    SAH_TRACEZ_INFO(ME, "Retry connecting to %s", la_mtp);
    uspa_mtp_enabled(la_mtp);

exit:
    amxp_timer_delete(&mtp_inst->socket_retry_timer);
    mtp_inst->socket_retry_timer = NULL;
    return;
}

static void uspa_mtp_socket_status_changed(UNUSED const char* const sig_name,
                                           const amxc_var_t* const data,
                                           UNUSED void* const priv) {
    const char* la_mtp = GET_CHAR(data, "path");
    const char* status = GETP_CHAR(data, "parameters.SocketStatus.to");
    amxc_htable_it_t* it = NULL;
    uspa_mtp_instance_t* mtp_inst = NULL;

    SAH_TRACEZ_INFO(ME, "%sSocketStatus changed to %s", la_mtp, status);
    it = amxc_htable_get(&mtp_instances, la_mtp);
    when_null(it, exit);

    mtp_inst = amxc_container_of(it, uspa_mtp_instance_t, hit);

    if((strcmp(status, "Error") == 0) || (strcmp(status, "Down") == 0)) {
        // Start retry timer if connection is down or not correctly established
        if(mtp_inst->socket_retry_timer == NULL) {
            amxp_timer_new(&mtp_inst->socket_retry_timer, uspa_mtp_connect_retry, mtp_inst);
            amxp_timer_start(mtp_inst->socket_retry_timer, 10000);
        }
    } else if(strcmp(status, "Up") == 0) {
        // Delete timer if connection is up now
        if(mtp_inst->socket_retry_timer != NULL) {
            amxp_timer_delete(&mtp_inst->socket_retry_timer);
            mtp_inst->socket_retry_timer = NULL;
        }
    }

exit:
    return;
}

static int uspa_mtp_instance_new(uspa_mtp_instance_t** inst, const char* la_mtp) {
    int retval = -1;
    amxc_string_t la_mtp_path;

    when_null(inst, exit);
    when_str_empty(la_mtp, exit);

    amxc_string_init(&la_mtp_path, 0);
    amxc_string_setf(&la_mtp_path, "%s", la_mtp);
    amxc_string_trimr(&la_mtp_path, uspa_utils_is_dot);
    when_true(amxc_string_is_empty(&la_mtp_path), exit);

    *inst = (uspa_mtp_instance_t*) calloc(1, sizeof(uspa_mtp_instance_t));
    when_null(*inst, exit);

    (*inst)->la_mtp = strdup(amxc_string_get(&la_mtp_path, 0));
    if((*inst)->la_mtp == NULL) {
        free(*inst);
        *inst = NULL;
        goto exit;
    }

    retval = 0;
exit:
    amxc_string_clean(&la_mtp_path);
    return retval;
}

static void uspa_mtp_instance_delete(uspa_mtp_instance_t** inst) {
    uspi_con_t* con = NULL;

    when_null(inst, exit);
    when_null(*inst, exit);


    if(((*inst)->mtp_id == USPA_MTP_ID_MQTT) && ((*inst)->mtp_data.mqtt != NULL)) {
        con = (*inst)->mtp_data.mqtt->con;
        uspa_imtp_disconnect(con, USPA_MTP_ID_MQTT);
    } else if(((*inst)->mtp_id == USPA_MTP_ID_IMTP) && ((*inst)->mtp_data.imtp != NULL)) {
        uspa_mtp_imtp_delete(&(*inst)->mtp_data.imtp);
    }

    free((*inst)->la_mtp);
    free(*inst);
    *inst = NULL;

exit:
    return;
}

/**
   @brief
   Callback function when a LocalAgent.MTP.{i}. instance is enabled or an enabled instance is added.

   This function will check which MTP protocol is enabled and call the configuration function
   corresponding to that protocol.
 */
static void uspa_mtp_enabled(const char* mtp) {
    int retval = -1;
    amxc_htable_it_t* hit = NULL;
    uspa_mtp_instance_t* mtp_inst = NULL;
    const char* protocol = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);

    uspa_imtp_set_status(mtp, "Connecting");
    retval = amxb_get(amxb_be_who_has("LocalAgent."), mtp, 0, &ret, 5);
    when_failed(retval, exit);

    hit = amxc_htable_get(&mtp_instances, mtp);
    if(hit == NULL) {
        retval = uspa_mtp_instance_new(&mtp_inst, mtp);
        when_failed(retval, exit);
        amxc_htable_insert(&mtp_instances, mtp, &mtp_inst->hit);
    } else {
        mtp_inst = amxc_container_of(hit, uspa_mtp_instance_t, hit);
    }

    protocol = GETP_CHAR(&ret, "0.0.Protocol");
    when_str_empty(protocol, exit);

    if(strcmp(protocol, "MQTT") == 0) {
        retval = uspa_mtp_mqtt_configure(mtp);
        mtp_inst->mtp_id = USPA_MTP_ID_MQTT;
    } else if(strcmp(protocol, "UDS") == 0) {
        retval = uspa_mtp_imtp_configure(mtp_inst);
        mtp_inst->mtp_id = USPA_MTP_ID_IMTP;
    }
    if(retval != 0) {
        SAH_TRACEZ_INFO(ME, "%s with protocol %s was enabled, but not connected", mtp, protocol);
    }

exit:
    if(retval != 0) {
        uspa_imtp_set_status(mtp, "Error");
    }
    amxc_var_clean(&ret);
}

/**
   @brief
   Disables the IMTP connection when an MTP gets disabled.

   This function will be called when a LocalAgent.MTP.{i}. or LocalAgent.Controller.{i}.MTP.{i}.
   instance is disabled.

   @param mtp path to an MTP instance
 */
static void uspa_mtp_disabled(const char* mtp, const char* protocol) {
    if(strcmp(protocol, "MQTT") == 0) {
        uspa_mtp_mqtt_disable(mtp);
    } else if(strcmp(protocol, "UDS") == 0) {
        uspa_mtp_imtp_remove(mtp);
    }
}

static void uspa_mtp_enable_toggled(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    UNUSED void* const priv) {
    int retval = -1;
    const char* mtp = GET_CHAR(data, "path");
    bool enabled = GETP_BOOL(data, "parameters.Enable.to");
    const char* protocol = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);

    retval = amxb_get(amxb_be_who_has("LocalAgent."), mtp, 0, &ret, 5);
    when_failed(retval, exit);

    protocol = GETP_CHAR(&ret, "0.0.Protocol");
    when_str_empty(protocol, exit);

    SAH_TRACEZ_INFO(ME, "Enable of %s is %d", mtp, enabled);
    if(enabled) {
        uspa_mtp_enabled(mtp);
    } else {
        uspa_mtp_disabled(mtp, protocol);
    }

exit:
    amxc_var_clean(&ret);
}

static void uspa_mtp_added(UNUSED const char* const sig_name,
                           const amxc_var_t* const data,
                           UNUSED void* const priv) {
    const char* mtp_template = GET_CHAR(data, "path");
    uint32_t index = GET_UINT32(data, "index");
    amxc_string_t mtp_instance;

    amxc_string_init(&mtp_instance, 0);
    amxc_string_setf(&mtp_instance, "%s%d.", mtp_template, index);
    uspa_mtp_enabled(amxc_string_get(&mtp_instance, 0));

    amxc_string_clean(&mtp_instance);
}

static void uspa_mtp_removed(UNUSED const char* const sig_name,
                             const amxc_var_t* const data,
                             UNUSED void* const priv) {
    const char* mtp_template = GET_CHAR(data, "path");
    const char* protocol = GETP_CHAR(data, "parameters.Protocol");
    uint32_t index = GET_UINT32(data, "index");
    amxc_string_t mtp_instance;
    amxc_htable_it_t* it = NULL;

    amxc_string_init(&mtp_instance, 0);
    amxc_string_setf(&mtp_instance, "%s%d.", mtp_template, index);

    SAH_TRACEZ_INFO(ME, "%s removed", amxc_string_get(&mtp_instance, 0));

    when_str_empty(protocol, exit);
    if(strcmp(protocol, "MQTT") == 0) {
        uspi_con_t* con = uspa_imtp_con_from_la_mtp(amxc_string_get(&mtp_instance, 0), USPA_MTP_ID_MQTT);
        when_null(con, exit);

        uspa_imtp_disconnect(con, USPA_MTP_ID_MQTT);
    } else if(strcmp(protocol, "IMTP") == 0) {
        uspa_mtp_imtp_remove(amxc_string_get(&mtp_instance, 0));
    }

    it = amxc_htable_take(&mtp_instances, amxc_string_get(&mtp_instance, 0));
    amxc_htable_it_clean(it, uspa_mtp_instance_hit_clean);

exit:
    amxc_string_clean(&mtp_instance);
}

int uspa_mtp_subscribe(amxb_bus_ctx_t* ctx) {
    int retval = -1;
    const char* path = "LocalAgent.MTP.";
    const char* expr_changed = "(notification == 'dm:object-changed') && \
                                (contains('parameters.Enable'))";
    const char* expr_added = "(notification == 'dm:instance-added') && (parameters.Enable == true)";
    const char* expr_removed = "(notification == 'dm:instance-removed')";
    const char* expr_status = "(notification == 'dm:object-changed') && \
                               (contains('parameters.SocketStatus'))";

    retval = amxb_subscribe(ctx, path, expr_changed, uspa_mtp_enable_toggled, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to %s changed events", path);
        goto exit;
    }

    retval = amxb_subscribe(ctx, path, expr_added, uspa_mtp_added, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to %s added events", path);
        goto exit;
    }

    retval = amxb_subscribe(ctx, path, expr_removed, uspa_mtp_removed, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to %s added events", path);
        goto exit;
    }

    retval = amxb_subscribe(ctx, path, expr_status, uspa_mtp_socket_status_changed, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to %sSocketStatus events", path);
        goto exit;
    }

exit:
    return retval;
}

int uspa_mtp_init(amxb_bus_ctx_t* ctx) {
    int retval = -1;
    amxc_var_t ret;

    amxc_var_init(&ret);

    amxc_htable_init(&mtp_instances, 0);

    retval = amxb_get(ctx, "LocalAgent.MTP.", 0, &ret, 5);

    amxc_var_for_each(mtp, GETI_ARG(&ret, 0)) {
        const char* mtp_path = amxc_var_key(mtp);
        uspa_mtp_instance_t* mtp_inst = NULL;

        if(GET_CHAR(mtp, "Alias") == NULL) {
            continue;
        }

        retval = uspa_mtp_instance_new(&mtp_inst, mtp_path);
        when_failed(retval, exit);

        amxc_htable_insert(&mtp_instances, mtp_path, &mtp_inst->hit);

        if(GET_BOOL(mtp, "Enable")) {
            uspa_mtp_enabled(mtp_path);
        }
    }

exit:
    amxc_var_clean(&ret);
    return retval;
}

void uspa_mtp_clean(amxb_bus_ctx_t* ctx) {
    const char* path = "LocalAgent.MTP.";

    amxc_htable_clean(&mtp_instances, uspa_mtp_instance_hit_clean);
    amxb_unsubscribe(ctx, path, uspa_mtp_socket_status_changed, NULL);
    amxb_unsubscribe(ctx, path, uspa_mtp_removed, NULL);
    amxb_unsubscribe(ctx, path, uspa_mtp_added, NULL);
    amxb_unsubscribe(ctx, path, uspa_mtp_enable_toggled, NULL);
}

amxc_htable_t* uspa_mtp_get_instances(void) {
    return &mtp_instances;
}

void uspa_mtp_instance_hit_clean(const char* key, amxc_htable_it_t* hit) {
    uspa_mtp_instance_t* inst = amxc_htable_it_get_data(hit, uspa_mtp_instance_t, hit);

    uspa_object_update_status(key, "Down");
    uspa_mtp_instance_delete(&inst);
}

int uspa_mtp_instance_con_add(const char* la_mtp, uspi_con_t* con, uint32_t mtp_id) {
    int retval = -1;
    amxc_htable_it_t* it = NULL;
    uspa_mtp_instance_t* mtp_inst = NULL;

    when_str_empty(la_mtp, exit);
    when_null(con, exit);

    it = amxc_htable_get(&mtp_instances, la_mtp);
    when_null(it, exit);

    mtp_inst = amxc_container_of(it, uspa_mtp_instance_t, hit);
    if(mtp_id == USPA_MTP_ID_MQTT) {
        mtp_inst->mtp_data.mqtt->con = con;
    }

    retval = 0;
exit:
    return retval;
}

int uspa_mtp_instance_con_rm(const char* la_mtp, uint32_t mtp_id) {
    int retval = -1;
    amxc_htable_it_t* it = NULL;
    uspa_mtp_instance_t* mtp_inst = NULL;

    when_str_empty(la_mtp, exit);

    it = amxc_htable_get(&mtp_instances, la_mtp);
    when_null(it, exit);

    mtp_inst = amxc_container_of(it, uspa_mtp_instance_t, hit);
    if(mtp_id == USPA_MTP_ID_MQTT) {
        mtp_inst->mtp_data.mqtt->con = NULL;
    }

    retval = 0;
exit:
    return retval;
}
