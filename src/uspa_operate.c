/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <usp/uspl.h>

#include "uspa.h"
#include "uspa_msghandler.h"
#include "uspa_request.h"
#include "uspa_object.h"
#include "uspa_utils.h"
#include "uspa_controller.h"

static void uspa_operate_set_err_values(amxc_var_t* cmd_failure,
                                        uint32_t err_code,
                                        const char* err_msg) {
    if(err_code != 0) {
        amxc_var_add_key(uint32_t, cmd_failure, "err_code", err_code);
    } else {
        amxc_var_add_key(uint32_t, cmd_failure, "err_code", USP_ERR_COMMAND_FAILURE);
    }
    if((err_msg != NULL) && (*err_msg != 0)) {
        amxc_var_add_key(cstring_t, cmd_failure, "err_msg", err_msg);
    } else {
        amxc_var_add_key(cstring_t, cmd_failure, "err_msg", uspl_error_code_to_str(USP_ERR_COMMAND_FAILURE));
    }
    SAH_TRACEZ_INFO(ME, "Operate err_code=%d, err_msg=%s", GET_UINT32(cmd_failure, "err_code"),
                    GET_CHAR(cmd_failure, "err_msg"));
}

static void uspa_async_response_success(amxc_var_t* oper_complete, amxc_var_t* result) {
    amxc_var_add_key(uint32_t, oper_complete, "operation_case",
                     USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_REQ_OUTPUT_ARGS);
    if(result != NULL) {
        amxc_var_set_key(oper_complete, "output_args", result, AMXC_VAR_FLAG_COPY);
    }
}

static void uspa_async_response_failure(amxc_var_t* oper_complete, uint32_t err_code, const char* err_msg) {
    amxc_var_t* cmd_failure = amxc_var_add_key(amxc_htable_t, oper_complete, "cmd_failure", NULL);

    amxc_var_add_key(uint32_t, oper_complete, "operation_case",
                     USP__NOTIFY__OPERATION_COMPLETE__OPERATION_RESP_CMD_FAILURE);
    uspa_operate_set_err_values(cmd_failure, err_code, err_msg);
}

static void uspa_async_oper_done(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                 amxb_request_t* request,
                                 int status,
                                 void* priv) {
    uspa_request_t* uspa_req = (uspa_request_t*) priv;
    amxc_var_t* out_args = NULL;
    amxc_var_t* oper_complete = NULL;
    int retval = -1;
    amxc_var_t ret;
    amxd_path_t cmd_path;

    amxc_var_new(&oper_complete);
    amxc_var_init(&ret);
    amxd_path_init(&cmd_path, NULL);

    // If request was actually called it could contain out args
    // If it was not called, get error info from uspa_request_t
    if(request != NULL) {
        out_args = GETI_ARG(request->result, 1);
    } else {
        out_args = uspa_req->out_args;
    }

    retval = amxb_get(amxb_be_who_has(uspa_req->request_obj), uspa_req->request_obj, 0, &ret, 5);
    when_failed_trace(retval, exit, ERROR, "Failed to get %s", uspa_req->request_obj);

    amxd_path_setf(&cmd_path, false, "%s", GETP_CHAR(&ret, "0.0.Command"));

    amxc_var_set_type(oper_complete, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, oper_complete, "path", amxd_path_get(&cmd_path, AMXD_OBJECT_TERMINATE));
    amxc_var_add_key(cstring_t, oper_complete, "cmd_name", amxd_path_get_param(&cmd_path));
    amxc_var_add_key(cstring_t, oper_complete, "cmd_key", GETP_CHAR(&ret, "0.0.CommandKey"));

    if(amxc_var_type_of(out_args) != AMXC_VAR_ID_HTABLE) {
        SAH_TRACEZ_WARNING(ME, "No output arguments found for operation");
    }

    if(status == 0) {
        uspa_object_update_status(uspa_req->request_obj, "Success");
        uspa_async_response_success(oper_complete, out_args);
    } else {
        uspa_object_update_status(uspa_req->request_obj, "Error");
        uspa_async_response_failure(oper_complete, GET_UINT32(out_args, "err_code"),
                                    GET_CHAR(out_args, "err_msg"));
    }

    amxp_sigmngr_emit_signal(NULL, "dm:OperationComplete", oper_complete);

exit:
    amxd_path_clean(&cmd_path);
    amxc_var_clean(&ret);
    amxc_var_delete(&oper_complete);
    amxb_close_request(&uspa_req->call_request);
    uspa_request_dm_del(uspa_req->request_obj);
    uspa_request_delete(&uspa_req);
}

static void uspa_sync_response_success(amxc_llist_t* resp_list,
                                       const char* command,
                                       amxc_var_t* ret) {
    amxc_var_t* response = NULL;
    amxc_var_t* output_args = NULL;
    amxc_var_t* ret_var = GETI_ARG(ret, 0);
    amxc_var_t* out_args_var = GETI_ARG(ret, 1);

    amxc_var_new(&response);
    amxc_var_set_type(response, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, response, "executed_command", command);
    amxc_var_add_key(uint32_t, response, "operation_resp_case", USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OUTPUT_ARGS);
    output_args = amxc_var_add_key(amxc_htable_t, response, "output_args", NULL);

    if(out_args_var != NULL) {
        amxc_var_move(output_args, out_args_var);
    }

    if((ret_var != NULL) && (amxc_var_type_of(ret_var) != AMXC_VAR_ID_NULL)) {
        amxc_var_t* _retval = amxc_var_add_key(amxc_htable_t, output_args, "_retval", NULL);
        amxc_var_move(_retval, ret_var);
    }

    amxc_llist_append(resp_list, &response->lit);
}

static void uspa_sync_response_failure(amxc_llist_t* resp_list,
                                       const char* command,
                                       uint32_t err_code,
                                       const char* err_msg) {
    amxc_var_t* response = NULL;
    amxc_var_t* cmd_failure = NULL;

    amxc_var_new(&response);

    amxc_var_set_type(response, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, response, "executed_command", command);
    amxc_var_add_key(uint32_t, response, "operation_resp_case", USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_CMD_FAILURE);
    cmd_failure = amxc_var_add_key(amxc_htable_t, response, "cmd_failure", NULL);
    uspa_operate_set_err_values(cmd_failure, err_code, err_msg);

    amxc_llist_append(resp_list, &response->lit);
}

static int uspa_invoke_sync(amxb_bus_ctx_t* ctx,
                            amxc_llist_t* resp_list,
                            amxc_var_t* request,
                            const char* function,
                            amxc_var_t* input_args,
                            bool resp_needed,
                            uint32_t access) {
    const char* object_path = GET_CHAR(request, "object_path");
    uint32_t status = GET_UINT32(request, "status");
    amxc_string_t cmd;

    amxc_string_init(&cmd, 0);
    amxc_string_setf(&cmd, "%s%s", object_path, function);

    if(status == amxd_status_ok) {
        SAH_TRACEZ_INFO(ME, "Synchronous operate: %s%s", object_path, function);
        int retval = -1;
        amxc_var_t ret;

        amxc_var_init(&ret);
        amxb_set_access(ctx, access);
        retval = amxb_call(ctx, object_path, function, input_args, &ret, 10);
        amxb_set_access(ctx, AMXB_PROTECTED);
        if(resp_needed) {
            if(retval == 0) {
                SAH_TRACEZ_INFO(ME, "Synchronous operate succeeded");
                uspa_sync_response_success(resp_list, amxc_string_get(&cmd, 0), &ret);
            } else {
                SAH_TRACEZ_WARNING(ME, "Synchronous operate failed");
                uspa_sync_response_failure(resp_list, amxc_string_get(&cmd, 0), GETP_UINT32(&ret, "1.err_code"), GETP_CHAR(&ret, "1.err_msg"));
            }
        }
        amxc_var_clean(&ret);
    } else if((status != amxd_status_ok) && resp_needed) {
        uint32_t err_code = uspl_amxd_status_to_usp_error(status);
        const char* err_msg = uspl_error_code_to_str(err_code);
        SAH_TRACEZ_INFO(ME, "Synchronous operate not invoked on path %s%s, because of error '%s'",
                        object_path, function, err_msg);
        uspa_sync_response_failure(resp_list, amxc_string_get(&cmd, 0), err_code, err_msg);
    }

    // always return 0; the response message will indicate success or failure
    amxc_string_clean(&cmd);
    return 0;
}

static int uspa_invoke_async(amxb_bus_ctx_t* ctx,
                             amxc_llist_t* resp_list,
                             amxc_var_t* request_var,
                             const char* function,
                             amxc_var_t* input_args,
                             const char* cmd_key,
                             const char* controller_id,
                             bool resp_needed,
                             uint32_t access) {
    int retval = -1;
    amxc_var_t var;
    amxc_string_t cmd;
    uspa_request_t* request = NULL;
    const char* object_path = GET_CHAR(request_var, "object_path");
    uint32_t status = GET_UINT32(request_var, "status");
    SAH_TRACEZ_INFO(ME, "Asynchronous operate: %s%s with command_key: %s",
                    object_path, function, cmd_key);

    amxc_string_init(&cmd, 0);
    amxc_string_setf(&cmd, "%s%s", object_path, function);
    amxc_var_init(&var);

    request = uspa_request_dm_add(controller_id, amxc_string_get(&cmd, 0), cmd_key);
    when_null(request, exit);

    if(resp_needed) {
        amxc_var_t* response = NULL;
        amxc_var_new(&response);
        amxc_var_set_type(response, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, response, "executed_command", amxc_string_get(&cmd, 0));
        amxc_var_add_key(uint32_t, response, "operation_resp_case", USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OBJ_PATH);
        amxc_var_add_key(cstring_t, response, "req_obj_path", request->request_obj);
        amxc_llist_append(resp_list, &response->lit);
    }

    if(status != amxd_status_ok) {
        uint32_t err_code = uspl_amxd_status_to_usp_error(status);
        amxc_var_new(&request->out_args);
        amxc_var_set_type(request->out_args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint32_t, request->out_args, "err_code", err_code);
        amxc_var_add_key(cstring_t, request->out_args, "err_msg", uspl_error_code_to_str(err_code));
        SAH_TRACEZ_ERROR(ME, "Failed to validate command, err_code=%d, err_msg=%s", err_code, uspl_error_code_to_str(err_code));
        uspa_object_update_status(request->request_obj, "Error");
        uspa_async_oper_done(NULL, NULL, retval, request);
        goto exit;
    }

    // Add CommandKey and Requestor to input_args such that plugin has this information
    amxc_var_add_key(cstring_t, input_args, "_CommandKey", cmd_key);
    amxc_var_add_key(cstring_t, input_args, "_Requestor", controller_id);

    amxb_set_access(ctx, access);
    request->call_request = amxb_async_call(ctx, object_path, function, input_args, uspa_async_oper_done, request);
    amxb_set_access(ctx, AMXB_PROTECTED);
    uspa_object_update_status(request->request_obj, "Active");

    when_null(request->call_request, exit);

exit:
    amxc_string_clean(&cmd);
    amxc_var_clean(&var);
    // always return 0; the response message will indicate success or failure
    return 0;
}

static bool is_function_async(amxb_bus_ctx_t* ctx,
                              const char* object_path,
                              const char* function) {
    amxc_var_t* attributes = NULL;
    amxc_var_t ret;
    bool async = true;
    amxc_string_t stripped_func;

    amxc_var_init(&ret);
    amxc_string_init(&stripped_func, 0);

    amxc_string_setf(&stripped_func, "%s", function);
    amxc_string_trimr(&stripped_func, uspa_utils_is_brace);

    amxb_describe(ctx, object_path, AMXB_FLAG_FUNCTIONS, &ret, 5);
    attributes = amxc_var_get_pathf(&ret, AMXC_VAR_FLAG_DEFAULT, "0.functions.'%s'.attributes",
                                    amxc_string_get(&stripped_func, 0));
    async = GET_BOOL(attributes, "asynchronous");

    amxc_string_clean(&stripped_func);
    amxc_var_clean(&ret);
    return async;
}

static int uspa_operate_save_reboot_params(amxc_var_t* oper_request, const char* cause) {
    int retval = -1;
    const char* cmd_key = GET_CHAR(oper_request, "command_key");
    amxc_var_t ret;
    amxc_var_t values;

    amxc_var_init(&ret);
    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "CommandKey", cmd_key);
    amxc_var_add_key(cstring_t, &values, "Cause", cause);

    retval = amxb_set(amxb_be_who_has("LocalAgent."), "LocalAgent.", &values, &ret, 5);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "Failed to set LocalAgent. reboot params");
        goto exit;
    }

exit:
    amxc_var_clean(&values);
    amxc_var_clean(&ret);
    return retval;
}

static void uspa_operate_handle_exceptions(amxc_var_t* oper_request, const char* function) {
    if(strcmp(function, "Reboot()") == 0) {
        uspa_operate_save_reboot_params(oper_request, "RemoteReboot");
    } else if(strcmp(function, "FactoryReset()") == 0) {
        uspa_operate_save_reboot_params(oper_request, "FactoryReset");
    }
}

static amxc_var_t* uspa_operate_add_result (const char* obj_path, uint32_t err_code) {
    amxc_var_t* retval = NULL;

    amxc_var_new(&retval);
    amxc_var_set_type(retval, AMXC_VAR_ID_LIST);

    amxc_var_t* result = amxc_var_add(amxc_htable_t, retval, NULL);
    amxc_var_add_key(cstring_t, result, "object_path", obj_path);
    amxc_var_add_key(uint32_t, result, "status", err_code);
    return retval;
}

static int uspa_invoke_operate(amxc_var_t* operate,
                               amxc_llist_t* resp_list,
                               char* controller_id,
                               bool resp_needed,
                               const char* acl_file) {
    amxb_bus_ctx_t* ctx = NULL;
    amxc_var_t* requests = NULL;
    const char* object_path = NULL;
    const char* function = NULL;
    amxd_path_t cmd_path;
    bool async = true;
    uint32_t access = uspa_controller_get_access(controller_id);

    amxd_path_init(&cmd_path, GET_CHAR(operate, "command"));
    object_path = amxd_path_get(&cmd_path, AMXD_OBJECT_TERMINATE);
    function = amxd_path_get_param(&cmd_path);
    ctx = uspa_discovery_get_ctx(object_path);

    when_str_empty(function, exit);

    if(ctx == NULL) {
        SAH_TRACEZ_ERROR(ME, "Operate: Invalid object path '%s'", object_path);
        requests = uspa_operate_add_result(object_path, amxd_status_invalid_path);
        amxc_var_for_each(request, requests) {
            uspa_invoke_sync(ctx, resp_list, request, function, GET_ARG(operate, "input_args"), resp_needed, access);
        }
        goto exit;
    }

    uspa_operate_handle_exceptions(operate, function);

    amxb_set_access(ctx, access);
    requests = amxa_validate_operate(ctx, acl_file, object_path, function);
    amxb_set_access(ctx, AMXB_PROTECTED);

    async = is_function_async(ctx, object_path, function);
    if(async) {
        amxc_var_for_each(request, requests) {
            uspa_invoke_async(ctx, resp_list, request, function, GET_ARG(operate, "input_args"),
                              GET_CHAR(operate, "command_key"), controller_id, resp_needed, access);
        }
    } else {
        amxc_var_for_each(request, requests) {
            uspa_invoke_sync(ctx, resp_list, request, function, GET_ARG(operate, "input_args"),
                             resp_needed, access);
        }
    }

exit:
    amxc_var_delete(&requests);
    amxd_path_clean(&cmd_path);
    return 0;
}

int uspa_handle_operate(uspl_rx_t* usp_rx,
                        uspl_tx_t** usp_tx,
                        const char* acl_file) {
    SAH_TRACEZ_INFO(ME, "Handle USP message of type operate");
    int retval = -1;
    amxc_var_t request;
    amxc_llist_t resp_list;
    bool resp_needed = false;

    amxc_var_init(&request);
    amxc_llist_init(&resp_list);

    when_null(usp_rx, exit);
    when_null(usp_tx, exit);

    retval = uspl_operate_extract(usp_rx, &request);

    if(retval != 0) {
        retval = uspa_msghandler_build_error(usp_rx, usp_tx, retval);
        goto exit;
    }

    resp_needed = GET_BOOL(&request, "send_resp");
    uspa_invoke_operate(&request, &resp_list, uspl_msghandler_from_id(usp_rx), resp_needed, acl_file);
    if(resp_needed) {
        retval = uspa_msghandler_build_reply(usp_rx, &resp_list, uspl_operate_resp_new, usp_tx);
    }
exit:
    amxc_var_clean(&request);
    amxc_llist_clean(&resp_list, variant_list_it_free);

    return retval;
}
