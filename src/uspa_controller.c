/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>

#include "uspa_controller.h"
#include "uspa_controller_mtp.h"
#include "uspa_object.h"
#include "uspa_mtp_mqtt.h"
#include "uspa_imtp.h"
#include "uspa_agent.h"
#include "uspa_subscription.h"
#include "uspa_utils.h"

amxc_htable_t contr_instances;        // Track controller instances: key = LocalAgent.Controller.{i}., value = uspa_contr_instance_t struct

static int uspa_contr_instance_new(uspa_contr_instance_t** inst) {
    int retval = -1;

    when_null(inst, exit);

    *inst = (uspa_contr_instance_t*) calloc(1, sizeof(uspa_contr_instance_t));
    when_null(*inst, exit);

    amxc_llist_init(&(*inst)->cons);

    retval = 0;
exit:
    return retval;
}

static void uspa_contr_con_list_it_delete(amxc_llist_it_t* it) {
    uspa_contr_con_t* contr_con = amxc_container_of(it, uspa_contr_con_t, lit);
    contr_con->con = NULL;
    free(contr_con->contr_mtp_inst);
    free(contr_con);
}

static void uspa_contr_instance_delete(uspa_contr_instance_t** inst) {

    when_null(inst, exit);
    when_null(*inst, exit);

    amxc_llist_clean(&(*inst)->cons, uspa_contr_con_list_it_delete);
    uspa_sub_retry_delete(&(*inst)->obr_retry);
    free(*inst);
    *inst = NULL;

exit:
    return;
}

static void uspa_contr_instance_hit_clean(UNUSED const char* key, amxc_htable_it_t* hit) {
    uspa_contr_instance_t* inst = amxc_htable_it_get_data(hit, uspa_contr_instance_t, hit);
    uspa_contr_instance_delete(&inst);
}

static void uspa_controller_con_link_mqtt(uspi_con_t* con, const char* la_mtp) {
    amxc_string_t search_path;
    amxc_var_t ret;
    amxc_string_t la_mtp_path;

    amxc_string_init(&la_mtp_path, 0);
    amxc_string_setf(&la_mtp_path, "%s", la_mtp);
    amxc_string_trimr(&la_mtp_path, uspa_utils_is_dot);
    amxc_var_init(&ret);
    amxc_string_init(&search_path, 0);

    // la_mtp passed to this function will never contain a Device prefix, but configured
    // AgentMTPReference might contain a prefix, so match for both
    amxc_string_setf(&search_path, "LocalAgent.Controller.[Enable==1].MTP.[Enable==1 && MQTT.AgentMTPReference matches '^(Device.)?%s'].",
                     amxc_string_get(&la_mtp_path, 0));
    amxb_get(amxb_be_who_has("LocalAgent."), amxc_string_get(&search_path, 0), 0, &ret, 5);

    amxc_var_for_each(contr_mtp, GETI_ARG(&ret, 0)) {
        amxd_path_t contr_inst;
        amxd_path_init(&contr_inst, amxc_var_key(contr_mtp));
        free(amxd_path_get_last(&contr_inst, true));
        free(amxd_path_get_last(&contr_inst, true));
        uspa_controller_con_add(amxd_path_get(&contr_inst, AMXD_OBJECT_TERMINATE),
                                amxc_var_key(contr_mtp), con);
        amxd_path_clean(&contr_inst);
    }

    amxc_var_clean(&ret);
    amxc_string_clean(&search_path);
    amxc_string_clean(&la_mtp_path);
}

static void uspa_controller_enable_toggled(UNUSED const char* const sig_name,
                                           const amxc_var_t* const data,
                                           UNUSED void* const priv) {
    const char* contr = GET_CHAR(data, "path");
    bool enabled = GETP_BOOL(data, "parameters.Enable.to");
    uspa_contr_instance_t* inst = uspa_contr_instance_get(contr);
    SAH_TRACEZ_INFO(ME, "Enable of %s is %d", contr, enabled);

    when_null_trace(inst, exit, ERROR, "No uspa_contr_instance_t found for %s", contr);

    if(enabled) {
        uspa_controller_mtp_init(amxb_be_who_has("LocalAgent."), contr);
        inst->enabled = true;
    } else {
        inst->enabled = false;
    }

exit:
    return;
}

static void uspa_controller_added(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  UNUSED void* const priv) {
    const char* contr_template = GET_CHAR(data, "path");
    uint32_t index = GET_UINT32(data, "index");
    bool enabled = GETP_BOOL(data, "parameters.Enable");
    uspa_contr_instance_t* contr_inst = NULL;
    int retval = -1;
    amxc_string_t contr_path;

    amxc_string_init(&contr_path, 0);
    amxc_string_setf(&contr_path, "%s%d.", contr_template, index);
    SAH_TRACEZ_INFO(ME, "Added controller instance %s", amxc_string_get(&contr_path, 0));

    retval = uspa_contr_instance_new(&contr_inst);
    when_failed(retval, exit);
    amxc_htable_insert(&contr_instances, amxc_string_get(&contr_path, 0), &contr_inst->hit);
    if(enabled) {
        contr_inst->enabled = true;
    }

exit:
    amxc_string_clean(&contr_path);
}

static int uspa_controller_build_on_board(const amxc_var_t* const data,
                                          uspl_tx_t* usp_tx) {
    int retval = -1;
    amxc_var_t request;
    amxc_var_t* on_board_req = NULL;

    amxc_var_init(&request);
    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &request, "notification_case", USP__NOTIFY__NOTIFICATION_ON_BOARD_REQ);
    amxc_var_add_key(cstring_t, &request, "subscription_id", ""); // R-NOT.7
    amxc_var_add_key(bool, &request, "send_resp", true);          // R-NOT.6
    on_board_req = amxc_var_add_key(amxc_htable_t, &request, "on_board_req", NULL);
    amxc_var_add_key(cstring_t, on_board_req, "oui", GET_CHAR(data, "oui"));
    amxc_var_add_key(cstring_t, on_board_req, "product_class", GET_CHAR(data, "product_class"));
    amxc_var_add_key(cstring_t, on_board_req, "serial_number", GET_CHAR(data, "serial_number"));
    amxc_var_add_key(cstring_t, on_board_req, "agent_supported_protocol_version",
                     GET_CHAR(data, "agent_supported_protocol_version"));

    retval = uspl_notify_new(usp_tx, &request);

    amxc_var_clean(&request);
    return retval;
}

static void retry_obr_timeout(UNUSED amxp_timer_t* timer, void* priv) {
    uspa_contr_instance_t* contr_inst = (uspa_contr_instance_t*) priv;
    uspa_sub_retry_t* obr_retry = contr_inst->obr_retry;
    const char* contr_path = amxc_htable_it_get_key(&contr_inst->hit);
    int retval = -1;
    uspi_con_t* con = NULL;
    amxc_var_t mtp_info;

    SAH_TRACEZ_INFO(ME, "Retrying OnBoardRequest notification");
    amxc_var_init(&mtp_info);

    con = uspa_imtp_con_from_contr(contr_path);
    if(con == NULL) {
        SAH_TRACEZ_WARNING(ME, "No IMTP connection found for %s, not sending notification", contr_path);
        goto exit;
    }

    retval = amxb_call(amxb_be_who_has("LocalAgent."), contr_path, "GetMTPInfo", NULL, &mtp_info, 5);
    when_failed(retval, exit);

    if(obr_retry->retry_attempts < USPA_SUB_RETRY_MAX) {
        (obr_retry->retry_attempts)++;
    }
    obr_retry->retry_time = uspa_subscription_calc_retry_timeout(contr_path, obr_retry);

    uspa_imtp_send(con, obr_retry->usp_tx, GETI_ARG(&mtp_info, 0));

    SAH_TRACEZ_NOTICE(ME, "Next USP notification retry in %d seconds", obr_retry->retry_time);
    amxp_timer_start(obr_retry->retry_timer, 1000 * obr_retry->retry_time);

exit:
    amxc_var_clean(&mtp_info);
}

static void uspa_controller_obr_retry(uspa_contr_instance_t* contr_inst, uspl_tx_t* usp_tx) {
    uspa_sub_retry_t* obr_retry = NULL;

    when_null(contr_inst, exit);
    when_not_null(contr_inst->obr_retry, exit);
    when_null(usp_tx, exit);

    obr_retry = calloc(1, sizeof(uspa_sub_retry_t));
    when_null(obr_retry, exit);

    contr_inst->obr_retry = obr_retry;
    obr_retry->usp_tx = usp_tx;

    amxp_timer_new(&obr_retry->retry_timer, retry_obr_timeout, contr_inst);
    obr_retry->retry_time = uspa_subscription_calc_retry_timeout(amxc_htable_it_get_key(&contr_inst->hit), obr_retry);
    SAH_TRACEZ_NOTICE(ME, "Next USP notification retry in %d seconds", obr_retry->retry_time);
    amxp_timer_start(obr_retry->retry_timer, 1000 * obr_retry->retry_time);

exit:
    return;
}

static void uspa_controller_obr_reset_retry(const char* contr_path, uspa_sub_retry_t* obr_retry) {
    SAH_TRACEZ_INFO(ME, "OnBoardRequest previously sent, but not confirmed with NotifyResp");
    SAH_TRACEZ_INFO(ME, "Retrying OnBoardRequest and resetting retry mechanism");
    obr_retry->retry_attempts = 0;
    obr_retry->retry_time = uspa_subscription_calc_retry_timeout(contr_path, obr_retry);
    SAH_TRACEZ_NOTICE(ME, "Next USP notification retry in %d seconds", obr_retry->retry_time);
    amxp_timer_start(obr_retry->retry_timer, 1000 * obr_retry->retry_time);
}

static void uspa_controller_send_on_board(UNUSED const char* const sig_name,
                                          const amxc_var_t* const data,
                                          UNUSED void* const priv) {
    int retval = 1;
    uspi_con_t* con = NULL;
    amxc_var_t mtp_info;
    uspl_tx_t* usp_tx = NULL;
    const char* contr_path = GET_CHAR(data, "path");
    const char* from_id = GET_CHAR(data, "agent_eid");
    const char* to_id = GET_CHAR(data, "contr_eid");
    uspa_contr_instance_t* contr_inst = uspa_contr_instance_get(contr_path);

    amxc_var_init(&mtp_info);

    when_null(contr_inst, exit);

    // Don't build new OnBoardRequest if a previous one isn't even confirmed yet
    if(contr_inst->obr_retry != NULL) {
        uspa_controller_obr_reset_retry(contr_path, contr_inst->obr_retry);
        usp_tx = contr_inst->obr_retry->usp_tx;
    } else {
        retval = uspl_tx_new(&usp_tx, from_id, to_id);
        when_failed(retval, exit);

        retval = uspa_controller_build_on_board(data, usp_tx);
        when_failed(retval, exit);
    }

    con = uspa_imtp_con_from_contr(contr_path);
    if(con == NULL) {
        SAH_TRACEZ_WARNING(ME, "No IMTP connection found for %s, not sending notification", contr_path);
        goto exit;
    }

    retval = amxb_call(amxb_be_who_has("LocalAgent."), contr_path, "GetMTPInfo", NULL, &mtp_info, 5);
    when_failed(retval, exit);

    SAH_TRACEZ_INFO(ME, "Send OnBoardRequest to controller = %s", to_id);
    uspa_imtp_send(con, usp_tx, GETI_ARG(&mtp_info, 0));

exit:
    uspa_controller_obr_retry(contr_inst, usp_tx);
    amxc_var_clean(&mtp_info);
    return;
}

static void uspa_controller_status_changed(UNUSED const char* const sig_name,
                                           const amxc_var_t* const data,
                                           UNUSED void* const priv) {
    const char* contr = GET_CHAR(data, "path");
    amxc_var_t ret;
    SAH_TRACEZ_INFO(ME, "Status of %s is %s", contr, GETP_CHAR(data, "parameters.Status.to"));

    amxc_var_init(&ret);

    amxb_get(amxb_be_who_has("LocalAgent."), "LocalAgent.Controller.[Status=='Up'].", 0, &ret, 5);
    if(GETP_ARG(&ret, "0.0") == NULL) {
        SAH_TRACEZ_INFO(ME, "No controllers are Up");
        uspa_agent_set_status("Down");
    } else {
        SAH_TRACEZ_INFO(ME, "At least one controller is Up");
        uspa_agent_set_status("Up");
    }

    amxc_var_clean(&ret);
}

int uspa_controller_subscribe(amxb_bus_ctx_t* ctx) {
    int retval = -1;
    const char* path = "LocalAgent.Controller.";
    const char* expr_enabled = "(notification == 'dm:object-changed') && \
                                (contains('parameters.Enable')) && \
                                (path matches 'LocalAgent.Controller.[0-9]+.$')";
    const char* expr_added = "(notification == 'dm:instance-added') && \
                              (path matches 'LocalAgent.Controller.$')";
    const char* expr_status = "(notification == 'dm:object-changed') && \
                                (contains('parameters.Status')) && \
                                (path matches 'LocalAgent.Controller.[0-9]+.$')";
    const char* expr_on_board = "(notification == 'OnBoardRequest!')";

    // Subscribe to events for enabled controllers
    retval = amxb_subscribe(ctx, path, expr_enabled, uspa_controller_enable_toggled, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to %s changed events", path);
        goto exit;
    }

    // Subscribe to events for added controllers
    retval = amxb_subscribe(ctx, path, expr_added, uspa_controller_added, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to %s added events", path);
        goto exit;
    }

    // Subscribe to OnBoardRequest! event
    retval = amxb_subscribe(ctx, path, expr_on_board, uspa_controller_send_on_board, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to OnBoardRequest! events");
        goto exit;
    }

    // Subscribe to events for controller status changes
    retval = amxb_subscribe(ctx, path, expr_status, uspa_controller_status_changed, NULL);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to %s changed events", path);
        goto exit;
    }

exit:
    return retval;
}

uspa_contr_instance_t* uspa_contr_instance_get(const char* contr_inst) {
    amxc_htable_it_t* hit = amxc_htable_get(&contr_instances, contr_inst);
    return amxc_htable_it_get_data(hit, uspa_contr_instance_t, hit);
}

bool uspa_controller_get_enable(const char* instance) {
    int retval = -1;
    amxc_var_t ret;
    bool enable = false;

    amxc_var_init(&ret);

    retval = amxb_get(amxb_be_who_has("LocalAgent."), instance, 0, &ret, 5);
    when_failed(retval, exit);

    enable = GETP_BOOL(&ret, "0.0.Enable");

exit:
    amxc_var_clean(&ret);
    return enable;
}

char* uspa_controller_get_eid(const char* instance) {
    int retval = -1;
    amxc_var_t ret;
    const char* eid = NULL;
    char* eid_ret = NULL;

    amxc_var_init(&ret);

    retval = amxb_get(amxb_be_who_has("LocalAgent."), instance, 0, &ret, 5);
    eid = GETP_CHAR(&ret, "0.0.EndpointID");
    when_true(retval != 0 || eid == NULL || *eid == 0, exit);

    eid_ret = strdup(eid);

exit:
    amxc_var_clean(&ret);
    return eid_ret;
}

char* uspa_controller_get_instance_from_eid(const char* eid) {
    char* result = NULL;
    amxc_var_t* controller = NULL;
    amxc_string_t path;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    when_str_empty(eid, exit);
    amxc_string_setf(&path, "LocalAgent.Controller.[EndpointID=='%s'].", eid);

    amxb_get(amxb_be_who_has("LocalAgent."), amxc_string_get(&path, 0), 0, &ret, 5);
    controller = GETP_ARG(&ret, "0.0");

    if(controller == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to find controller instance with EndpointID = %s", eid);
        goto exit;
    }

    result = strdup(amxc_var_key(controller));

exit:
    amxc_var_clean(&ret);
    amxc_string_clean(&path);
    return result;
}

int uspa_controller_con_add(const char* contr_inst, const char* contr_mtp_inst, uspi_con_t* con) {
    int retval = -1;
    uspa_contr_instance_t* inst = NULL;
    uspa_contr_con_t* contr_con = NULL;

    when_str_empty(contr_inst, exit);
    when_str_empty(contr_mtp_inst, exit);
    when_null(con, exit);

    inst = uspa_contr_instance_get(contr_inst);
    when_null(inst, exit);

    contr_con = (uspa_contr_con_t*) calloc(1, sizeof(uspa_contr_con_t));
    when_null(contr_con, exit);

    contr_con->contr_mtp_inst = strdup(contr_mtp_inst);
    if(contr_con->contr_mtp_inst == NULL) {
        free(contr_con);
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Adding connection for %s", contr_mtp_inst);
    contr_con->con = con;
    amxc_llist_append(&inst->cons, &contr_con->lit);
    uspa_object_update_status(contr_inst, "Up");

    retval = 0;
exit:
    return retval;
}

int uspa_controller_con_del(const char* contr_inst, const char* contr_mtp_inst) {
    int retval = -1;
    uspa_contr_instance_t* inst = NULL;
    uspa_contr_con_t* contr_con = NULL;

    when_str_empty(contr_inst, exit);

    inst = uspa_contr_instance_get(contr_inst);
    when_null(inst, exit);

    amxc_llist_for_each(it, &inst->cons) {
        contr_con = amxc_llist_it_get_data(it, uspa_contr_con_t, lit);
        if(strcmp(contr_con->contr_mtp_inst, contr_mtp_inst) == 0) {
            break;
        }
    }
    when_null(contr_con, exit);

    SAH_TRACEZ_INFO(ME, "Removing connection for %s", contr_mtp_inst);
    amxc_llist_it_take(&contr_con->lit);
    contr_con->con = NULL;
    free(contr_con->contr_mtp_inst);
    free(contr_con);

    if(amxc_llist_size(&inst->cons) == 0) {
        uspa_object_update_status(contr_inst, "Down");
    }

    retval = 0;
exit:
    return retval;
}

uspi_con_t* uspa_controller_con_get(const char* contr_inst) {
    uspi_con_t* con = NULL;
    uspa_contr_instance_t* inst = NULL;
    amxc_llist_it_t* lit = NULL;
    uspa_contr_con_t* contr_con = NULL;

    when_str_empty(contr_inst, exit);

    inst = uspa_contr_instance_get(contr_inst);
    when_null(inst, exit);

    if(!inst->enabled) {
        SAH_TRACEZ_WARNING(ME, "Controller %s is disabled", contr_inst);
        goto exit;
    }

    lit = amxc_llist_get_first(&inst->cons);
    when_null(lit, exit);
    contr_con = amxc_llist_it_get_data(lit, uspa_contr_con_t, lit);
    con = contr_con->con;

exit:
    return con;
}

void uspa_controller_con_link(uspi_con_t* con, const char* protocol, const char* la_mtp) {
    when_null(con, exit);
    when_str_empty(protocol, exit);
    when_str_empty(la_mtp, exit);

    if(strcmp(protocol, "MQTT") == 0) {
        uspa_controller_con_link_mqtt(con, la_mtp);
    }

exit:
    return;
}

void uspa_controller_con_unlink(uspi_con_t* con) {
    amxc_htable_for_each(hit, &contr_instances) {
        uspa_contr_instance_t* contr_inst = amxc_htable_it_get_data(hit, uspa_contr_instance_t, hit);
        amxc_llist_it_t* lit = amxc_llist_get_first(&contr_inst->cons);
        while(lit != NULL) {
            amxc_llist_it_t* next = amxc_llist_it_get_next(lit);
            uspa_contr_con_t* contr_con = amxc_llist_it_get_data(lit, uspa_contr_con_t, lit);
            if(contr_con->con == con) {
                SAH_TRACEZ_INFO(ME, "Removing con for %s", contr_con->contr_mtp_inst);
                amxc_llist_it_take(&contr_con->lit);
                contr_con->con = NULL;
                free(contr_con->contr_mtp_inst);
                free(contr_con);
                if(amxc_llist_size(&contr_inst->cons) == 0) {
                    uspa_object_update_status(amxc_htable_it_get_key(&contr_inst->hit), "Down");
                }
            }
            lit = next;
        }
    }
}

int uspa_controller_init(amxb_bus_ctx_t* ctx) {
    int retval = -1;
    amxc_var_t ret;
    amxc_var_t* controllers = NULL;

    amxc_var_init(&ret);

    amxc_htable_init(&contr_instances, 0);

    retval = amxb_get(ctx, "LocalAgent.Controller.", 0, &ret, 5);
    when_failed(retval, exit);

    controllers = GETI_ARG(&ret, 0);
    amxc_var_for_each(contr, controllers) {
        bool enable = GET_BOOL(contr, "Enable");
        const char* alias = GET_CHAR(contr, "Alias");
        uspa_contr_instance_t* contr_inst = NULL;

        if(alias == NULL) {
            continue;
        }

        retval = uspa_contr_instance_new(&contr_inst);
        when_failed(retval, exit);
        amxc_htable_insert(&contr_instances, amxc_var_key(contr), &contr_inst->hit);

        if(enable) {
            uspa_controller_mtp_init(ctx, amxc_var_key(contr));
            contr_inst->enabled = true;
        }
    }

exit:
    amxc_var_clean(&ret);
    return retval;
}

void uspa_controller_clean(amxb_bus_ctx_t* ctx) {
    const char* path = "LocalAgent.Controller.";

    amxc_htable_clean(&contr_instances, uspa_contr_instance_hit_clean);

    amxb_unsubscribe(ctx, path, uspa_controller_status_changed, NULL);
    amxb_unsubscribe(ctx, path, uspa_controller_send_on_board, NULL);
    amxb_unsubscribe(ctx, path, uspa_controller_added, NULL);
    amxb_unsubscribe(ctx, path, uspa_controller_enable_toggled, NULL);
}

uint32_t uspa_controller_get_access(const char* controller_id) {
    amxc_string_t param_path;
    amxc_var_t ret;
    bool access = false;
    int retval = AMXB_PUBLIC;

    amxc_var_init(&ret);
    amxc_string_init(&param_path, 0);

    when_str_empty(controller_id, exit);

    amxc_string_setf(&param_path, "LocalAgent.Controller.[EndpointID == '%s'].ProtectedAccess",
                     controller_id);

    amxb_get(amxb_be_who_has("LocalAgent."), amxc_string_get(&param_path, 0), 0, &ret, 5);
    access = GETP_BOOL(&ret, "0.0.ProtectedAccess");
    retval = access ? AMXB_PROTECTED : AMXB_PUBLIC;

exit:
    amxc_string_clean(&param_path);
    amxc_var_clean(&ret);
    return retval;
}

void uspa_controller_obr_done(const char* msg_id) {
    amxc_htable_for_each(hit, &contr_instances) {
        uspa_contr_instance_t* contr_inst = amxc_container_of(hit, uspa_contr_instance_t, hit);
        if(contr_inst->obr_retry == NULL) {
            continue;
        }
        if(strcmp(contr_inst->obr_retry->usp_tx->msg_id, msg_id) == 0) {
            SAH_TRACEZ_INFO(ME, "Stop retrying OnBoardRequest with msg_id %s", msg_id);
            uspa_sub_retry_delete(&contr_inst->obr_retry);
            contr_inst->obr_retry = NULL;
            break;
        }
    }
}
