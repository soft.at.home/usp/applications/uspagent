/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include <usp/uspl.h>

#include "uspa.h"
#include "uspa_error.h"

static void uspa_error_append_forbidden_part(amxc_var_t* error, amxc_var_t* request) {
    uint32_t err_code = USP_ERR_PERMISSION_DENIED;
    const char* object_path = GET_CHAR(request, "object_path");
    amxc_var_t* params = GET_ARG(request, "parameters");
    amxc_var_for_each(param, params) {
        bool allowed = GET_BOOL(param, "allowed");
        if(!allowed) {
            const char* param_path = GET_CHAR(param, "param");
            uspa_error_append_param(error, object_path, param_path, err_code, NULL);
        }
    }
}

int uspa_error_top_level(amxc_var_t* error, uint32_t err_code, const char* err_msg) {
    int retval = -1;

    when_null(error, exit);

    amxc_var_set_type(error, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, error, "err_code", err_code);

    if((err_msg != NULL) && (*err_msg != 0)) {
        amxc_var_add_key(cstring_t, error, "err_msg", err_msg);
    } else {
        amxc_var_add_key(cstring_t, error, "err_msg", uspl_error_code_to_str(err_code));
    }

    retval = 0;
exit:
    return retval;
}

int uspa_error_append_param(amxc_var_t* error,
                            const char* obj_path,
                            const char* param_path,
                            uint32_t err_code,
                            const char* err_msg) {
    int retval = -1;
    amxc_var_t* param_errs = NULL;
    amxc_var_t* param_err = NULL;
    amxc_string_t full_path;

    amxc_string_init(&full_path, 0);

    when_null(error, exit);
    when_str_empty(obj_path, exit);

    param_errs = GET_ARG(error, "param_errs");
    if(param_errs == NULL) {
        param_errs = amxc_var_add_key(amxc_llist_t, error, "param_errs", NULL);
    }

    if((param_path != NULL) && (*param_path != 0)) {
        amxc_string_setf(&full_path, "%s%s", obj_path, param_path);
    } else {
        amxc_string_setf(&full_path, "%s", obj_path);
    }

    param_err = amxc_var_add(amxc_htable_t, param_errs, NULL);
    amxc_var_add_key(cstring_t, param_err, "param_path", amxc_string_get(&full_path, 0));
    amxc_var_add_key(uint32_t, param_err, "err_code", err_code);

    if((err_msg != NULL) && (*err_msg != 0)) {
        amxc_var_add_key(cstring_t, param_err, "err_msg", err_msg);
    } else {
        amxc_var_add_key(cstring_t, param_err, "err_msg", uspl_error_code_to_str(err_code));
    }

    retval = 0;
exit:
    amxc_string_clean(&full_path);
    return retval;
}

void uspa_error_append_forbidden(amxc_var_t* error, amxc_var_t* requests) {
    amxc_var_for_each(request, requests) {
        amxc_var_t* resolved_paths = GET_ARG(request, "resolved_paths");

        if(resolved_paths != NULL) {
            amxc_var_for_each(var, resolved_paths) {
                uspa_error_append_forbidden_part(error, var);
            }
        } else {
            uspa_error_append_forbidden_part(error, request);
        }
    }
}
