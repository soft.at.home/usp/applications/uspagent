/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>

#include "uspa_request.h"

static uint32_t uspa_request_index_from_path(amxd_path_t* path) {
    char* last = NULL;
    amxc_var_t tmp;
    uint32_t index = 0;

    amxc_var_init(&tmp);
    last = amxd_path_get_last(path, true);

    last[strlen(last) - 1] = '\0';

    amxc_var_set(cstring_t, &tmp, last);
    index = amxc_var_dyncast(uint32_t, &tmp);

    free(last);
    amxc_var_clean(&tmp);
    return index;
}

int uspa_request_new(uspa_request_t** request, const char* path) {
    int retval = -1;

    when_null(request, exit);

    (*request) = calloc(1, sizeof(uspa_request_t));
    when_null(*request, exit);

    (*request)->request_obj = strdup(path);
    if((*request)->request_obj == NULL) {
        free(*request);
        *request = NULL;
        goto exit;
    }

    retval = 0;
exit:
    return retval;
}

void uspa_request_delete(uspa_request_t** request) {
    when_null(request, exit);
    when_null(*request, exit);

    if((*request)->out_args != NULL) {
        amxc_var_delete(&(*request)->out_args);
    }
    free((*request)->request_obj);
    free(*request);

exit:
    return;
}

uspa_request_t* uspa_request_dm_add(const char* originator,
                                    const char* command,
                                    const char* command_key) {
    int retval = -1;
    amxc_var_t ret;
    amxc_var_t values;
    uspa_request_t* request = NULL;
    const char* path = NULL;

    amxc_var_init(&ret);
    amxc_var_init(&values);

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Originator", originator);
    amxc_var_add_key(cstring_t, &values, "Command", command);
    amxc_var_add_key(cstring_t, &values, "CommandKey", command_key);

    retval = amxb_add(amxb_be_who_has("LocalAgent."), "LocalAgent.Request.", 0, NULL, &values, &ret, 5);
    path = GETP_CHAR(&ret, "0.path");

    if((retval != 0) || (path == NULL) || (*path == 0)) {
        SAH_TRACEZ_ERROR(ME, "Failed to add LocalAgent.Request. objects for command: %s", command);
        goto exit;
    }

    uspa_request_new(&request, path);

exit:
    amxc_var_clean(&values);
    amxc_var_clean(&ret);

    return request;
}

void uspa_request_dm_del(const char* request_path) {
    amxd_path_t path;
    amxc_var_t ret;
    uint32_t index = 0;
    const char* cpath = NULL;

    amxc_var_init(&ret);
    amxd_path_init(&path, request_path);

    index = uspa_request_index_from_path(&path);
    cpath = amxd_path_get(&path, AMXD_OBJECT_TERMINATE);

    amxb_del(amxb_be_who_has("LocalAgent."), cpath, index, NULL, &ret, 5);

    amxc_var_clean(&ret);
    amxd_path_clean(&path);
}
