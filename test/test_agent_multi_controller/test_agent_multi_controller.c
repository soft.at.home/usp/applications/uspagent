/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <poll.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include <usp/uspl.h>
#include <imtp/imtp_message.h>

#include "uspa.h"
#include "uspa_imtp.h"
#include "uspa_msghandler.h"
#include "test_agent_multi_controller.h"
#include "dummy_be.h"
#include "mock.h"
#include "common.h"

#define UNUSED __attribute__((unused))

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;

static amxo_parser_t parser;
static const char* odl_config = "../common/test_config.odl";
static const char* la_definition = "../common/tr181-localagent_definition.odl";
static const char* la_defaults = "../common/tr181-localagent_mtp_enabled.odl";
static const char* mqtt_definition = "../common/tr181-mqtt_definition.odl";
static const char* mqtt_defaults = "../common/tr181-mqtt_defaults.odl";
static const char* phonebook_definition = "../common/phonebook_definition.odl";
static const char* phonebook_defaults = "../common/phonebook_defaults.odl";

static amxc_string_t* acl_file = NULL;

static int testpipe[2] = {};

static void uspa_initialize(void) {
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    uspi_con_t* con = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);

    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, phonebook_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, phonebook_defaults, root_obj), 0);

    will_return(__wrap_uspi_con_get_fd, testpipe[0]); // uspa_imtp_connect
    assert_int_equal(_uspa_main(0, &dm, &parser), 0);

    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "MQTT.Client.1.", 0, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    assert_true(GETP_BOOL(&ret, "0.0.Enable"));

    con = uspa_imtp_con_from_la_mtp("LocalAgent.MTP.1.", USPA_MTP_ID_MQTT);
    assert_non_null(con);

    amxc_var_clean(&ret);
}

int test_uspa_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    const char* role = "root";

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "GetMTPInfo", AMXO_FUNC(_Controller_GetMTPInfo));
    amxo_resolver_ftab_add(&parser, "DeferredCall", AMXO_FUNC(_Phonebook_DeferredCall));
    amxo_resolver_ftab_add(&parser, "CreateListenSocket", AMXO_FUNC(_Client_CreateListenSocket));

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxb_be_load("/usr/bin/mods/usp/mod-amxb-usp.so"), 0);

    handle_events();

    assert_int_equal(pipe(testpipe), 0);
    capture_sigalrm();

    uspa_initialize();
    acl_initialize(&parser, &acl_file, role);
    handle_events();

    return 0;
}

int test_uspa_teardown(UNUSED void** state) {
    amxp_signal_t* sig = NULL;

    will_return(__wrap_uspi_con_get_fd, testpipe[0]);   // uspa_imtp_disconnect
    will_return(__wrap_uspi_con_disconnect, 1);         // uspa_imtp_disconnect
    assert_int_equal(_uspa_main(1, &dm, &parser), 0);
    handle_events();

    close(testpipe[0]);
    close(testpipe[1]);
    acl_clean(&acl_file);

    assert_int_equal(amxb_be_remove("usp"), 0);
    amxb_free(&bus_ctx);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

static void add_enabled_controller(void) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LocalAgent.Controller.");
    amxd_trans_add_inst(&trans, 0, "controller-2");
    amxd_trans_set_value(cstring_t, &trans, "EndpointID", "controller-2");
    amxd_trans_set_value(cstring_t, &trans, "AssignedRole", "Device.LocalAgent.ControllerTrust.Role.1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LocalAgent.Controller.controller-2.MTP.");
    amxd_trans_add_inst(&trans, 0, "mtp-test");
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "MQTT");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LocalAgent.Controller.controller-2.MTP.mtp-test.MQTT.");
    amxd_trans_set_value(cstring_t, &trans, "AgentMTPReference", "LocalAgent.MTP.1");
    amxd_trans_set_value(cstring_t, &trans, "Topic", "controller_test");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LocalAgent.Controller.controller-2.MTP.mtp-test.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_clean(&trans);
}

void test_multiple_controllers_can_subscribe_to_same_event(UNUSED void** state) {
    const char* notif_type = "ValueChange";
    const char* sub_path = "MQTT.Client.1.";
    const char* name = "Kowalski";
    int8_t* buffer = calloc(1, 1024);
    int8_t* buffer2 = NULL;
    imtp_frame_t* frame = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;
    int header_len = 8;
    int readlen = 0;
    int frame1_len = 0;
    amxc_var_t notification;
    amxc_var_t values;
    amxc_var_t ret;

    amxc_var_init(&notification);
    amxc_var_init(&values);
    amxc_var_init(&ret);

    add_enabled_controller();
    add_subscription_instance(&dm, notif_type, sub_path, "LocalAgent.Controller.1.");
    add_subscription_instance(&dm, notif_type, sub_path, "LocalAgent.Controller.2.");
    handle_events();

    // Trigger ValueChange notification
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Name", name);
    assert_int_equal(amxb_set(bus_ctx, sub_path, &values, &ret, 5), 0);

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    readlen = read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_frame_parse(&frame, buffer), 0);
    frame1_len = frame->length;

    tlv_proto = imtp_frame_get_first_tlv(frame, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);
    assert_string_equal(uspl_msghandler_to_id(usp_notify), "controller");

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "value_change.param_path", "MQTT.Client.1.Name"));
    assert_true(test_verify_data(&notification, "value_change.param_value", name));

    // The second message will start right after the first with an offset of some header bytes
    buffer2 = calloc(1, readlen - frame1_len - header_len);
    memcpy(buffer2, buffer + frame1_len + header_len, readlen - frame1_len - header_len);

    // Clean up first received IMTP message after copying data to new buffer to avoid losing it
    imtp_frame_delete(&frame);
    uspl_rx_delete(&usp_notify);

    assert_int_equal(imtp_frame_parse(&frame, buffer2), 0);
    tlv_proto = imtp_frame_get_first_tlv(frame, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);
    assert_string_equal(uspl_msghandler_to_id(usp_notify), "controller-2");

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "value_change.param_path", "MQTT.Client.1.Name"));
    assert_true(test_verify_data(&notification, "value_change.param_value", name));

    imtp_frame_delete(&frame);
    uspl_rx_delete(&usp_notify);
    amxc_var_clean(&ret);
    amxc_var_clean(&values);
    amxc_var_clean(&notification);
}

// Multiple controllers can use the same LocalAgent.MTP.{i}. reference. Disabling a
// LocalAgent.Controller.{i}. instance must not disable the IMTP for the other controller.
// Note: Did not disable subscription in previous test.
void test_disabling_controller_does_not_disable_imtp(UNUSED void** state) {
    const char* notif_type = "ValueChange";
    const char* sub_path = "MQTT.Client.1.";
    const char* name = "Skipper";
    int8_t* buffer = calloc(1, 1024);
    imtp_frame_t* frame = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;
    int header_len = 8;
    int readlen = 0;
    int frame_len = 0;
    amxc_var_t notification;
    amxc_var_t values;
    amxc_var_t ret;
    amxd_trans_t trans;

    amxc_var_init(&notification);
    amxc_var_init(&values);
    amxc_var_init(&ret);
    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "LocalAgent.Controller.controller-2.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    handle_events();

    // Trigger ValueChange notification
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Name", name);
    assert_int_equal(amxb_set(bus_ctx, sub_path, &values, &ret, 5), 0);

    // Only send notification to enabled controller
    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    readlen = read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_frame_parse(&frame, buffer), 0);
    frame_len = frame->length;

    tlv_proto = imtp_frame_get_first_tlv(frame, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);
    assert_string_equal(uspl_msghandler_to_id(usp_notify), "controller");

    uspl_notify_extract(usp_notify, &notification);
    amxc_var_dump(&notification, STDOUT_FILENO);

    assert_true(test_verify_data(&notification, "value_change.param_path", "MQTT.Client.1.Name"));
    assert_true(test_verify_data(&notification, "value_change.param_value", name));

    // Notification is only sent to enabled controller, so readlen should be equal to frame_len + frame_header_len
    assert_int_equal(readlen, frame_len + header_len);

    uspl_rx_delete(&usp_notify);
    imtp_frame_delete(&frame);
    amxd_trans_clean(&trans);
    amxc_var_clean(&ret);
    amxc_var_clean(&values);
    amxc_var_clean(&notification);
}
