/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include "uspa.h"
#include "test_agent_start_stop.h"
#include "dummy_be.h"
#include "mock.h"
#include "common.h"

#define UNUSED __attribute__((unused))

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxo_parser_t parser;

static const char* la_definition = "../common/tr181-localagent_definition.odl";
static const char* la_defaults = "../common/tr181-localagent_mtp_enabled.odl";
static const char* mqtt_definition = "../common/tr181-mqtt_definition.odl";
static const char* mqtt_defaults = "../common/tr181-mqtt_defaults.odl";

static int con_fd = 123;

int test_uspa_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "CreateListenSocket", AMXO_FUNC(_Client_CreateListenSocket));
    amxo_resolver_ftab_add(&parser, "UpdateStatus", AMXO_FUNC(_UpdateStatus));

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    // Register data model
    amxb_register(bus_ctx, &dm);

    // Load USP back end
    assert_int_equal(amxb_be_load("/usr/bin/mods/usp/mod-amxb-usp.so"), 0);

    handle_events();

    return 0;
}

int test_uspa_teardown(UNUSED void** state) {
    assert_int_equal(amxb_be_remove("usp"), 0);
    amxb_free(&bus_ctx);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_can_start_app(UNUSED void** state) {
    assert_int_equal(_uspa_main(0, &dm, &parser), 0);
}

void test_can_stop_app(UNUSED void** state) {
    assert_int_equal(_uspa_main(1, &dm, &parser), 0);
    handle_events();
}

void test_can_start_app_with_defaults(UNUSED void** state) {
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    amxc_var_t ret;

    amxc_var_init(&ret);

    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_defaults, root_obj), 0);

    will_return(__wrap_uspi_con_get_fd, con_fd); // uspa_imtp_connect
    assert_int_equal(_uspa_main(0, &dm, &parser), 0);

    // Status is Down before handling events and Up after handling events
    // i.e. Status is not set to Up before entry-point has run completely
    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Status", 0, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_string_equal(GETP_CHAR(&ret, "0.0.Status"), "Down");

    handle_events();
    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Status", 0, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_string_equal(GETP_CHAR(&ret, "0.0.Status"), "Up");

    assert_int_equal(amxb_get(bus_ctx, "MQTT.Client.cpe-mqtt-test.", 0, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    assert_true(GETP_BOOL(&ret, "0.0.Enable"));

    amxc_var_clean(&ret);
}

void test_can_stop_app_with_defaults(UNUSED void** state) {
    will_return(__wrap_uspi_con_get_fd, con_fd);        // uspa_imtp_disconnect
    will_return(__wrap_uspi_con_disconnect, 1);         // uspa_imtp_disconnect
    assert_int_equal(_uspa_main(1, &dm, &parser), 0);
    handle_events();
}
