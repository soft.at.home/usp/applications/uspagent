/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include <usp/uspl.h>
#include <imtp/imtp_message.h>

#include "uspa.h"
#include "uspa_imtp.h"
#include "uspa_agent.h"
#include "uspa_imtp_mqtt.h"

#include "test_agent_reconnect_mqtt.h"
#include "dummy_be.h"
#include "mock.h"
#include "common.h"

#define UNUSED __attribute__((unused))

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxo_parser_t parser;

static const char* odl_config = "../common/test_config.odl";
static const char* odl_extra = "../../odl/uspagent_extra.odl";
static const char* la_definition = "../common/tr181-localagent_definition.odl";
static const char* la_defaults = "../common/tr181-localagent_mtp_enabled.odl";
static const char* la_autoreconnect = "tr181-localagent_autoreconnect.odl";
static const char* mqtt_config = "mqtt_config.odl";
static const char* mqtt_definition = "../common/tr181-mqtt_definition.odl";
static const char* mqtt_defaults = "../common/tr181-mqtt_defaults.odl";

static int testpipe[2] = {};
static int force_reconnect_count = 0;

static amxd_status_t _Client_ForceReconnect(UNUSED amxd_object_t* object,
                                            UNUSED amxd_function_t* func,
                                            UNUSED amxc_var_t* args,
                                            UNUSED amxc_var_t* ret) {
    force_reconnect_count++;
    return amxd_status_ok;
}

int test_uspa_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "CreateListenSocket", AMXO_FUNC(_Client_CreateListenSocket));
    amxo_resolver_ftab_add(&parser, "ForceReconnect", AMXO_FUNC(_Client_ForceReconnect));

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    // Register data model
    amxb_register(bus_ctx, &dm);

    // Load USP back end
    assert_int_equal(amxb_be_load("/usr/bin/mods/usp/mod-amxb-usp.so"), 0);

    handle_events();

    assert_int_equal(pipe(testpipe), 0);

    return 0;
}

int test_uspa_teardown(UNUSED void** state) {
    will_return(__wrap_uspi_con_get_fd, testpipe[0]);   // uspa_imtp_disconnect
    will_return(__wrap_uspi_con_disconnect, 1);         // uspa_imtp_disconnect
    assert_int_equal(_uspa_main(1, &dm, &parser), 0);
    handle_events();

    close(testpipe[0]);
    close(testpipe[1]);

    assert_int_equal(amxb_be_remove("usp"), 0);
    amxb_free(&bus_ctx);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_auto_reconnect_true_creates_subscription(UNUSED void** state) {
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    amxc_var_t ret;
    amxc_var_t values;

    amxc_var_init(&ret);
    amxc_var_init(&values);

    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_extra, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_autoreconnect, root_obj), 0);
    handle_events();

    will_return(__wrap_uspi_con_get_fd, testpipe[0]); // uspa_imtp_connect
    assert_int_equal(_uspa_main(0, &dm, &parser), 0);

    handle_events();
    assert_int_equal(force_reconnect_count, 0);

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "BrokerAddress", "test.mosquitto.org");
    assert_int_equal(amxb_set(bus_ctx, "MQTT.Client.cpe-mqtt-test.", &values, &ret, 5), 0);

    handle_events();
    assert_int_equal(force_reconnect_count, 1);

    // Different parameter change does not trigger ForceReconnect
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &values, "ConnectRetryTime", 10);
    assert_int_equal(amxb_set(bus_ctx, "MQTT.Client.cpe-mqtt-test.", &values, &ret, 5), 0);

    handle_events();
    assert_int_equal(force_reconnect_count, 1);

    amxc_var_clean(&values);
    amxc_var_clean(&ret);
}

void test_auto_reconnect_disabled_unsubscribes(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t values;

    amxc_var_init(&ret);
    amxc_var_init(&values);

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &values, "X_SOFTATHOME-COM_AutoReconnect", false);
    assert_int_equal(amxb_set(bus_ctx, "LocalAgent.MTP.mtp-test.MQTT", &values, &ret, 5), 0);

    handle_events();

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "BrokerAddress", "broker.hivemq.com");
    assert_int_equal(amxb_set(bus_ctx, "MQTT.Client.cpe-mqtt-test.", &values, &ret, 5), 0);

    handle_events();
    assert_int_equal(force_reconnect_count, 1);

    amxc_var_clean(&values);
    amxc_var_clean(&ret);
}

void test_auto_reconnect_enabled_subscribes(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t values;

    amxc_var_init(&ret);
    amxc_var_init(&values);

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &values, "X_SOFTATHOME-COM_AutoReconnect", true);
    assert_int_equal(amxb_set(bus_ctx, "LocalAgent.MTP.mtp-test.MQTT", &values, &ret, 5), 0);

    handle_events();

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "BrokerAddress", "test.mosquitto.org");
    assert_int_equal(amxb_set(bus_ctx, "MQTT.Client.cpe-mqtt-test.", &values, &ret, 5), 0);

    handle_events();
    assert_int_equal(force_reconnect_count, 2);

    amxc_var_clean(&values);
    amxc_var_clean(&ret);
}

void test_controller_mtp_deleted_unsubscribes(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t values;

    amxc_var_init(&ret);
    amxc_var_init(&values);

    assert_int_equal(amxb_del(bus_ctx, "LocalAgent.MTP.mtp-test.", 0, NULL, &ret, 5), 0);

    will_return(__wrap_uspi_con_get_fd, testpipe[0]);   // uspa_imtp_disconnect
    will_return(__wrap_uspi_con_disconnect, 1);         // uspa_imtp_disconnect
    handle_events();

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "BrokerAddress", "broker.hivemq.com");
    assert_int_equal(amxb_set(bus_ctx, "MQTT.Client.cpe-mqtt-test.", &values, &ret, 5), 0);

    handle_events();
    assert_int_equal(force_reconnect_count, 2);

    amxc_var_clean(&values);
    amxc_var_clean(&ret);
}

void test_controller_mtp_added_subscribes(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t values;

    amxc_var_init(&ret);
    amxc_var_init(&values);

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Protocol", "MQTT");
    amxc_var_add_key(bool, &values, "Enable", false);
    assert_int_equal(amxb_add(bus_ctx, "LocalAgent.MTP.", 0, "mtp-test", &values, &ret, 5), 0);
    handle_events();

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "Reference", "MQTT.Client.1");
    amxc_var_add_key(bool, &values, "X_SOFTATHOME-COM_AutoReconnect", true);
    assert_int_equal(amxb_set(bus_ctx, "LocalAgent.MTP.mtp-test.MQTT.", &values, &ret, 5), 0);
    handle_events();

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &values, "Enable", true);
    assert_int_equal(amxb_set(bus_ctx, "LocalAgent.MTP.mtp-test.", &values, &ret, 5), 0);

    will_return(__wrap_uspi_con_get_fd, testpipe[0]); // uspa_imtp_connect
    handle_events();

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "BrokerAddress", "test.mosquitto.org");
    assert_int_equal(amxb_set(bus_ctx, "MQTT.Client.cpe-mqtt-test.", &values, &ret, 5), 0);

    handle_events();
    assert_int_equal(force_reconnect_count, 3);

    amxc_var_clean(&values);
    amxc_var_clean(&ret);
}
