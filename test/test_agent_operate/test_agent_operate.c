/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <poll.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include <usp/uspl.h>
#include <imtp/imtp_message.h>

#include "uspa.h"
#include "uspa_imtp.h"
#include "uspa_msghandler.h"
#include "test_agent_operate.h"
#include "dummy_be.h"
#include "mock.h"
#include "common.h"

#define UNUSED __attribute__((unused))

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;

static amxo_parser_t parser;
static const char* odl_config = "../common/test_config.odl";
static const char* la_definition = "../common/tr181-localagent_definition.odl";
static const char* la_defaults = "../common/tr181-localagent_mtp_enabled.odl";
static const char* mqtt_definition = "../common/tr181-mqtt_definition.odl";
static const char* mqtt_defaults = "../common/tr181-mqtt_defaults.odl";
static const char* phonebook_definition = "../common/phonebook_definition.odl";
static const char* phonebook_defaults = "../common/phonebook_defaults.odl";
static const char* device_odl = "device.odl";

static const char* role = "root";
static const char* agent_eid = "proto::agent-test";
static const char* controller_eid = "controller";
static amxc_string_t* acl_file = NULL;
static uint32_t acl_order = 1;

static int testpipe[2] = {};

static amxd_status_t _Phonebook_CallNumber(UNUSED amxd_object_t* object,
                                           UNUSED amxd_function_t* func,
                                           amxc_var_t* args,
                                           UNUSED amxc_var_t* ret) {
    uint32_t number = GET_UINT32(args, "number");
    printf("Calling number: %d\n", number);
    fflush(stdout);

    assert_non_null(GET_ARG(args, "_CommandKey"));
    assert_non_null(GET_ARG(args, "_Requestor"));

    return amxd_status_ok;
}

static amxd_status_t _Phonebook_Teleport(UNUSED amxd_object_t* object,
                                         UNUSED amxd_function_t* func,
                                         amxc_var_t* args,
                                         UNUSED amxc_var_t* ret) {
    printf("Teleport failed, because phonebooks can't teleport...\n");
    fflush(stdout);

    amxc_var_add_key(uint32_t, args, "err_code", 9000);
    amxc_var_add_key(cstring_t, args, "err_msg", "Phonebook can't teleport");

    return amxd_status_unknown_error;
}

static amxd_status_t _Phonebook_GetRandomNumber(UNUSED amxd_object_t* object,
                                                UNUSED amxd_function_t* func,
                                                amxc_var_t* args,
                                                UNUSED amxc_var_t* ret) {
    printf("GetRandomNumber\n");
    fflush(stdout);

    amxc_var_add_key(uint32_t, args, "RandomNumber", 321);

    return amxd_status_ok;
}

static amxd_status_t _Phonebook_SyncFunc(UNUSED amxd_object_t* object,
                                         UNUSED amxd_function_t* func,
                                         amxc_var_t* args,
                                         UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_ok;
    bool success = GET_BOOL(args, "success");
    printf("SyncFunc will succeed/fail: %d\n", success);
    fflush(stdout);

    if(success) {
        amxc_var_add_key(cstring_t, args, "text", "Text");
        amxc_var_add_key(uint32_t, args, "number", 55);
    } else {
        amxc_var_add_key(uint32_t, args, "err_code", USP_ERR_COMMAND_FAILURE);
        amxc_var_add_key(cstring_t, args, "err_msg", uspl_error_code_to_str(USP_ERR_COMMAND_FAILURE));
        status = amxd_status_unknown_error;
    }

    return status;
}

static amxd_status_t _Phonebook_ProtectedMethodSync(UNUSED amxd_object_t* object,
                                                    UNUSED amxd_function_t* func,
                                                    UNUSED amxc_var_t* args,
                                                    UNUSED amxc_var_t* ret) {
    printf("ProtectedMethodSync\n");
    fflush(stdout);
    return amxd_status_ok;
}

static amxd_status_t _Phonebook_ProtectedMethodAsync(UNUSED amxd_object_t* object,
                                                     UNUSED amxd_function_t* func,
                                                     UNUSED amxc_var_t* args,
                                                     UNUSED amxc_var_t* ret) {
    printf("ProtectedMethodAsync\n");
    fflush(stdout);
    return amxd_status_ok;
}

static amxd_status_t _Device_Reboot(UNUSED amxd_object_t* object,
                                    UNUSED amxd_function_t* func,
                                    UNUSED amxc_var_t* args,
                                    UNUSED amxc_var_t* ret) {
    printf("Rebooting box...\n");
    return amxd_status_ok;
}

static void fill_callnumber_var(amxc_var_t* operate_var, const char* cmd_key) {
    amxc_var_t* input_args = NULL;

    amxc_var_add_key(cstring_t, operate_var, "command", "Device.Phonebook.Contact.1.CallNumber()");
    amxc_var_add_key(cstring_t, operate_var, "command_key", cmd_key);
    amxc_var_add_key(bool, operate_var, "send_resp", true);
    input_args = amxc_var_add_key(amxc_htable_t, operate_var, "input_args", NULL);
    amxc_var_add_key(uint32_t, input_args, "number", 123);
}

static void fill_teleport_var(amxc_var_t* operate_var) {
    amxc_var_add_key(cstring_t, operate_var, "command", "Device.Phonebook.Contact.1.Teleport()");
    amxc_var_add_key(cstring_t, operate_var, "command_key", "teleport");
    amxc_var_add_key(bool, operate_var, "send_resp", true);
}

static void fill_getrandomnumber_var(amxc_var_t* operate_var) {
    amxc_var_add_key(cstring_t, operate_var, "command", "Device.Phonebook.Contact.1.GetRandomNumber()");
    amxc_var_add_key(cstring_t, operate_var, "command_key", "getrandomnumber");
    amxc_var_add_key(bool, operate_var, "send_resp", true);
}

static void fill_syncfunc_var(amxc_var_t* operate_var, bool success, bool send_resp) {
    amxc_var_t* input_args = NULL;

    amxc_var_add_key(cstring_t, operate_var, "command", "Device.Phonebook.Contact.1.SyncFunc()");
    amxc_var_add_key(cstring_t, operate_var, "command_key", "sync_func");
    amxc_var_add_key(bool, operate_var, "send_resp", send_resp);
    input_args = amxc_var_add_key(amxc_htable_t, operate_var, "input_args", NULL);
    amxc_var_add_key(bool, input_args, "success", success);
}

static int add_subscription_oper_complete(void) {
    amxd_object_t* sub_obj = amxd_dm_findf(&dm, "LocalAgent.Subscription.");
    amxd_trans_t trans;
    amxc_var_t ret;
    amxc_var_t* get_response = NULL;
    uint32_t index = 0;
    amxc_string_t subs_path;

    amxc_string_init(&subs_path, 0);
    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_add_inst(&trans, 0, "sub_1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "ID", "oper_complete");
    amxd_trans_set_value(cstring_t, &trans, "NotifType", "OperationComplete");
    amxd_trans_set_value(cstring_t, &trans, "Recipient", "LocalAgent.Controller.1.");
    amxd_trans_set_value(cstring_t, &trans, "ReferenceList", "Device.Phonebook.Contact.1.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    index = GETP_UINT32(&trans.retvals, "1.index");
    amxd_trans_clean(&trans);

    handle_events();

    amxc_string_setf(&subs_path, "LocalAgent.Subscription.%d.", index);
    assert_int_equal(amxb_get(bus_ctx, amxc_string_get(&subs_path, 0), 0, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    get_response = GETI_ARG(&ret, 0);
    get_response = GET_ARG(get_response, amxc_string_get(&subs_path, 0));
    assert_non_null(get_response);
    assert_false(amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, get_response)));
    assert_int_equal(strcmp("oper_complete", GET_CHAR(get_response, "ID")), 0);

    amxc_string_clean(&subs_path);
    amxc_var_clean(&ret);

    return index;
}

static void del_subscription(uint32_t index) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LocalAgent.Subscription.");
    amxd_trans_del_inst(&trans, index, NULL);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
}

static void add_controller(const char* alias, const char* endpoint_id, bool protected_access) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LocalAgent.Controller.");
    amxd_trans_add_inst(&trans, 0, alias);
    amxd_trans_set_value(cstring_t, &trans, "EndpointID", endpoint_id);
    amxd_trans_set_value(cstring_t, &trans, "AssignedRole", "Device.LocalAgent.ControllerTrust.Role.1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(bool, &trans, "ProtectedAccess", protected_access);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);

    handle_events();

    amxd_trans_clean(&trans);
}

static void uspa_initialize(void) {
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    uspi_con_t* con = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);

    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, phonebook_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, phonebook_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, device_odl, root_obj), 0);

    will_return(__wrap_uspi_con_get_fd, testpipe[0]); // uspa_imtp_connect
    assert_int_equal(_uspa_main(0, &dm, &parser), 0);

    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "MQTT.Client.1.", 0, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    assert_true(GETP_BOOL(&ret, "0.0.Enable"));

    con = uspa_imtp_con_from_la_mtp("LocalAgent.MTP.1.", USPA_MTP_ID_MQTT);
    assert_non_null(con);

    amxc_var_clean(&ret);
}

static void restrict_access(const char* path) {
    amxc_string_t filepath;
    amxc_var_t* acls = NULL;
    amxc_var_t* new_acls = NULL;
    const char* acl_dir = GET_CHAR(&parser.config, "acl_dir");

    amxc_string_init(&filepath, 0);
    amxc_string_setf(&filepath, "%s/%s/", acl_dir, role);
    acls = amxa_parse_files(amxc_string_get(&filepath, 0));
    assert_non_null(acls);

    new_acls = amxc_var_add_key(amxc_htable_t, acls, path, NULL);
    amxc_var_add_key(cstring_t, new_acls, "CommandEvent", "----");
    amxc_var_add_key(uint32_t, new_acls, "Order", ++acl_order);

    amxc_string_setf(&filepath, "%s/merged/%s.json", acl_dir, role);
    assert_int_equal(amxa_merge_rules(acls, amxc_string_get(&filepath, 0)), 0);

    amxc_var_delete(&acls);
    amxc_string_clean(&filepath);
}

int test_uspa_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "GetMTPInfo", AMXO_FUNC(_Controller_GetMTPInfo));
    amxo_resolver_ftab_add(&parser, "CreateListenSocket", AMXO_FUNC(_Client_CreateListenSocket));
    amxo_resolver_ftab_add(&parser, "CallNumber", AMXO_FUNC(_Phonebook_CallNumber));
    amxo_resolver_ftab_add(&parser, "Teleport", AMXO_FUNC(_Phonebook_Teleport));
    amxo_resolver_ftab_add(&parser, "GetRandomNumber", AMXO_FUNC(_Phonebook_GetRandomNumber));
    amxo_resolver_ftab_add(&parser, "SyncFunc", AMXO_FUNC(_Phonebook_SyncFunc));
    amxo_resolver_ftab_add(&parser, "ProtectedMethodSync", AMXO_FUNC(_Phonebook_ProtectedMethodSync));
    amxo_resolver_ftab_add(&parser, "ProtectedMethodAsync", AMXO_FUNC(_Phonebook_ProtectedMethodAsync));
    amxo_resolver_ftab_add(&parser, "Reboot", AMXO_FUNC(_Device_Reboot));

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxb_be_load("/usr/bin/mods/usp/mod-amxb-usp.so"), 0);

    handle_events();

    assert_int_equal(pipe(testpipe), 0);

    uspa_initialize();
    acl_initialize(&parser, &acl_file, role);

    return 0;
}

int test_uspa_teardown(UNUSED void** state) {
    will_return(__wrap_uspi_con_get_fd, testpipe[0]);   // uspa_imtp_disconnect
    will_return(__wrap_uspi_con_disconnect, 1);         // uspa_imtp_disconnect
    assert_int_equal(_uspa_main(1, &dm, &parser), 0);
    handle_events();

    close(testpipe[0]);
    close(testpipe[1]);
    acl_clean(&acl_file);

    assert_int_equal(amxb_be_remove("usp"), 0);
    amxb_free(&bus_ctx);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_can_invoke_async_operate(UNUSED void** state) {
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    amxc_var_init(&operate_var);

    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    fill_callnumber_var(&operate_var, "callnumber");

    assert_int_equal(uspl_tx_new(&usp_operate_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", "Device.Phonebook.Contact.1.CallNumber()"));
    assert_true(test_verify_data(result, "req_obj_path", "LocalAgent.Request.1."));

    handle_events();

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&operate_var);
}

static void test_async_operate_sends_event_common(void) {
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;
    int8_t* buffer = calloc(1, 1024);
    imtp_frame_t* frame = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;
    amxc_var_t oper_complete;
    uint32_t subs_index;

    amxc_llist_init(&resp_list);
    amxc_var_init(&oper_complete);
    amxc_var_init(&operate_var);

    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    fill_callnumber_var(&operate_var, "callnumber");

    subs_index = add_subscription_oper_complete();

    assert_int_equal(uspl_tx_new(&usp_operate_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", "Device.Phonebook.Contact.1.CallNumber()"));
    assert_non_null(GET_ARG(result, "req_obj_path"));

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    assert_true(read(testpipe[0], buffer, 1024) > 0);
    assert_int_equal(imtp_frame_parse(&frame, buffer), 0);

    tlv_proto = imtp_frame_get_first_tlv(frame, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &oper_complete);
    amxc_var_dump(&oper_complete, STDOUT_FILENO);
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_key", "callnumber"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_name", "CallNumber()"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.path", "Device.Phonebook.Contact.1."));
    assert_true(test_verify_data(&oper_complete, "subscription_id", "oper_complete"));

    del_subscription(subs_index);
    uspl_rx_delete(&usp_notify);
    imtp_frame_delete(&frame);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&oper_complete);
    amxc_var_clean(&operate_var);
}

void test_async_operate_sends_event(UNUSED void** state) {
    test_async_operate_sends_event_common();
}

// Tests that the OperationComplete event is sent when AgentMTPReference has a Device prefix
void test_async_operate_sends_event_v2(UNUSED void** state) {
    amxd_object_t* object = amxd_dm_findf(&dm, "LocalAgent.Controller.1.MTP.1.MQTT.");
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    assert_non_null(object);

    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, "AgentMTPReference", "Device.LocalAgent.MTP.1");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);

    test_async_operate_sends_event_common();

    amxd_trans_clean(&trans);
}

void test_failed_async_operate_sends_event(UNUSED void** state) {
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t oper_complete;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;
    int8_t* buffer = calloc(1, 1024);
    imtp_frame_t* frame = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;
    uint32_t subs_index = 0;

    amxc_llist_init(&resp_list);
    amxc_var_init(&oper_complete);
    amxc_var_init(&operate_var);

    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    fill_teleport_var(&operate_var);

    subs_index = add_subscription_oper_complete();

    assert_int_equal(uspl_tx_new(&usp_operate_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", "Device.Phonebook.Contact.1.Teleport()"));
    assert_non_null(GET_ARG(result, "req_obj_path"));

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    assert_true(read(testpipe[0], buffer, 1024) > 0);
    assert_int_equal(imtp_frame_parse(&frame, buffer), 0);

    tlv_proto = imtp_frame_get_first_tlv(frame, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &oper_complete);
    amxc_var_dump(&oper_complete, STDOUT_FILENO);
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_key", "teleport"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_name", "Teleport()"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.path", "Device.Phonebook.Contact.1."));
    assert_true(test_verify_data(&oper_complete, "subscription_id", "oper_complete"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_failure.err_code", "9000"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_failure.err_msg", "Phonebook can't teleport"));

    del_subscription(subs_index);
    uspl_rx_delete(&usp_notify);
    imtp_frame_delete(&frame);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&oper_complete);
    amxc_var_clean(&operate_var);
}

static void test_disabled_subscription_disconnects_slots(uint32_t subs_index) {
    amxd_object_t* sub_obj = NULL;
    amxd_trans_t trans;
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;
    struct pollfd fds[1] = {};

    sub_obj = amxd_dm_findf(&dm, "LocalAgent.Subscription.%d.", subs_index);
    assert_non_null(sub_obj);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    amxc_llist_init(&resp_list);
    amxc_var_init(&operate_var);

    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    fill_callnumber_var(&operate_var, "callnumber");

    assert_int_equal(uspl_tx_new(&usp_operate_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", "Device.Phonebook.Contact.1.CallNumber()"));
    assert_non_null(GET_ARG(result, "req_obj_path"));

    handle_events();
    fds[0].fd = testpipe[0];
    fds[0].events = POLLIN;

    assert_int_equal(poll(fds, 1, 1), 0);

    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    amxc_var_clean(&operate_var);
}

static void test_enabled_subscription_connects_slots(uint32_t subs_index) {
    amxd_object_t* sub_obj = NULL;
    amxd_trans_t trans;
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;
    int8_t* buffer = calloc(1, 1024);
    imtp_frame_t* frame = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;
    amxc_var_t oper_complete;

    amxc_llist_init(&resp_list);
    amxc_var_init(&oper_complete);
    amxc_var_init(&operate_var);

    sub_obj = amxd_dm_findf(&dm, "LocalAgent.Subscription.%d.", subs_index);
    assert_non_null(sub_obj);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    fill_callnumber_var(&operate_var, "callnumber");

    assert_int_equal(uspl_tx_new(&usp_operate_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", "Device.Phonebook.Contact.1.CallNumber()"));
    assert_non_null(GET_ARG(result, "req_obj_path"));

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    assert_true(read(testpipe[0], buffer, 1024) > 0);
    assert_int_equal(imtp_frame_parse(&frame, buffer), 0);

    tlv_proto = imtp_frame_get_first_tlv(frame, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &oper_complete);
    amxc_var_dump(&oper_complete, STDOUT_FILENO);

    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_key", "callnumber"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_name", "CallNumber()"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.path", "Device.Phonebook.Contact.1."));
    assert_true(test_verify_data(&oper_complete, "subscription_id", "oper_complete"));

    uspl_rx_delete(&usp_notify);
    imtp_frame_delete(&frame);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&oper_complete);
    amxc_var_clean(&operate_var);
}

void test_toggled_subscription_connects_disconnects_slots(UNUSED void** state) {
    uint32_t subs_index = add_subscription_oper_complete();
    test_disabled_subscription_disconnects_slots(subs_index);
    test_enabled_subscription_connects_slots(subs_index);
    del_subscription(subs_index);
}

void test_async_operate_with_output_args(UNUSED void** state) {
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;
    int8_t* buffer = calloc(1, 1024);
    imtp_frame_t* frame = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;
    amxc_var_t oper_complete;
    uint32_t subs_index = 0;

    amxc_llist_init(&resp_list);
    amxc_var_init(&oper_complete);
    amxc_var_init(&operate_var);

    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    fill_getrandomnumber_var(&operate_var);
    subs_index = add_subscription_oper_complete();

    assert_int_equal(uspl_tx_new(&usp_operate_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", "Device.Phonebook.Contact.1.GetRandomNumber()"));
    assert_non_null(GET_ARG(result, "req_obj_path"));

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    assert_true(read(testpipe[0], buffer, 1024) > 0);
    assert_int_equal(imtp_frame_parse(&frame, buffer), 0);

    tlv_proto = imtp_frame_get_first_tlv(frame, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &oper_complete);
    amxc_var_dump(&oper_complete, STDOUT_FILENO);
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_key", "getrandomnumber"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_name", "GetRandomNumber()"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.path", "Device.Phonebook.Contact.1."));
    assert_true(test_verify_data(&oper_complete, "oper_complete.output_args.RandomNumber", "321"));
    assert_true(test_verify_data(&oper_complete, "subscription_id", "oper_complete"));

    del_subscription(subs_index);
    uspl_rx_delete(&usp_notify);
    imtp_frame_delete(&frame);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&oper_complete);
    amxc_var_clean(&operate_var);
}

void test_two_async_operates_have_unique_cmd_keys(UNUSED void** state) {
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;
    uint8_t* buffer = calloc(1, 1024);
    uint8_t* buffer2 = NULL;
    imtp_frame_t* frame = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;
    amxc_var_t oper_complete;
    int header_len = 8;
    int readlen = 0;
    int frame1_len = 0;
    uint32_t subs_index = 0;

    amxc_llist_init(&resp_list);
    amxc_var_init(&oper_complete);
    amxc_var_init(&operate_var);

    // First command
    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    fill_callnumber_var(&operate_var, "callnumber");
    subs_index = add_subscription_oper_complete();

    assert_int_equal(uspl_tx_new(&usp_operate_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", "Device.Phonebook.Contact.1.CallNumber()"));
    assert_non_null(GET_ARG(result, "req_obj_path"));

    // Clean up before next cmd
    uspl_tx_delete(&usp_operate_msg);
    uspl_tx_delete(&usp_tx);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    amxc_llist_clean(&resp_list, variant_list_it_free);

    // Second command
    amxc_var_clean(&operate_var);
    amxc_var_init(&operate_var);
    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    fill_callnumber_var(&operate_var, "callnumber-2");

    assert_int_equal(uspl_tx_new(&usp_operate_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", "Device.Phonebook.Contact.1.CallNumber()"));
    assert_non_null(GET_ARG(result, "req_obj_path"));

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    readlen = read(testpipe[0], buffer, 1024);
    assert_int_equal(imtp_frame_parse(&frame, buffer), 0);
    frame1_len = frame->length;

    tlv_proto = imtp_frame_get_first_tlv(frame, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &oper_complete);
    amxc_var_dump(&oper_complete, STDOUT_FILENO);
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_key", "callnumber"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_name", "CallNumber()"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.path", "Device.Phonebook.Contact.1."));
    assert_true(test_verify_data(&oper_complete, "subscription_id", "oper_complete"));

    // The second message will start right after the first with an offset of a few header bytes
    buffer2 = calloc(1, readlen - frame1_len - header_len);
    memcpy(buffer2, buffer + frame1_len + header_len, readlen - frame1_len - header_len);

    // Clean up first received IMTP message after copying data to new buffer to avoid losing it
    imtp_frame_delete(&frame);
    uspl_rx_delete(&usp_notify);

    assert_int_equal(imtp_frame_parse(&frame, buffer2), 0);
    tlv_proto = imtp_frame_get_first_tlv(frame, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &oper_complete);
    amxc_var_dump(&oper_complete, STDOUT_FILENO);
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_key", "callnumber-2"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_name", "CallNumber()"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.path", "Device.Phonebook.Contact.1."));
    assert_true(test_verify_data(&oper_complete, "subscription_id", "oper_complete"));

    del_subscription(subs_index);
    uspl_rx_delete(&usp_notify);
    imtp_frame_delete(&frame);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&oper_complete);
    amxc_var_clean(&operate_var);
}

void test_async_operate_without_braces(UNUSED void** state) {
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;
    int8_t* buffer = calloc(1, 1024);
    imtp_frame_t* frame = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;
    amxc_var_t oper_complete;
    uint32_t subs_index = 0;

    amxc_llist_init(&resp_list);
    amxc_var_init(&oper_complete);
    amxc_var_init(&operate_var);

    subs_index = add_subscription_oper_complete();
    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &operate_var, "command", "Device.Phonebook.Contact.1.GetRandomNumber");
    amxc_var_add_key(cstring_t, &operate_var, "command_key", "getrandomnumber");
    amxc_var_add_key(bool, &operate_var, "send_resp", true);

    assert_int_equal(uspl_tx_new(&usp_operate_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", "Device.Phonebook.Contact.1.GetRandomNumber"));

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    assert_true(read(testpipe[0], buffer, 1024) > 0);
    assert_int_equal(imtp_frame_parse(&frame, buffer), 0);

    tlv_proto = imtp_frame_get_first_tlv(frame, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &oper_complete);
    amxc_var_dump(&oper_complete, STDOUT_FILENO);
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_key", "getrandomnumber"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_name", "GetRandomNumber"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.path", "Device.Phonebook.Contact.1."));
    assert_true(test_verify_data(&oper_complete, "oper_complete.output_args.RandomNumber", "321"));
    assert_true(test_verify_data(&oper_complete, "subscription_id", "oper_complete"));

    del_subscription(subs_index);
    uspl_rx_delete(&usp_notify);
    imtp_frame_delete(&frame);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&oper_complete);
    amxc_var_clean(&operate_var);
}

void test_async_operate_not_allowed(UNUSED void** state) {
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;
    int8_t* buffer = calloc(1, 1024);
    imtp_frame_t* frame = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;
    amxc_var_t oper_complete;
    uint32_t subs_index = 0;

    amxc_llist_init(&resp_list);
    amxc_var_init(&oper_complete);
    amxc_var_init(&operate_var);

    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    fill_getrandomnumber_var(&operate_var);
    subs_index = add_subscription_oper_complete();

    restrict_access("Device.Phonebook.Contact.*.GetRandomNumber()");

    assert_int_equal(uspl_tx_new(&usp_operate_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", "Device.Phonebook.Contact.1.GetRandomNumber()"));

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    assert_true(read(testpipe[0], buffer, 1024) > 0);
    assert_int_equal(imtp_frame_parse(&frame, buffer), 0);

    tlv_proto = imtp_frame_get_first_tlv(frame, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &oper_complete);
    amxc_var_dump(&oper_complete, STDOUT_FILENO);
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_key", "getrandomnumber"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_name", "GetRandomNumber()"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.path", "Device.Phonebook.Contact.1."));
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_failure.err_code", "7006"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_failure.err_msg", uspl_error_code_to_str(7006)));
    assert_true(test_verify_data(&oper_complete, "subscription_id", "oper_complete"));

    del_subscription(subs_index);
    uspl_rx_delete(&usp_notify);
    imtp_frame_delete(&frame);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&oper_complete);
    amxc_var_clean(&operate_var);
}

void test_async_operate_protected_allowed(UNUSED void** state) {
    const char* alias = "protected-controller";
    const char* endpoint_id = "proto::protected-controller";
    const char* cmd = "Device.Phonebook.Contact.1.ProtectedMethodAsync()";
    const char* cmd_key = "protected";
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;
    int8_t* buffer = calloc(1, 1024);
    imtp_frame_t* frame = NULL;
    const imtp_tlv_t* tlv_proto = NULL;
    uspl_rx_t* usp_notify = NULL;
    uint8_t* pbuf = NULL;
    amxc_var_t oper_complete;
    uint32_t subs_index = 0;

    subs_index = add_subscription_oper_complete();
    add_controller(alias, endpoint_id, true);

    amxc_llist_init(&resp_list);
    amxc_var_init(&oper_complete);
    amxc_var_init(&operate_var);

    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &operate_var, "command", cmd);
    amxc_var_add_key(cstring_t, &operate_var, "command_key", cmd_key);
    amxc_var_add_key(bool, &operate_var, "send_resp", true);

    assert_int_equal(uspl_tx_new(&usp_operate_msg, endpoint_id, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", cmd));

    will_return(__wrap_uspi_con_get_fd, testpipe[1]); // uspa_imtp_send
    handle_events();

    assert_true(read(testpipe[0], buffer, 1024) > 0);
    assert_int_equal(imtp_frame_parse(&frame, buffer), 0);

    tlv_proto = imtp_frame_get_first_tlv(frame, imtp_tlv_type_protobuf_bytes);
    assert_non_null(tlv_proto);
    pbuf = (uint8_t*) tlv_proto->value + tlv_proto->offset;
    usp_notify = uspl_msghandler_unpack_protobuf(pbuf, tlv_proto->length);
    assert_non_null(usp_notify);

    uspl_notify_extract(usp_notify, &oper_complete);
    amxc_var_dump(&oper_complete, STDOUT_FILENO);
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_key", cmd_key));
    assert_true(test_verify_data(&oper_complete, "oper_complete.cmd_name", "ProtectedMethodAsync()"));
    assert_true(test_verify_data(&oper_complete, "oper_complete.path", "Device.Phonebook.Contact.1."));
    assert_true(test_verify_data(&oper_complete, "subscription_id", "oper_complete"));
    assert_non_null(GETP_ARG(&oper_complete, "oper_complete.output_args"));

    del_subscription(subs_index);
    uspl_rx_delete(&usp_notify);
    imtp_frame_delete(&frame);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&oper_complete);
    amxc_var_clean(&operate_var);
}

void test_can_invoke_reboot(UNUSED void** state) {
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;
    const char* cmd_key = "reboot_key";
    amxd_object_t* object = NULL;
    amxc_var_t params;

    amxc_llist_init(&resp_list);
    amxc_var_init(&operate_var);
    amxc_var_init(&params);

    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &operate_var, "command", "Device.Reboot()");
    amxc_var_add_key(cstring_t, &operate_var, "command_key", cmd_key);
    amxc_var_add_key(bool, &operate_var, "send_resp", true);

    assert_int_equal(uspl_tx_new(&usp_operate_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", "Device.Reboot()"));

    object = amxd_dm_findf(&dm, "LocalAgent.");
    amxd_object_get_params(object, &params, amxd_dm_access_protected);

    assert_string_equal(GET_CHAR(&params, "CommandKey"), cmd_key);
    assert_string_equal(GET_CHAR(&params, "Cause"), "RemoteReboot");

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&operate_var);
    amxc_var_clean(&params);
}

void test_sync_operate_with_output_args(UNUSED void** state) {
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    amxc_var_init(&operate_var);

    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    fill_syncfunc_var(&operate_var, true, true);

    assert_int_equal(uspl_tx_new(&usp_operate_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", "Device.Phonebook.Contact.1.SyncFunc()"));
    assert_true(test_verify_data(result, "output_args.text", "Text"));
    assert_true(test_verify_data(result, "output_args.number", "55"));

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&operate_var);
}

void test_sync_operate_with_output_args_failure(UNUSED void** state) {
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    amxc_var_init(&operate_var);

    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    fill_syncfunc_var(&operate_var, false, true);

    assert_int_equal(uspl_tx_new(&usp_operate_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", "Device.Phonebook.Contact.1.SyncFunc()"));
    assert_true(test_verify_data(result, "cmd_failure.err_code", "7022"));
    assert_true(test_verify_data(result, "cmd_failure.err_msg", "Command failure"));

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&operate_var);
}

void test_send_resp_false_gives_no_response(UNUSED void** state) {
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    amxc_var_t operate_var;

    amxc_var_init(&operate_var);

    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    fill_syncfunc_var(&operate_var, true, false);

    assert_int_equal(uspl_tx_new(&usp_operate_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);
    assert_null(usp_tx);

    uspl_rx_delete(&usp_request);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&operate_var);
}

void test_sync_operate_not_allowed(UNUSED void** state) {
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    amxc_var_init(&operate_var);

    restrict_access("Device.Phonebook.Contact.*.SyncFunc()");

    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    fill_syncfunc_var(&operate_var, false, true);

    assert_int_equal(uspl_tx_new(&usp_operate_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", "Device.Phonebook.Contact.1.SyncFunc()"));
    assert_true(test_verify_data(result, "cmd_failure.err_code", "7006"));
    assert_true(test_verify_data(result, "cmd_failure.err_msg", uspl_error_code_to_str(7006)));

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&operate_var);
}

void test_sync_operate_not_supported(UNUSED void** state) {
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t* input_args = NULL;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    amxc_var_init(&operate_var);

    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);

    // Input path is not supported and not instantiated
    amxc_var_add_key(cstring_t, &operate_var, "command", "Device.Phonebook.Contact.9.NotSupported()");
    amxc_var_add_key(cstring_t, &operate_var, "command_key", "sync_func");
    amxc_var_add_key(bool, &operate_var, "send_resp", true);
    input_args = amxc_var_add_key(amxc_htable_t, &operate_var, "input_args", NULL);
    amxc_var_add_key(bool, input_args, "success", false);

    assert_int_equal(uspl_tx_new(&usp_operate_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", "Device.Phonebook.Contact.9.NotSupported()"));
    assert_true(test_verify_data(result, "cmd_failure.err_code", "7026"));
    assert_true(test_verify_data(result, "cmd_failure.err_msg", uspl_error_code_to_str(7026)));

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&operate_var);
}

void test_sync_operate_not_instantiated(UNUSED void** state) {
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t* input_args = NULL;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    amxc_var_init(&operate_var);

    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);

    // Input path is supported, but not instantiated
    amxc_var_add_key(cstring_t, &operate_var, "command", "Device.Phonebook.Contact.9.SyncFunc()");
    amxc_var_add_key(cstring_t, &operate_var, "command_key", "sync_func");
    amxc_var_add_key(bool, &operate_var, "send_resp", true);
    input_args = amxc_var_add_key(amxc_htable_t, &operate_var, "input_args", NULL);
    amxc_var_add_key(bool, input_args, "success", false);

    assert_int_equal(uspl_tx_new(&usp_operate_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", "Device.Phonebook.Contact.9.SyncFunc()"));
    assert_true(test_verify_data(result, "cmd_failure.err_code", "7016"));
    assert_true(test_verify_data(result, "cmd_failure.err_msg", uspl_error_code_to_str(7016)));

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&operate_var);
}

void test_sync_operate_protected_allowed(UNUSED void** state) {
    const char* endpoint_id = "proto::protected-controller";
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;

    amxc_llist_init(&resp_list);
    amxc_var_init(&operate_var);

    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &operate_var, "command", "Device.Phonebook.Contact.1.ProtectedMethodSync()");
    amxc_var_add_key(cstring_t, &operate_var, "command_key", "protected_sync");
    amxc_var_add_key(bool, &operate_var, "send_resp", true);

    assert_int_equal(uspl_tx_new(&usp_operate_msg, endpoint_id, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);
    assert_true(test_verify_data(result, "executed_command", "Device.Phonebook.Contact.1.ProtectedMethodSync()"));
    assert_non_null(GET_ARG(result, "output_args"));
    assert_null(GETP_ARG(result, "output_args._retval"));

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&operate_var);
}

void test_sync_operate_invalid_path(UNUSED void** state) {
    uspl_tx_t* usp_operate_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_request = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t operate_var;
    amxc_var_t* result = NULL;
    amxc_llist_t resp_list;
    imtp_frame_t* frame = NULL;

    amxc_llist_init(&resp_list);
    amxc_var_init(&operate_var);

    amxc_var_set_type(&operate_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &operate_var, "command", "Device.Phonebook_typo.Contact.1.GetRandomNumber()");
    amxc_var_add_key(cstring_t, &operate_var, "command_key", "getrandomnumber");
    amxc_var_add_key(bool, &operate_var, "send_resp", true);

    assert_int_equal(uspl_tx_new(&usp_operate_msg, controller_eid, agent_eid), 0);
    assert_int_equal(uspl_operate_new(usp_operate_msg, &operate_var), 0);
    usp_request = uspl_msghandler_unpack_protobuf(usp_operate_msg->pbuf, usp_operate_msg->pbuf_len);

    assert_int_equal(uspa_handle_operate(usp_request, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_operate_resp_extract(usp_response, &resp_list), 0);
    result = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    assert_non_null(result);
    amxc_var_dump(result, STDOUT_FILENO);

    assert_int_equal(GETP_UINT32(result, "cmd_failure.err_code"), 7026);
    assert_string_equal(GETP_CHAR(result, "cmd_failure.err_msg"), "Invalid path");

    handle_events();

    imtp_frame_delete(&frame);
    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_request);
    uspl_rx_delete(&usp_response);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_operate_msg);
    amxc_var_clean(&operate_var);
}
