/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <setjmp.h>
#include <cmocka.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <amxa/amxa.h>

#include "common.h"

static uint64_t deferred_call_id = 0;
static int sub_id = 0;
static int sfd = 0;
static const char* acl_dir = NULL;

static char* get_next_id(void) {
    amxc_string_t id;

    amxc_string_init(&id, 0);
    amxc_string_setf(&id, "sub_%d", sub_id++);

    return amxc_string_take_buffer(&id);
}

bool test_verify_data(const amxc_var_t* data, const char* field, const char* value) {
    bool rv = false;
    char* field_value = NULL;
    amxc_var_t* field_data = GETP_ARG(data, field);

    printf("Verify table data: check field [%s] contains [%s]\n", field, value);
    fflush(stdout);
    assert_non_null(field_data);

    field_value = amxc_var_dyncast(cstring_t, field_data);
    assert_non_null(field_data);

    rv = (strcmp(field_value, value) == 0);

    free(field_value);
    return rv;
}

void capture_sigalrm(void) {
    sigset_t mask;

    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);

    sfd = signalfd(-1, &mask, 0);
}

void read_sigalrm(uint32_t timeout) {
    struct signalfd_siginfo fdsi;
    ssize_t s;
    fd_set set;
    struct timeval ts;

    ts.tv_sec = timeout;
    ts.tv_usec = 0;

    FD_ZERO(&set);
    FD_SET(sfd, &set);
    int rv = select(sfd + 1, &set, NULL, NULL, &ts);
    if(rv > 0) {
        s = read(sfd, &fdsi, sizeof(struct signalfd_siginfo));
    } else {
        printf("Timeout\n");
        return;
    }

    assert_int_equal(s, sizeof(struct signalfd_siginfo));
    if(fdsi.ssi_signo == SIGALRM) {
        printf("Got SIGALRM\n");
        fflush(stdout);
    } else {
        printf("Read unexpected signal\n");
        fflush(stdout);
    }
}

void handle_events(void) {
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

void connection_read(UNUSED int fd, UNUSED void* priv) {
    // Do nothing
}

amxd_status_t _Client_CreateListenSocket(UNUSED amxd_object_t* object,
                                         UNUSED amxd_function_t* func,
                                         UNUSED amxc_var_t* args,
                                         UNUSED amxc_var_t* ret) {
    return amxd_status_ok;
}

amxd_status_t _Phonebook_DeferredCall(UNUSED amxd_object_t* object,
                                      amxd_function_t* func,
                                      UNUSED amxc_var_t* args,
                                      UNUSED amxc_var_t* ret) {
    printf("Initialize Phonebook DeferredCall\n");
    fflush(stdout);

    amxd_function_defer(func, &deferred_call_id, ret, NULL, NULL);

    return amxd_status_deferred;
}

amxd_status_t _Controller_GetMTPInfo(amxd_object_t* object,
                                     UNUSED amxd_function_t* func,
                                     UNUSED amxc_var_t* args,
                                     amxc_var_t* ret) {
    amxd_object_t* mtp = amxd_object_findf(object, "MTP");
    amxc_llist_it_t* it = amxd_object_first_instance(mtp);
    amxd_object_t* mtp_instance = amxc_container_of(it, amxd_object_t, it);
    char* protocol = amxd_object_get_value(cstring_t, mtp_instance, "Protocol", NULL);
    amxd_status_t status = amxd_status_unknown_error;

    when_str_empty(protocol, exit);

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, ret, "protocol", protocol);

    if(strcmp(protocol, "MQTT") == 0) {
        amxd_object_t* mqtt = amxd_object_findf(mtp_instance, "MQTT");
        char* topic = amxd_object_get_value(cstring_t, mqtt, "Topic", NULL);
        amxc_var_add_key(cstring_t, ret, "topic", topic);
        free(topic);
    }

    status = amxd_status_ok;
exit:
    free(protocol);
    return status;
}

void _Phonebook_DeferredCall_Done(void) {
    printf("Phonebook DeferredCall done\n");
    fflush(stdout);
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_var_set(bool, &ret, true);

    amxd_function_deferred_done(deferred_call_id, amxd_status_ok, NULL, &ret);
    amxc_var_clean(&ret);
}

amxd_status_t _UpdateStatus(amxd_object_t* object,
                            UNUSED amxd_function_t* func,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    const char* status = GET_CHAR(args, "status");
    amxd_dm_t* dm = amxd_object_get_dm(object);
    amxd_trans_t trans;
    amxd_status_t retval = amxd_status_unknown_error;

    amxd_trans_init(&trans);

    amxd_trans_select_object(&trans, object);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &trans, "Status", status);
    retval = amxd_trans_apply(&trans, dm);

    if(retval != amxd_status_ok) {
        amxc_var_set(bool, ret, false);
    } else {
        amxc_var_set(bool, ret, true);
    }

    amxd_trans_clean(&trans);
    return retval;
}

void add_subscription_instance(amxd_dm_t* dm,
                               const char* notif_type,
                               const char* ref_list,
                               const char* recipient) {
    amxd_object_t* sub_obj = amxd_dm_findf(dm, "LocalAgent.Subscription.");
    amxd_trans_t trans;
    char* id = get_next_id();
    amxc_string_t sub_path;

    amxc_string_init(&sub_path, 0);
    amxc_string_setf(&sub_path, "LocalAgent.Subscription.%s.", id);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_add_inst(&trans, 0, id);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "ID", id);
    amxd_trans_set_value(cstring_t, &trans, "NotifType", notif_type);
    amxd_trans_set_value(cstring_t, &trans, "Recipient", recipient);
    amxd_trans_set_value(cstring_t, &trans, "ReferenceList", ref_list);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    assert_non_null(amxd_object_findf(sub_obj, "[ID == '%s'].", id));

    amxc_string_clean(&sub_path);

    free(id);
}

amxd_status_t _assign_default_string(amxd_object_t* object,
                                     amxd_param_t* param,
                                     amxd_action_t reason,
                                     const amxc_var_t* const args,
                                     amxc_var_t* const retval,
                                     void* priv) {
    amxc_var_t* params = GET_ARG(args, "parameters");
    const char* param_name = amxc_var_constcast(cstring_t, (amxc_var_t*) priv);
    const char* param_value = GET_CHAR(params, param_name);
    // If an index is provided in the args or retval, use this one
    uint32_t index = GET_UINT32(args, "index") == 0 ? GET_UINT32(retval, "index") :
        GET_UINT32(args, "index");
    // If there is no index in the args or retval, take the next index
    uint32_t new_index = index == 0 ? (object->last_index + 1) : index;

    if((param_value == NULL) || (*param_value == 0)) {
        amxc_string_t default_value;
        amxc_string_init(&default_value, 0);
        amxc_string_setf(&default_value, "cpe-%s-%d", param_name, new_index);
        amxc_var_add_key(cstring_t, params, param_name, amxc_string_get(&default_value, 0));
        amxc_string_clean(&default_value);
    }

    return amxd_action_object_add_inst(object, param, reason, args, retval, priv);
}

void acl_initialize(amxo_parser_t* parser, amxc_string_t** acl_file, const char* role) {
    amxc_var_t* root_acls = NULL;
    amxc_string_t path;

    amxc_string_init(&path, 0);

    // Mimic the acl-manager functionality
    acl_dir = GET_CHAR(&parser->config, "acl_dir");
    amxc_string_setf(&path, "%s/merged", acl_dir);
    mkdir(amxc_string_get(&path, 0), 0700);

    amxc_string_setf(&path, "%s/%s/", acl_dir, role);
    root_acls = amxa_parse_files(amxc_string_get(&path, 0));
    assert_non_null(root_acls);

    amxc_string_setf(&path, "%s/merged/%s.json", acl_dir, role);
    assert_int_equal(amxa_merge_rules(root_acls, amxc_string_get(&path, 0)), 0);

    // Init acl file to use in each of the real test functions
    amxc_string_new(acl_file, 0);
    amxc_string_setf(*acl_file, "%s/merged/%s.json", acl_dir, role);

    amxc_string_clean(&path);
    amxc_var_delete(&root_acls);
}

void acl_clean(amxc_string_t** acl_file) {
    amxc_string_t path;

    amxc_string_init(&path, 0);

    remove(amxc_string_get(*acl_file, 0));

    amxc_string_setf(&path, "%s/merged", acl_dir);
    rmdir(amxc_string_get(&path, 0));

    amxc_string_delete(acl_file);
    amxc_string_clean(&path);
}
