/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
%define {

    /**
    * This object provides bulk data collection capabilities and global collection settings that affect the entire device.
    * Bulk Data utilizes various solutions (e.g., IPDR, HTTP) to collect data from devices and transfer the data to a collection server.
    * The IPDR solution is based on a service specification described in [TR-232].
    * The HTTP solution is based on transfer mechanisms described in [Annex A/TR-369].
    * The USPEventNotif solution is based on sending a Profile.{i}.Push! Event Notification via USP [TR-369].
    * The Bulk Data Collection Profiles are measured over a reporting interval (which can be aligned with absolute time) and are made available to the collection server.
    *
    * @version 1.0
    */
    %persistent object "${prefix_}BulkData" {

        /**
        * Enables or disables all collection profiles.
        * If false, bulk data will not be collected or reported.
        *
        * @version 1.0
        */
        %persistent bool Enable;

        /**
        * Indicates the status of the Bulk Data Collection mechanism. Enumeration of:
        * - Enabled (Bulk Data Collection is enabled and working as intended)
        * - Disabled (Bulk Data Collection is disabled)
        * - Error (Bulk Data Collection is enabled, but there is an error condition preventing the successful collection of bulk data, OPTIONAL)
        *
        * @version 1.0
        */
        %read-only string Status {
            default "Disabled";
            on action validate call check_enum ["Disabled", "Enabled", "Error"];
        }

        /**
        * Minimum reporting interval in seconds that the CPE is capable of supporting.
        * A value of 0 indicates no minimum reporting interval.
        *
        * @version 1.0
        */
        %read-only uint32 MinReportingInterval;

        /**
        * Comma-separated list of strings. Represents the IPDR and transport protocols that this device is capable of supporting. Each list item is an enumeration of:
        * - Streaming (IPDR Streaming Protocol [IPDR-SP])
        * - File (IPDR File Transfer Protocol [IPDR-FTP])
        * - HTTP (Hypertext Transfer Protocol [RFC2616])
        * - MQTT (Message Queuing Telemetry Transport [MQTT31], [MQTT311], and [MQTT50])
        * - USPEventNotif (User Services Platform (USP - [Annex A/TR-369]) Event Notification)
        *
        * @version 1.0
        */
        %read-only csv_string Protocols {
            default "MQTT,USPEventNotif";
            on action validate call check_enum ["Streaming", "File", "HTTP", "MQTT", "USPEventNotif"];
        }

        /**
        * Comma-separated list of strings. Represents the Encoding Types for the protocols that this device is capable of supporting. Each list item is an enumeration of:
        * - XML (Used with the IPDR Streaming and File Protocols. [IPDR-XML])
        * - XDR (Used with the IPDR Streaming and File Protocols. [IPDR-XDR])
        * - CSV (Comma Separated Values. Used with the HTTP and USPEventNotif Protocols. [RFC4180])
        * - JSON (JavaScript Object Notation. Used with the HTTP and USPEventNotif Protocols. [RFC7159])
        *
        * @version 1.0
        */
        %read-only csv_string EncodingTypes {
            default "JSON";
            on action validate call check_enum ["XML", "XDR", "CSV", "JSON"];
        }

        /**
        * When true, the Device supports the use of wildcards to determine the parameters that are reported using a Profile.
        *
        * @version 1.0
        */
        %read-only bool ParameterWildCardSupported = true;

        /**
        * The maximum number of profiles that can exist at any given time. Specifically, the maximum number of Profile.{i}. instances that the Controller can create.
        * If the value of this parameter is -1, then it means that the CPE doesn't have a limit to the number of profiles that can exist.
        *
        * @version 1.0
        */
        %read-only int32 MaxNumberOfProfiles {
            on action validate call check_minimum -1;
        }

        /**
        * The maximum number of parameters that can be referenced via the bulk data collection mechanism. Specifically, the maximum number of parameters that can be referenced via Profile.{i}.Parameter.{i}.Reference across all Profile and Parameter instances (including the expansion of partial paths within the Reference parameter).
        * If the value of this parameter is -1, then it means that the CPE doesn't have a limit to the number of parameter that can be referenced via the bulk data collection mechanism.
        *
        * @version 1.0
        */
        %read-only int32 MaxNumberOfParameterReferences {
            on action validate call check_minimum -1;
        }

        /**
        * A set of Bulk Data Collection profiles.
        * Each profile represents a bulk data report, including its own timing configuration, communications configuration, and set of parameters. This allows the Controller to configure multiple reports to be generated at different times for different sets of data.
        * At most one entry in this table can exist with a given value for Alias. On creation of a new table entry, the Agent MUST choose an initial value for Alias such that the new entry does not conflict with any existing entries. The non-functional key parameter Alias is immutable and therefore MUST NOT change once it's been assigned.
        *
        * @version 1.0
        */
        %persistent object Profile[] {
            on action add-inst call verify_max_instances "BulkData.MaxNumberOfProfiles";

            /**
            * The number of entries in the Profile table.
            *
            * @version 1.0
            */
            counted with ProfileNumberOfEntries;

            /**
            * Enables or disables this specific bulk data profile.
            * If false, this profile will not be collected or reported.
            *
            * @version 1.0
            */
            %persistent bool Enable = false;

            /**
            * [Alias] A non-volatile unique key used to reference this instance. Alias provides a mechanism for a Controller to label this instance for future reference.
            * The following mandatory constraints MUST be enforced:
            * - The value MUST NOT be empty.
            * - The value MUST start with a letter.
            * - If the value is not assigned by the Controller at creation time, the Agent MUST assign a value with an "cpe-" prefix.
            * The value MUST NOT change once it's been assigned.
            *
            * @version 1.0
            */
            %persistent %unique %key string Alias {
                on action validate call matches_regexp "[a-zA-Z].*";
                on action validate call check_maximum_length 64;
            }

            /**
            * The name of the profile.
            *
            * @version 1.0
            */
            %persistent string Name;

            /**
            * The value MUST be the Path Name of the Device.LocalAgent.Controller. instance that created or last updated this Profile. If the referenced object is deleted, the parameter value MUST be set to an empty string.
            * The value of this parameter is automatically populated by the USP Agent upon Profile creation using the reference to the USP Controller that created the instance.
            * The value of this parameter is automatically updated by the USP Agent upon Profile alteration using the reference to the USP Controller that changed the instance.
            *
            * @version 1.0
            */
            %read-only string Controller;

            /**
            * The number of failed reports to be retained and transmitted (in addition to the current report) at the end of the current reporting interval.
            * If the value of the EncodingType parameter is modified any outstanding failed reports are deleted.
            * If the CPE cannot retain the number of failed reports from previous reporting intervals while transmitting the report of the current reporting interval, then the oldest failed reports are 
            * deleted until the CPE is able to transmit the report from the current reporting interval.
            * 0 indicates that failed reports are not to be retained, -1 indicates that the CPE will retain as many failed reports as possible.
            *
            * @version 1.0
            */
            %persistent int32 NumberOfRetainedFailedReports {
                default 0;
                on action validate call check_minimum -1;
            }

            /**
            * The value MUST be a member of the list reported by the Protocols parameter. The Bulk Data Protocol being used for this collection profile.
            *
            * @version 1.0
            */
            %persistent string Protocol {
                default "MQTT";
                on action validate call check_is_in "${prefix_}BulkData.Protocols";
            }

            /**
            * The value MUST be a member of the list reported by the EncodingTypes parameter. The Bulk Data encoding type being used for this collection profile.
            *
            * @version 1.0
            */
            %persistent string EncodingType {
                default "JSON";
                on action validate call check_is_in "${prefix_}BulkData.EncodingTypes";
            }

            /**
            * The reporting interval in seconds. Each report is generated based on this interval and TimeReference.
            * The CPE MAY reject a request to set ReportingInterval to less than MinReportingInterval.
            * Reporting intervals MUST begin every ReportingInterval seconds.
            * If ReportingInterval is changed while collection is enabled, the first reporting interval begins immediately.
            * For example, if ReportingInterval is 86400 (a day) and if TimeReference is set to UTC midnight on some day (in the past, present, or future) then the CPE will generate (and transmit, if the Protocol parameter is set to Streaming) its report at midnight every 24 hours.
            *
            * @version 1.0
            */
            %persistent uint32 ReportingInterval {
                default 86400;
                on action validate call check_minimum 1;
                on action validate call check_minimum "${prefix_}BulkData.MinReportingInterval";
            }

            /**
            * An absolute time reference in UTC to determine when will be transmitted. Each reporting interval MUST complete at this reference time plus or minus an integer multiple of ReportingInterval, unless unable to due to higher prioritized operations.
            * TimeReference is used only to set the "phase" of the reporting intervals. The actual value of TimeReference can be arbitrarily far into the past or future.
            * If TimeReference is changed while collection of bulk data is enabled, the first reporting interval begins immediately.
            * The Unknown Time value as defined in [TR-106] indicates that no particular time reference is specified. That is, the CPE MAY locally choose the time reference, and is required only to adhere to the specified reporting intervals.
            * If absolute time is not available to the CPE, its reporting interval behavior MUST be the same as if the TimeReference parameter was set to the Unknown Time value.
            * For example, if ReportingInterval is 86400 (a day) and if TimeReference is set to UTC midnight on some day (in the past, present, or future) then the CPE will generate (and transmit, if in a "ITPush" mode) its report at midnight every 24 hours.
            * Note that, if TimeReference is set to a time other than the Unknown Time, the first reporting interval (which has to begin immediately) will almost certainly be shorter than ReportingInterval). This is why TimeReference is defined in terms of when reporting intervals complete rather than start.
            *
            * @version 1.0
            */
            %persistent datetime TimeReference {
                on action validate call matches_regexp "^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z$";
                default "0001-01-01T00:00:00Z";
            }

            /**
            * Bulk Data Push event for delivering a bulk data report within a USP Notification message.
            *
            * @version 1.0
            */
            event "Push!";

            /**
            * Bulk data parameter table.
            * Each entry in this table represents a parameter (or set of parameters if a partial path is provided) to be collected and reported.
            *
            * @version 1.0
            */
            %persistent object Parameter[] {
                on action add-inst call verify_max_instances "${prefix_}BulkData.MaxNumberOfParameterReferences";

                /**
                * The number of entries in the Parameter table.
                *
                * @version 1.0
                */
                counted with ParameterNumberOfEntries;

                /**
                * Name of the parameter in the report body.
                * If the value of this parameter is an empty string, then the value of the Reference parameter is used as the name.
                * When the value Reference parameter contains wildcards and/or partial parameter names, the rules for determining the value of this parameter are specified in [Annex A.2.1/TR-369].
                *
                * @version 1.0
                */
                %persistent string Name {
                    on action validate call check_maximum_length 64;
                }

                /**
                * The value MUST be the Path Name of a parameter or object. Represents the parameter(s) that are part of this Bulk Data collection profile. The value MUST be a path name of a parameter or an object.
                * When the ParameterWildCardSupported parameter has a value of true, patterns for instance identifiers are permitted with wildcards (an "*" character) in place of instance identifiers; any attempt to set the value otherwise MUST be rejected by the CPE.
                * In the case where a partial parameter path is specified, the sub-objects of the resolved pattern and contained parameters will be part of the bulk data collected and reported. If the path name refers to an object then it MUST end with a '.'.
                *
                * @version 1.0
                */
                %persistent string Reference {
                    on action validate call check_maximum_length 256;
                    on action validate call check_reference_is_valid "${prefix_}BulkData.ParameterWildCardSupported";
                }
            }

            /**
            * This object defines the properties to be used when the Profile object's EncodingType parameter value is CSV.
            *
            * @version 1.0
            */
            %persistent object CSVEncoding {

                /**
                * Field separator to use when encoding CSV data.
                *
                * @version 1.0
                */
                %persistent string FieldSeparator = ",";

                /**
                * Row separator to use when encoding CSV data.
                *
                * @version 1.0
                */
                %persistent string RowSeparator = "&#13;&#10;";

                /**
                * Escape character to use when encoding CSV data.
                *
                * @version 1.0
                */
                %persistent string EscapeCharacter = "&quot;";

                /**
                * This parameter describes the formatting used for reports defined by this profile as described in [Annex A.2.4/TR-369].
                * Note: This parameter is encoded as a token in the BBF-Report-Format header field and MUST NOT include spaces or other characters excluded from token characters defined in [RFC2616]. Enumeration of:
                * - ParameterPerRow (Reports are formatted with each parameter formatted as a row entry)
                * - ParameterPerColumn (Reports are formatted with each parameter formatted as a column entry)
                *
                * @version 1.0
                */
                %persistent string ReportFormat {
                    default "ParameterPerColumn";
                    on action validate call check_enum ["ParameterPerRow", "ParameterPerColumn"];
                }

                /**
                * The format of the timestamp to use for data inserted into the row. Enumeration of:
                * - Unix-Epoch (Timestamp is inserted using the UNIX epoch time (milliseconds since Jan 1, 1970 UTC) timestamp format. If the CPE is unable to acquire a time, then the time that has elapsed since the last reboot of the device is used)
                * - ISO-8601 (Timestamp is inserted using the ISO-8601 timestamp format)
                * - None (Timestamp is not inserted in the row)
                *
                * @version 1.0
                */
                %persistent string RowTimestamp {
                    default "Unix-Epoch";
                    on action validate call check_enum ["Unix-Epoch", "ISO-8601", "None"];
                }
            }

            /**
            * This object defines the properties to be used when the Profile object's EncodingType parameter value is JSON.
            *
            * @version 1.0
            */
            %persistent object JSONEncoding {

                /**
                * This parameter describes the formatting used for the report as described in [Annex A.2.5/TR-369].
                * Note: This parameter is encoded as a token in the BBF-Report-Format header field and MUST NOT include spaces or other characters excluded from token characters defined in [RFC2616]. Enumeration of:
                * - ObjectHierarchy (Reports are formatted with each object in the object hierarchy of the data model encoded as a corresponding hierarchy of JSON Objects with the parameters of the object specified as name/value pairs of the JSON Object)
                * - NameValuePair (Reports are formatted with each parameter of the data model encoded as a corresponding array of JSON Objects with the parameters specified as name/value pairs)
                *
                * @version 1.0
                */
                %persistent string ReportFormat {
                    default "ObjectHierarchy";
                    on action validate call check_enum ["ObjectHierarchy", "NameValuePair"];
                }

                /**
                * The format of timestamp to use for the JSON Object named "CollectionTime" as described in [Annex A.2.5/TR-369]. Enumeration of:
                * - Unix-Epoch (Timestamp is inserted using the UNIX epoch time (milliseconds since Jan 1, 1970 UTC) timestamp format. If the CPE is unable to acquire a time, then the time that has elapsed since the last reboot of the device is used)
                * - ISO-8601 (Timestamp is inserted using the ISO-8601 timestamp format)
                * - None (Timestamp is not inserted)
                *
                * @version 1.0
                */
                %persistent string ReportTimestamp {
                    default "Unix-Epoch";
                    on action validate call check_enum ["Unix-Epoch", "ISO-8601", "None"];
                }
            }

            /**
            * This object defines the properties to be used when transporting bulk data using the HTTP/HTTPS protocol. This object is used when the Protocol parameter has a value of HTTP. For authentication purposes the CPE MUST support HTTP Basic and Digest Access Authentication as defined in [RFC2616].
            *
            * @version 1.0
            */
            %persistent object HTTP {

                /**
                * The [URL] for the collection server to receive the Bulk Data transmitted by the CPE.
                *
                * @version 1.0
                */
                %persistent string URL {
                    on action validate call check_maximum_length 2048;
                }

                /**
                * Username used to authenticate the CPE when making a connection to the collection server.
                *
                * @version 1.0
                */
                %persistent string Username {
                    on action validate call check_maximum_length 256;
                }

                /**
                * Password used to authenticate the CPE when making a connection to the collection server.
                * When read, this parameter returns an empty string, regardless of the actual value.
                *
                * @version 1.0
                */
                %persistent string Password {
                    on action validate call check_maximum_length 256;
                    on action read call hide_value;
                }

                /**
                * Comma-separated list of strings. Indicates the HTTP Compression mechanism(s) supported by this CPE for the purposes of transferring bulk data. Each list item is an enumeration of:
                * - GZIP (As defined in [Section 3.5/RFC2616])
                * - Compress (As defined in [Section 3.5/RFC2616])
                * - Deflate (As defined in [Section 3.5/RFC2616])
                *
                * @version 1.0
                */
                %read-only csv_string CompressionsSupported {
                    default "GZIP,Compress,Deflate";
                    on action validate call check_enum ["GZIP", "Compress", "Deflate"];
                }

                /**
                * The value MUST be a member of the list reported by the CompressionsSupported parameter, or else be None. The value of this parameter represents the HTTP Compression mechanism to be used by the CPE when transferring data to the collection server.
                *
                * @version 1.0
                */
                %persistent string Compression {
                    default "None";
                    on action validate call check_is_none_or_in ".CompressionsSupported";
                }

                /**
                * Comma-separated list of strings. Indicates the HTTP method(s) supported by this CPE for the purposes of transferring bulk data. Each list item is an enumeration of:
                * - POST (As defined in [Section 9.5/RFC2616])
                * - PUT (As defined in [Section 9.6/RFC2616])
                *
                * @version 1.0
                */
                %read-only csv_string MethodsSupported {
                    default "POST,PUT";
                    on action validate call check_enum ["POST", "PUT"];
                }

                /**
                * The value MUST be a member of the list reported by the MethodsSupported parameter. The value of this parameter represents the HTTP method to be used by the CPE when transferring data to the collection server.
                *
                * @version 1.0
                */
                %persistent string Method {
                    on action validate call check_is_in ".MethodsSupported";
                    default "POST";
                }

                /**
                * When true, the CPE encodes the HTTP Date Header [Section 14.18/RFC2616] in the HTTP client request.
                *
                * @version 1.0
                */
                %persistent bool UseDateHeader = true;

                /**
                * When true, the CPE retries unsuccessful attempts to transfer data.
                *
                * @version 1.0
                */
                %persistent bool RetryEnable;

                /**
                * Configures the data transfer retry wait interval, in seconds, as specified in [Annex A.1.2.1/TR-369].
                * The device MUST use a random value between RetryMinimumWaitInterval and (RetryMinimumWaitInterval * RetryIntervalMultiplier / 1000) as the first retry wait interval. Other values in the retry pattern MUST be calculated using this value as a starting point.
                *
                * @version 1.0
                */
                %persistent uint16 RetryMinimumWaitInterval {
                    default 5;
                    on action validate call check_minimum 1;
                }

                /**
                * Configures the retry interval multiplier as specified in [Annex A.1.2.1/TR-369].
                * This value is expressed in units of 0.001. Hence the values of the multiplier range between 1.000 and 65.535.
                * The device MUST use a random value between RetryMinimumWaitInterval and (RetryMinimumWaitInterval * RetryIntervalMultiplier / 1000) as the first retry wait interval. Other values in the retry pattern MUST be calculated using this value as a starting point.
                *
                * @version 1.0
                */
                %persistent uint16 RetryIntervalMultiplier {
                    default 2000;
                    on action validate call check_minimum 1000;
                }

                /**
                * Determines whether or not data transfers that have failed are required to be persisted across reboots.
                * If PersistAcrossReboot is true, then failed data transfers MUST be persisted across reboots.
                * If PersistAcrossReboot is false, then failed data transfers are not required to be persisted across reboots.
                *
                * @version 1.0
                */
                %persistent bool PersistAcrossReboot;

                /**
                * This object represents an instance of a parameter to be used in the report header used as part of the HTTP Request-URI transmitted by the CPE to the collection server using the Request-URI in addition to the parameters required by [Annex A.1.1/TR-369].
                *
                * @version 1.0
                */
                %persistent object RequestURIParameter[] {

                    /**
                    * The number of entries in the RequestURIParameter table.
                    *
                    * @version 1.0
                    */
                    counted with RequestURIParameterNumberOfEntries;

                    /**
                    * Name of the Request-URI parameter.
                    * If Name is an empty string, the name of the Request-URI parameter is the value of Reference.
                    *
                    * @version 1.0
                    */
                    %persistent string Name {
                        on action validate call check_maximum_length 64;
                    }

                    /**
                    * The value MUST be the path name of a parameter to be used as the Request-URI parameter.
                    * If the value of this parameter is empty, then this object is not encoded in the report header.
                    *
                    * @version 1.0
                    */
                    %persistent string Reference {
                        on action validate call check_maximum_length 256;
                    }
                }
            }

            /**
            * This object defines the properties to be used when transporting bulk data using
            * the MQTT protocol. This object is used when the Protocol parameter has a value of MQTT.
            *
            * @version 1.0
            */
            %persistent object MQTT {

                /**
                * The value MUST be the Path Name of a row in the MQTT.Client table. A reference
                * to the MQTT Client used by this Bulk Data Collection Profile when communicating via the MQTT Protocol.
                *
                * @version 1.0
                */
                %persistent string Reference;

                /**
                * The topic name the Agent MUST use when sending the Bulk Data report.
                *
                * @version 1.0
                */
                %persistent string PublishTopic {
                    on action validate call check_maximum_length 65535;
                }

                /**
                * The Agent MUST use this QoS value when sending the Bulk Data report.
                *
                * If the referenced MQTT Client uses MQTT 5.0 and the MQTT server only indicates
                * support for a QoS value in the CONNACK Maximum QoS property lower than this
                * QoS value, the Agent MUST use the highest QoS value that is supported by the server.
                *
                * @version 1.0
                */
                %persistent uint32 PublishQoS {
                    on action validate call check_range [0, 2];
                }

                /**
                * If set to true the Agent MUST set the RETAIN flag in MQTT PUBLISH messages
                * carrying the Bulk Data report to 1, unless the MQTT server sent Retain Available = 0 (MQTT 5.0)
                * in its CONNACK (in which case, the Agent MUST set the RETAIN flag to 0).
                *
                * @version 1.0
                */
                %persistent bool PublishRetain;
            }
            /**
            * Start the bulk data collection and transmission as defined in this profile immediately,
            * regardless of the current values of the Enable and ReportingInterval parameters.
            * This command can be used for testing of the bulk data collection mechanism and profile,
            * but also to preempt any regular schedule without affecting it.
            *
            */
            void ForceCollection();
        }
    }
}
