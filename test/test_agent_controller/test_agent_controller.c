/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <poll.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include <usp/uspl.h>
#include <imtp/imtp_message.h>

#include "uspa.h"
#include "uspa_imtp.h"
#include "uspa_controller.h"
#include "test_agent_controller.h"
#include "dummy_be.h"
#include "mock.h"
#include "common.h"

#define UNUSED __attribute__((unused))

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;

static amxo_parser_t parser;
static const char* odl_config = "../common/test_config.odl";
static const char* la_definition = "../common/tr181-localagent_definition.odl";
static const char* la_defaults = "tr181-localagent_defaults.odl";
static const char* mqtt_definition = "../common/tr181-mqtt_definition.odl";
static const char* mqtt_defaults = "../common/tr181-mqtt_defaults.odl";

static amxc_string_t* acl_file = NULL;

static int testpipe_1[2] = {};
static int testpipe_2[2] = {};

static void uspa_initialize(void) {
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    uspi_con_t* con = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);

    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_defaults, root_obj), 0);

    will_return(__wrap_uspi_con_get_fd, testpipe_1[0]); // uspa_imtp_connect
    will_return(__wrap_uspi_con_get_fd, testpipe_2[0]); // uspa_imtp_connect
    assert_int_equal(_uspa_main(0, &dm, &parser), 0);

    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "MQTT.Client.1.", 0, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    assert_true(GETP_BOOL(&ret, "0.0.Enable"));

    con = uspa_imtp_con_from_la_mtp("LocalAgent.MTP.1.", USPA_MTP_ID_MQTT);
    assert_non_null(con);

    amxc_var_clean(&ret);
}

int test_uspa_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    const char* role = "root";

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "GetMTPInfo", AMXO_FUNC(_Controller_GetMTPInfo));
    amxo_resolver_ftab_add(&parser, "DeferredCall", AMXO_FUNC(_Phonebook_DeferredCall));
    amxo_resolver_ftab_add(&parser, "CreateListenSocket", AMXO_FUNC(_Client_CreateListenSocket));

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxb_be_load("/usr/bin/mods/usp/mod-amxb-usp.so"), 0);

    handle_events();

    assert_int_equal(pipe(testpipe_1), 0);
    assert_int_equal(pipe(testpipe_2), 0);
    capture_sigalrm();

    uspa_initialize();
    acl_initialize(&parser, &acl_file, role);
    handle_events();

    return 0;
}

int test_uspa_teardown(UNUSED void** state) {
    amxp_signal_t* sig = NULL;

    will_return(__wrap_uspi_con_get_fd, testpipe_1[0]);   // uspa_imtp_disconnect
    will_return(__wrap_uspi_con_disconnect, 1);           // uspa_imtp_disconnect
    will_return(__wrap_uspi_con_get_fd, testpipe_2[0]);   // uspa_imtp_disconnect
    will_return(__wrap_uspi_con_disconnect, 1);           // uspa_imtp_disconnect
    assert_int_equal(_uspa_main(1, &dm, &parser), 0);
    handle_events();

    close(testpipe_1[0]);
    close(testpipe_1[1]);
    close(testpipe_2[0]);
    close(testpipe_2[1]);

    acl_clean(&acl_file);

    assert_int_equal(amxb_be_remove("usp"), 0);
    amxb_free(&bus_ctx);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_adding_controller_adds_inst(UNUSED void** state) {
    amxd_trans_t trans;
    uspa_contr_instance_t* inst = NULL;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LocalAgent.Controller.");
    amxd_trans_add_inst(&trans, 1, "controller-test");
    amxd_trans_set_value(cstring_t, &trans, "EndpointID", "proto::controller");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    inst = uspa_contr_instance_get("LocalAgent.Controller.1.");
    assert_non_null(inst);
    assert_false(inst->enabled);
}

void test_update_mtp_reference_links_con(UNUSED void** state) {
    amxd_trans_t trans;
    uspa_contr_instance_t* inst = NULL;
    const char* contr_inst = "LocalAgent.Controller.1.";
    const char* contr_inst_mqtt = "LocalAgent.Controller.1.MTP.1.MQTT.";
    uspi_con_t* con_1 = NULL;
    uspi_con_t* con_2 = NULL;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, contr_inst);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    con_1 = uspa_controller_con_get(contr_inst);
    assert_non_null(con_1);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, contr_inst_mqtt);
    amxd_trans_set_cstring_t(&trans, "AgentMTPReference", "LocalAgent.MTP.2");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    con_2 = uspa_controller_con_get(contr_inst);
    assert_non_null(con_2);
    //assert_ptr_not_equal(con_1, con_2);
}

// Need to add enabled and configured MTP before enabling controller
void test_enabling_controller_links_con_and_sets_enable(UNUSED void** state) {
    amxd_trans_t trans;
    uspa_contr_instance_t* inst = NULL;
    const char* contr_inst = "LocalAgent.Controller.1.";
    uspi_con_t* con = NULL;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LocalAgent.Controller.controller-test.MTP.");
    amxd_trans_add_inst(&trans, 1, "mtp-test");
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "MQTT");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LocalAgent.Controller.controller-test.MTP.mtp-test.MQTT.");
    amxd_trans_set_value(cstring_t, &trans, "AgentMTPReference", "LocalAgent.MTP.1");
    amxd_trans_set_value(cstring_t, &trans, "Topic", "controller_test");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LocalAgent.Controller.controller-test.MTP.mtp-test.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    inst = uspa_contr_instance_get(contr_inst);
    assert_non_null(inst);
    // Controller is still disabled
    assert_false(inst->enabled);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, contr_inst);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    // Controller is enabled now
    assert_true(inst->enabled);

    // Find con, because there is an enabled MTP
    con = uspa_controller_con_get(contr_inst);
    assert_non_null(con);
}

void test_disabling_controller_unlinks_con_and_sets_disable(UNUSED void** state) {
    amxd_trans_t trans;
    uspa_contr_instance_t* inst = NULL;
    const char* contr_inst = "LocalAgent.Controller.1.";
    uspi_con_t* con = NULL;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, contr_inst);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    inst = uspa_contr_instance_get(contr_inst);
    assert_non_null(inst);
    assert_false(inst->enabled);
    con = uspa_controller_con_get(contr_inst);
    assert_null(con);
}