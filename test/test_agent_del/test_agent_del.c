/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <sys/stat.h>
#include <sys/types.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include <usp/uspl.h>

#include "uspa.h"
#include "uspa_msghandler.h"
#include "test_agent_del.h"
#include "dummy_be.h"
#include "common.h"

#define UNUSED __attribute__((unused))

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;

static amxo_parser_t parser;
static const char* odl_config = "../common/test_config.odl";
static const char* la_definition = "../common/tr181-localagent_definition.odl";
static const char* la_defaults = "../common/tr181-localagent_mtp_disabled.odl";
static const char* mqtt_definition = "../common/tr181-mqtt_definition.odl";
static const char* mqtt_defaults = "../common/tr181-mqtt_defaults.odl";
static const char* phonebook_odl = "phonebook.odl";

static amxc_string_t* acl_file = NULL;

static void build_delete_var(amxc_var_t* del_var, const char* path) {
    amxc_var_t* requests = NULL;
    amxc_var_set_type(del_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, del_var, "allow_partial", true);
    requests = amxc_var_add_key(amxc_llist_t, del_var, "requests", NULL);
    amxc_var_add(cstring_t, requests, path);
}

static void uspa_initialize(void) {
    amxd_object_t* root_obj = amxd_dm_get_root(&dm);
    amxc_var_t ret;

    amxc_var_init(&ret);

    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, la_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, phonebook_odl, root_obj), 0);
    assert_int_equal(_uspa_main(0, &dm, &parser), 0);

    amxc_var_clean(&ret);
}

int test_uspa_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    const char* role = "root";

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "CreateListenSocket", AMXO_FUNC(_Client_CreateListenSocket));

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);

    // Register data model
    amxb_register(bus_ctx, &dm);

    // Load USP back end
    assert_int_equal(amxb_be_load("/usr/bin/mods/usp/mod-amxb-usp.so"), 0);

    handle_events();

    uspa_initialize();
    acl_initialize(&parser, &acl_file, role);

    return 0;
}

int test_uspa_teardown(UNUSED void** state) {
    assert_int_equal(_uspa_main(1, &dm, &parser), 0);
    handle_events();
    acl_clean(&acl_file);
    assert_int_equal(amxb_be_remove("usp"), 0);
    amxb_free(&bus_ctx);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_can_delete_phonebook_contact(UNUSED void** state) {
    uspl_tx_t* usp_del_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    const char* delete_path = "Device.Phonebook.Contact.2.";
    amxc_var_t* get_response = NULL;
    amxc_var_t phonebook;
    amxc_var_t del_var;

    amxc_var_init(&phonebook);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    amxb_get(bus_ctx, "Device.Phonebook.Contact.", 0, &phonebook, 5);
    amxb_set_access(bus_ctx, AMXB_PROTECTED);
    amxc_var_dump(&phonebook, STDOUT_FILENO);
    get_response = GETI_ARG(&phonebook, 0);
    assert_non_null(get_response);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, get_response)), 2);

    // del_var will contain data to del from dm
    amxc_var_init(&del_var);
    build_delete_var(&del_var, delete_path);

    // usp_del_msg holds protobuf
    assert_int_equal(uspl_tx_new(&usp_del_msg, "one", "two"), 0);
    assert_int_equal(uspl_delete_new(usp_del_msg, &del_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_del_msg->pbuf, usp_del_msg->pbuf_len); // unpack to have rx struct

    assert_int_equal(uspa_handle_delete(usp_rx, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    // check content of phonebook again
    amxc_var_clean(&phonebook);
    amxc_var_init(&phonebook);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    amxb_get(bus_ctx, "Device.Phonebook.Contact.", 0, &phonebook, 5);
    amxb_set_access(bus_ctx, AMXB_PROTECTED);
    amxc_var_dump(&phonebook, STDOUT_FILENO);
    get_response = GETI_ARG(&phonebook, 0);
    assert_non_null(get_response);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, get_response)), 1);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_del_msg);
    amxc_var_clean(&phonebook);
    amxc_var_clean(&del_var);
}

void test_passing_null_arg(UNUSED void** state) {
    uspl_tx_t* usp_del_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    const char* delete_path = "Device.Phonebook.Contact.2.";
    amxc_var_t del_var;

    // del_var will contain data to del from dm
    amxc_var_init(&del_var);
    build_delete_var(&del_var, delete_path);

    // usp_del_msg holds protobuf
    assert_int_equal(uspl_tx_new(&usp_del_msg, "one", "two"), 0);
    assert_int_equal(uspl_delete_new(usp_del_msg, &del_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_del_msg->pbuf, usp_del_msg->pbuf_len);

    assert_int_equal(uspa_handle_delete(NULL, &usp_tx, NULL), -1);
    assert_int_equal(uspa_handle_delete(usp_rx, NULL, NULL), -1);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_del_msg);
    amxc_var_clean(&del_var);
}

void test_del_req_with_non_existing_data(UNUSED void** state) {
    uspl_tx_t* usp_del_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t phonebook;
    amxc_var_t del_var;
    amxc_var_t* get_response = NULL;

    amxc_var_init(&phonebook);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    amxb_get(bus_ctx, "Device.Phonebook.Contact.", 0, &phonebook, 5);
    amxb_set_access(bus_ctx, AMXB_PROTECTED);
    amxc_var_dump(&phonebook, STDOUT_FILENO);
    get_response = GETI_ARG(&phonebook, 0);
    assert_non_null(get_response);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, get_response)), 1);

    // del_var will contain data to del from dm
    amxc_var_init(&del_var);
    build_delete_var(&del_var, "Device.Phonebook.Contakt.1.");

    // usp_del_msg holds protobuf
    assert_int_equal(uspl_tx_new(&usp_del_msg, "one", "two"), 0);
    assert_int_equal(uspl_delete_new(usp_del_msg, &del_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_del_msg->pbuf, usp_del_msg->pbuf_len); // unpack to have rx struct

    assert_int_equal(uspa_handle_delete(usp_rx, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    // check content of phonebook again
    amxc_var_clean(&phonebook);
    amxc_var_init(&phonebook);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    amxb_get(bus_ctx, "Device.Phonebook.Contact.", 0, &phonebook, 5);
    amxb_set_access(bus_ctx, AMXB_PROTECTED);
    amxc_var_dump(&phonebook, STDOUT_FILENO);
    get_response = GETI_ARG(&phonebook, 0);
    assert_non_null(get_response);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, get_response)), 1);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_del_msg);
    amxc_var_clean(&phonebook);
    amxc_var_clean(&del_var);
}

void test_del_req_with_non_existing_base_object(UNUSED void** state) {
    uspl_tx_t* usp_del_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    uspl_rx_t* usp_response = NULL;
    amxc_var_t del_var;
    amxc_llist_t response_list;

    // del_var will contain data from an object that does not exist in the dm
    amxc_var_init(&del_var);
    amxc_llist_init(&response_list);
    build_delete_var(&del_var, "Phonebooks.Contact.1.");

    // usp_del_msg holds protobuf
    assert_int_equal(uspl_tx_new(&usp_del_msg, "one", "two"), 0);
    assert_int_equal(uspl_delete_new(usp_del_msg, &del_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_del_msg->pbuf, usp_del_msg->pbuf_len); // unpack to have rx struct

    assert_int_equal(uspa_handle_delete(usp_rx, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_delete_resp_extract(usp_response, &response_list), 0);
    amxc_llist_for_each(it, &response_list) {
        amxc_var_t* response_record = amxc_llist_it_get_data(it, amxc_var_t, lit);
        amxc_var_dump(response_record, STDOUT_FILENO);
        assert_int_equal(GETP_UINT32(response_record, "result.err_code"), USP_ERR_OBJECT_DOES_NOT_EXIST);
        assert_string_equal(GETP_CHAR(response_record, "result.err_msg"), uspl_error_code_to_str(USP_ERR_OBJECT_DOES_NOT_EXIST));
    }


    uspl_rx_delete(&usp_response);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_del_msg);
    amxc_var_clean(&del_var);
    amxc_llist_clean(&response_list, variant_list_it_free);
}

void test_del_req_with_readonly_data(UNUSED void** state) {
    uspl_tx_t* usp_del_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t phonebook;
    amxc_var_t del_var;
    amxc_var_t* get_response = NULL;

    amxc_var_init(&phonebook);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    amxb_get(bus_ctx, "Device.Phonebook.PrivContact.", 0, &phonebook, 5);
    amxb_set_access(bus_ctx, AMXB_PROTECTED);
    amxc_var_dump(&phonebook, STDOUT_FILENO);
    get_response = GETI_ARG(&phonebook, 0);
    assert_non_null(get_response);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, get_response)), 1);

    // del_var will contain data to del from dm
    amxc_var_init(&del_var);
    build_delete_var(&del_var, "Device.Phonebook.PrivContact.1.");

    // usp_del_msg holds protobuf
    assert_int_equal(uspl_tx_new(&usp_del_msg, "one", "two"), 0);
    assert_int_equal(uspl_delete_new(usp_del_msg, &del_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_del_msg->pbuf, usp_del_msg->pbuf_len); // unpack to have rx struct

    assert_int_equal(uspa_handle_delete(usp_rx, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    // check content of phonebook again
    amxc_var_clean(&phonebook);
    amxc_var_init(&phonebook);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    amxb_get(bus_ctx, "Device.Phonebook.PrivContact.", 0, &phonebook, 5);
    amxb_set_access(bus_ctx, AMXB_PROTECTED);
    amxc_var_dump(&phonebook, STDOUT_FILENO);
    get_response = GETI_ARG(&phonebook, 0);
    assert_non_null(get_response);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, get_response)), 1);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_del_msg);
    amxc_var_clean(&phonebook);
    amxc_var_clean(&del_var);
}

void test_del_req_with_wildcard_data(UNUSED void** state) {
    uspl_tx_t* usp_del_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t phonebook;
    amxc_var_t del_var;
    amxc_var_t* get_response = NULL;

    amxc_var_init(&phonebook);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    amxb_get(bus_ctx, "Device.Phonebook.Page.", 0, &phonebook, 5);
    amxb_set_access(bus_ctx, AMXB_PROTECTED);
    amxc_var_dump(&phonebook, STDOUT_FILENO);
    get_response = GETI_ARG(&phonebook, 0);
    assert_non_null(get_response);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, get_response)), 4);

    // del_var will contain data to del from dm
    amxc_var_init(&del_var);
    build_delete_var(&del_var, "Device.Phonebook.Page.*.");

    // usp_del_msg holds protobuf
    assert_int_equal(uspl_tx_new(&usp_del_msg, "one", "two"), 0);
    assert_int_equal(uspl_delete_new(usp_del_msg, &del_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_del_msg->pbuf, usp_del_msg->pbuf_len); // unpack to have rx struct

    assert_int_equal(uspa_handle_delete(usp_rx, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    // check content of phonebook again
    amxc_var_clean(&phonebook);
    amxc_var_init(&phonebook);
    assert_int_not_equal(amxb_get(bus_ctx, "Device.Phonebook.Contact.Page.", 0, &phonebook, 5), 0);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_del_msg);
    amxc_var_clean(&phonebook);
    amxc_var_clean(&del_var);
}

void test_del_la_mtp(UNUSED void** state) {
    uspl_tx_t* usp_del_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t ret;
    amxc_var_t del_var;
    amxc_var_t* get_response = NULL;
    const char* requested_path = "LocalAgent.MTP.1.";

    amxc_var_init(&ret);

    amxb_get(bus_ctx, "LocalAgent.MTP.", 0, &ret, 1);
    amxc_var_dump(&ret, STDOUT_FILENO);
    get_response = GETI_ARG(&ret, 0);
    assert_non_null(get_response);
    assert_false(amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, get_response)));

    amxc_var_init(&del_var);
    build_delete_var(&del_var, requested_path);

    assert_int_equal(uspl_tx_new(&usp_del_msg, "one", "two"), 0);
    assert_int_equal(uspl_delete_new(usp_del_msg, &del_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_del_msg->pbuf, usp_del_msg->pbuf_len);
    assert_int_equal(uspa_handle_delete(usp_rx, &usp_tx, amxc_string_get(acl_file, 0)), 0);
    handle_events();

    amxc_var_clean(&ret);
    amxc_var_init(&ret);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    assert_int_not_equal(amxb_get(bus_ctx, requested_path, 0, &ret, 1), 0);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_del_msg);
    amxc_var_clean(&del_var);
    amxc_var_clean(&ret);
}

void test_cannot_delete_phonenumber(UNUSED void** state) {
    uspl_tx_t* usp_del_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    amxc_var_t ret;
    amxc_var_t del_var;
    amxc_var_t* get_response = NULL;
    const char* requested_path = "Device.Phonebook.Contact.1.PhoneNumber.1.";

    amxc_var_init(&del_var);
    build_delete_var(&del_var, requested_path);

    assert_int_equal(uspl_tx_new(&usp_del_msg, "one", "two"), 0);
    assert_int_equal(uspl_delete_new(usp_del_msg, &del_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_del_msg->pbuf, usp_del_msg->pbuf_len); // unpack to have rx struct

    assert_int_equal(uspa_handle_delete(usp_rx, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    amxc_var_init(&ret);
    amxb_get(bus_ctx, requested_path, 0, &ret, 5);
    amxc_var_dump(&ret, STDOUT_FILENO);
    get_response = GETI_ARG(&ret, 0);
    assert_non_null(get_response);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, get_response)), 1);

    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_del_msg);
    amxc_var_clean(&ret);
    amxc_var_clean(&del_var);
}

void test_del_with_allow_partial_false_rejected(UNUSED void** state) {
    uspl_tx_t* usp_del_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    uspl_rx_t* usp_response = NULL;
    const char* delete_path = "Device.Phonebook.Contact.2.";
    amxc_var_t* requests = NULL;
    amxc_var_t del_var;
    amxc_var_t error;

    amxc_var_init(&del_var);
    amxc_var_init(&error);

    amxc_var_set_type(&del_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &del_var, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, &del_var, "requests", NULL);
    amxc_var_add(cstring_t, requests, delete_path);

    // usp_del_msg holds protobuf
    assert_int_equal(uspl_tx_new(&usp_del_msg, "one", "two"), 0);
    assert_int_equal(uspl_delete_new(usp_del_msg, &del_var), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_del_msg->pbuf, usp_del_msg->pbuf_len); // unpack to have rx struct

    assert_int_equal(uspa_handle_delete(usp_rx, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);

    assert_int_equal(uspl_error_resp_extract(usp_response, &error), 0);
    amxc_var_dump(&error, STDOUT_FILENO);
    assert_int_equal(GET_UINT32(&error, "err_code"), USP_ERR_RESOURCES_EXCEEDED);
    assert_string_equal(GET_CHAR(&error, "err_msg"), uspl_error_code_to_str(USP_ERR_RESOURCES_EXCEEDED));

    uspl_rx_delete(&usp_response);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_del_msg);
    amxc_var_clean(&del_var);
    amxc_var_clean(&error);
}

void test_del_with_allow_partial_false_ignored(UNUSED void** state) {
    uspl_tx_t* usp_del_msg = NULL;
    uspl_tx_t* usp_tx = NULL;
    uspl_rx_t* usp_rx = NULL;
    uspl_rx_t* usp_response = NULL;
    const char* delete_path = "Device.Phonebook.Contact.1.";
    amxc_var_t* requests = NULL;
    amxc_var_t* del_resp = NULL;
    amxc_var_t del_req;
    amxc_llist_t resp_list;

    amxc_var_init(&del_req);
    amxc_llist_init(&resp_list);

    amxc_var_add_key(bool, &parser.config, "ignore-allow-partial", true);

    amxc_var_set_type(&del_req, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &del_req, "allow_partial", false);
    requests = amxc_var_add_key(amxc_llist_t, &del_req, "requests", NULL);
    amxc_var_add(cstring_t, requests, delete_path);

    // usp_del_msg holds protobuf
    assert_int_equal(uspl_tx_new(&usp_del_msg, "one", "two"), 0);
    assert_int_equal(uspl_delete_new(usp_del_msg, &del_req), 0);
    usp_rx = uspl_msghandler_unpack_protobuf(usp_del_msg->pbuf, usp_del_msg->pbuf_len);

    assert_int_equal(uspa_handle_delete(usp_rx, &usp_tx, amxc_string_get(acl_file, 0)), 0);

    usp_response = uspl_msghandler_unpack_protobuf(usp_tx->pbuf, usp_tx->pbuf_len);
    assert_non_null(usp_response);
    assert_int_equal(uspl_msghandler_msg_type(usp_response), USP__HEADER__MSG_TYPE__DELETE_RESP);

    assert_int_equal(uspl_delete_resp_extract(usp_response, &resp_list), 0);
    del_resp = amxc_var_from_llist_it(amxc_llist_get_first(&resp_list));
    amxc_var_dump(del_resp, STDOUT_FILENO);
    assert_int_equal(GETP_UINT32(del_resp, "result.err_code"), 0);
    assert_string_equal(GETP_CHAR(del_resp, "result.requested_path"), delete_path);

    amxc_llist_clean(&resp_list, variant_list_it_free);
    uspl_rx_delete(&usp_response);
    uspl_rx_delete(&usp_rx);
    uspl_tx_delete(&usp_tx);
    uspl_tx_delete(&usp_del_msg);
    amxc_var_clean(&del_req);
}
