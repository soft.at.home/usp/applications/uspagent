/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPA_CONTROLLER_MTP_H__)
#define __USPA_CONTROLLER_MTP_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "uspa.h"

/**
   @brief
   Get the parent controller based on a LocalAgent.Controller.{i}.MTP.{i}.
   instance path.

   @param contr_mtp controller MTP path
   @return controller path in case of success or NULL in case of error
 */
char* uspa_controller_mtp_get_contr(const char* contr_mtp);

/**
   @brief
   Subscribe to events for LocalAgent.Controller.{i}.MTP.{i}. and act accordingly

   If an MTP is enabled or an enabled MTP is added, check the enable parameter of the parent
   LocalAgent.Controller.{i}. object. If it is enabled, check the Enable parameter of the
   corresponding LocalAgent.MTP.{i}. object and setup an IMTP connection if it is also enabled.

   If an MTP is disabled (TODO) or deleted (TODO), close the IMTP connection.

   @param ctx a bus context
   @return 0 when subscribed successfully, -1 in case of failure
 */
int uspa_controller_mtp_subscribe(amxb_bus_ctx_t* ctx);

/**
   @brief
   Initializes all enabled Controller MTPs on startup or when a parent Controller instance is
   enabled.

   Will check the Enable parameter of each MTP instance for a provided Controller instance. For
   each enabled instance it will be checked if the corresponding LocalAgent.MTP. instance is also
   enabled. If this is the case, setup an IMTP connection to the MTP.

   @param ctx a bus context
   @param contr_path a path to an enabled controller instance
   @return 0 in case the IMTP connection was set up, -1 otherwise
 */
int uspa_controller_mtp_init(amxb_bus_ctx_t* ctx, const char* contr_path);

/**
   @brief
   Get the value of 'AgentMTPReference' based on LocalAgent.Controller.{i}.MTP.{i}.MQTT.
   instance path.

   @param contr_mtp controller MTP path
   @return the value of 'AgentMTPReference' parameter
 */
char* uspa_controller_mtp_get_agent_mtp_reference(const char* contr_mtp);

/**
   @brief
   Cleans up the subscriptions that were added with @ref uspa_controller_mtp_subscribe

   @param ctx a bus context
 */
void uspa_controller_mtp_clean(amxb_bus_ctx_t* ctx);

#ifdef __cplusplus
}
#endif

#endif // __USPA_CONTROLLER_MTP_H__
