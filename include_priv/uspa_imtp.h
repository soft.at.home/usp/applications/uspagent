/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPA_IMTP_H__)
#define __USPA_IMTP_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <usp/uspl.h>

#include "uspa.h"

/**
   @brief
   Sets the LocalAgent.MTP.{i}.SocketStatus parameter to a value.
   Possible values are "Connecting", "Up", "Down" and "Error".

   The SocketStatus is used to track the status of the IMTP socket connection
   to the MTP.

   @param mtp_inst MTP instance path
   @param status new value for the SocketStatus
   @return 0 in case of success, any other value indicates an error
 */
int uspa_imtp_set_status(const char* mtp_inst, const char* status);

/**
   @brief
   Connects the USP agent with the MTP plugin (e.g. the mqtt client)

   @param reference path to the MTP that needs to be connected
   @param mtp_id MTP type id
   @param la_mtp LocalAgent.MTP.{i}. reference path (used for tracking)
   @return 0 in case a connection was successfully established, -1 otherwise
 */
int uspa_imtp_connect(const char* reference,
                      uint32_t mtp_id,
                      const char* la_mtp);

/**
   @brief
   Disconnects the USP agent from the MTP plugin

   @param con connection struct that was created during connection setup
   @param type_id type of the MTP connection
   @return 0 in case of success, -1 otherwise
 */
int uspa_imtp_disconnect(uspi_con_t* con, uint32_t type_id);

/**
   @brief
   Sends data to the connected MTP over the IMTP connection

   @param con connection to send message on
   @param usp_tx USP TX struct with protobuf to transmit
   @param mtp_info MTP metadata variant
   @return number of bytes sent in case of success, -1 otherwise
 */
int uspa_imtp_send(uspi_con_t* con, uspl_tx_t* usp_tx, amxc_var_t* mtp_info);

/**
   @brief
   Function to clean up the uspa_imtp_con_t from the static linked list used to track the
   connections

   @param it linked list iterator for the uspa_imtp_con_t
 */
void uspa_imtp_list_free(amxc_llist_it_t* it);

/**
   @brief
   Get a uspi_con_t struct that belongs to a LocalAgent MTP instance

   @param mtp instance path to a LocalAgent.MTP.
   @param mtp_id MTP protocol type of the MTP instance
   @return pointer to a uspi_con_t struct if a match is found for the provided MTP path,
           NULL otherwise
 */
uspi_con_t* uspa_imtp_con_from_la_mtp(const char* la_mtp, uint32_t mtp_id);

/**
   @brief
   Get the first uspi_con_t struct that belongs to a Controller instance.

   @note
   It is possible that a controller has multiple MTPs. This function will return the
   uspi_con_t struct for the first found MTP.

   @param contr_inst instance path to a LocalAgent.Controller.
   @return pointer to a uspi_con_t struct if a match is found for the provided controller path,
           NULL otherwise
 */
uspi_con_t* uspa_imtp_con_from_contr(const char* contr_inst);

#ifdef __cplusplus
}
#endif

#endif // __USPA_IMTP_H__
