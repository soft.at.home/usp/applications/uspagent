/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPA_DISCOVERY_H__)
#define __USPA_DISCOVERY_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "uspa.h"

/**
   @brief
   Returns the back-end context for the registered path.

   @note
   This function will currently only look for registered root paths (optionally prefixed with
   'Device.'). More elaborate search logic will be implemented later

   @param path requested object path
   @return back-end context where the requested object is registered
 */
amxb_bus_ctx_t* uspa_discovery_ctx_from_imtp(const char* path);

/**
   @brief
   Check if the provided path belongs to a local data model or a remote (registered) data model.

   This function will check if the path was registered by a USP Service

   @param path object path to check
   @return true if the object is registered, false otherwise
 */
bool uspa_discovery_object_is_remote(const char* path);

/**
   @brief
   Discover which back-end context to use for the requested path

   @param path data model path
   @return the back-end context if found, NULL otherwise
 */
amxb_bus_ctx_t* uspa_discovery_get_ctx(const char* path);

void _uspa_discovery_service_added(const char* const sig_name,
                                   const amxc_var_t* const data,
                                   void* const priv);

void _uspa_discovery_service_removed(const char* const sig_name,
                                     const amxc_var_t* const data,
                                     void* const priv);

void _uspa_discovery_service_registered(const char* const sig_name,
                                        const amxc_var_t* const data,
                                        void* const priv);

#ifdef __cplusplus
}
#endif

#endif // __USPA_DISCOVERY_H__
