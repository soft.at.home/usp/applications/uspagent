/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPA_OBJECT_H__)
#define __USPA_OBJECT_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "uspa.h"

/**
   @brief
   Helper macro for getting a value

   This helper macro gets a value, using a type indicator

   @param type the data type that must be returned
   @param object pointer to a data model object
   @param name parameter name
 */
#define uspa_object_get_value(type, object, name) \
    uspa_object_get_ ## type(object, name)

/**
   @brief
   Gets a boolean value of a parameter from the data model.

   @note
   If the parameter is not found, false is returned

   @param obj_path a path data model object. MUST not be a search path
   @param param parameter name
   @return the boolean value of the parameter
 */
bool uspa_object_get_bool(const char* obj_path, const char* param);

/**
   @brief
   Gets a uint32_t value of a parameter from the data model.

   @note
   If the parameter is not found, 0 is returned

   @param obj_path a path data model object. MUST not be a search path
   @param param parameter name
   @return the unsigned integer value of the parameter
 */
uint32_t uspa_object_get_uint32_t(const char* obj_path, const char* param);

/**
   @brief
   Gets a string value of a parameter from the data model.

   @note
   If the parameter is not found, NULL is returned

   @param obj_path a path data model object
   @param param parameter name
   @return dynamically allocated string if found, or NULL otherwise
 */
char* uspa_object_get_cstring_t(const char* obj_path, const char* param);

/**
   @brief
   Updates the read-only status parameter of a data model object by calling the protected method
   UpdateStatus.

   Can only be used for instances of LocalAgent.Request.{i}. or LocalAgent.MTP.{i}. objects.

   @param obj_path data model path to a valid object to update
   @param status value of the status parameter
   @return 0 in case of success, any other value indicates an error
 */
int uspa_object_update_status(const char* obj_path, const char* status);

/**
   @brief
   Determine whether the provided object exists in the instantiated data model.

   If the provided path is a search path, the function will return true if at least one instance is
   found in the data model.

   @param ctx bus context to search for the object
   @param object object path, can be a search path
   @return true if the object is found in the instantiated data model
 */
bool uspa_object_is_instantiated(amxb_bus_ctx_t* ctx, const char* object);

/**
   @brief
   Checks whether the provide object exists with amxb_describe for regular paths and with
   amxb_resolve for search paths

   @param ctx bus context to use
   @param object object path
   @return true if the object exists, false if it doesn't
 */
bool uspa_object_exists(amxb_bus_ctx_t* ctx, const char* object);

#ifdef __cplusplus
}
#endif

#endif // __USPA_OBJECT_H__
