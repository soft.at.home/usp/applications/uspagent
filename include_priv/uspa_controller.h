/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPA_CONTROLLER_H__)
#define __USPA_CONTROLLER_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <uspi/uspi_connection.h>

#include "uspa.h"

/**
   @brief
   Subscribe to events for LocalAgent.Controller.{i}. and act accordingly

   If a Controller is enabled, check the enable parameters in the LocalAgent.Controller.{i}.MTP.
   and LocalAgent.Controller. data model and setup an IMTP connection if they are both enabled.

   If an MTP is disabled (TODO) or deleted (TODO), close the IMTP connection.

   When an OnBoardRequest! notification is received, send an on board request to the corresponding
   controller.

   @param ctx a bus context
   @return 0 when subscribed successfully, -1 in case of failure
 */
int uspa_controller_subscribe(amxb_bus_ctx_t* ctx);

/**
   @brief
   Gets the LocalAgent.Controller.{i}.Enable parameter based on the provided instance path.

   @param instance a path to a Controller instance
   @return true in case the controller is enabled
   @return false in case the controller is disabled
 */
bool uspa_controller_get_enable(const char* instance);

/**
   @brief
   Gets the LocalAgent.Controller.{i}.EndpointID parameter based on the provided instance path.

   @note
   Memory is dynamically allocated for the returned value and must be freed by the caller.

   @param instance a path to a Controller instance
   @return dynamically allocated EndpointID if found, NULL otherwise
 */
char* uspa_controller_get_eid(const char* instance);

/**
   @brief
   Gets the LocalAgent.Controller.{i}. instance path based on the provided EndpointID

   @note
   Memory is dynamically allocated for the returned value and must be freed by the caller.

   @param eid EndpointID of the controller
   @return dynamically allocated controller instance path if found, NULL otherwise
 */
char* uspa_controller_get_instance_from_eid(const char* eid);

/**
   @brief
   Initializes all enabled controllers on startup.

   Will check for each controller if the MTP child instances are enabled and if the corresponding
   LocalAgent.MTP. instances are enabled. If all of them are enabled, setup an IMTP connection
   to the MTPs that are used by this controller.

   @param ctx a bus context
   @return 0 in case the IMTP connection was set up, -1 otherwise
 */
int uspa_controller_init(amxb_bus_ctx_t* ctx);

/**
   @brief
   Cleans up the uspa_contr_instance_t structs and the subscriptions that were added in
   @ref uspa_controller_subscribe.

   @param ctx a bus context
 */
void uspa_controller_clean(amxb_bus_ctx_t* ctx);

int uspa_controller_con_add(const char* contr_inst, const char* contr_mtp_inst, uspi_con_t* con);

int uspa_controller_con_del(const char* contr_inst, const char* contr_mtp_inst);

void uspa_controller_con_link(uspi_con_t* con, const char* protocol, const char* la_mtp);

void uspa_controller_con_unlink(uspi_con_t* con);

uspa_contr_instance_t* uspa_contr_instance_get(const char* contr_inst);

uspi_con_t* uspa_controller_con_get(const char* contr_inst);

/**
   @brief
   Returns the bus access for the controller based on its EndpointID.

   The controller's bus access is fetched from the LocalAgent.Controller.{i}.ProtectedAccess parameter

   @param controller_id EndpointID of the controller
   @return AMXB_PROTECTED if the controller has protected bus access, AMXB_PUBLIC if the controller
           has public bus access
 */
uint32_t uspa_controller_get_access(const char* controller_id);

/**
   @brief
   When a NotifyResponse message is received with an empty ID, it means we are dealing with a
   response for an OnBoardRequest. We can then use the top level message ID to find out which
   OnBoardRequest was confirmed, so it doesn't need to be retried anymore.

   @param msg_id message id for the received NotifyResponse
 */
void uspa_controller_obr_done(const char* msg_id);

#ifdef __cplusplus
}
#endif

#endif // __USPA_CONTROLLER_H__
