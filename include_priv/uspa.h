/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPA_H__)
#define __USPA_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <yajl/yajl_gen.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxj/amxj_variant.h>
#include <amxp/amxp.h>
#include <amxp/amxp_dir.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_path.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <amxa/amxa.h>

#include <usp/uspl.h>
#include <uspi/uspi_connection.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "uspagent"

#define USPA_MTP_ID_NULL 0
#define USPA_MTP_ID_MQTT 1
#define USPA_MTP_ID_IMTP 2
#define USPA_SUB_RETRY_MAX 9
#define AGENT_SUPPORTED_PROTOCOL_VERSIONS "1.0,1.1,1.2,1.3"
#define USPA_IMTP_SOCK 100

#ifndef when_true_do
#define when_true_do(x, l, c) if(!(x)) { c; goto l; }
#endif

#ifndef when_false_do
#define when_false_do(x, l, c) if(!(x)) { c; goto l; }
#endif

amxo_parser_t* uspa_get_parser(void);

int _uspa_main(int reason,
               amxd_dm_t* dm,
               amxo_parser_t* parser);

// Struct with global app information
typedef struct _uspa_app {
    amxo_parser_t* parser;              // Parser
    amxb_bus_ctx_t* bus_ctx;            // One of the connected bus contexts
} uspa_app_t;

// Struct to manage some bookkeeping for the MQTT MTP
typedef struct _uspa_mtp_mqtt {
    uspi_con_t* con;
    char* response_topic;               // MQTT response topic used by the agent
    char* reference;                    // MQTT.Client.X. reference where the connection was made
} uspa_mtp_mqtt_t;

// Struct to manage some bookkeeping for the IMTP MTP
typedef struct _uspa_mtp_imtp {
    uspi_con_t* con_agent;
    uspi_con_t* con_controller;
} uspa_mtp_imtp_t;

typedef struct _uspa_mtp_instance {
    amxc_htable_it_t hit;
    char* la_mtp;                       // LocalAgent.MTP. instance path
    amxp_timer_t* socket_retry_timer;   // Timer to retry setting up IMTP connection to MTP
    uint32_t mtp_id;                    // MTP type ID to track which MTP is used for this connection
    union {                             // Based on type_id
        uspa_mtp_mqtt_t* mqtt;
        uspa_mtp_imtp_t* imtp;
    } mtp_data;
} uspa_mtp_instance_t;

// Struct for asynchronous operations
typedef struct _uspa_request {
    char* request_obj;                  // Instance path to the request object
    amxb_request_t* call_request;       // To track the asynchronous request and cancel it if needed
    amxc_var_t* out_args;               // Fake output arguments in case request is not actually called
} uspa_request_t;

// Struct to keep track of subscription instances
typedef struct _uspa_sub {
    amxc_llist_it_t lit;               // Linked list iterator to find struct in list of subs
    char* instance_path;               // Path to the corresponding LocalAgent.Subscription.{i}. instance
    char* contr_path;                  // Path to the LocalAgent.Controller.{i}. recipient
    char* id;                          // Subscription ID
    char* notif_type;                  // NotifType
    amxc_llist_t sub_ref_list;         // List of uspa_sub_ref_t structs. There will be one for each entry in the ReferenceList
    amxc_llist_t retry_list;           // List to keep track of notifications that must be retried.
    amxp_timer_t* periodic_timer;      // Timer for periodic subscriptions
    bool boot_notif;                   // Set to true if the subscription is of type ValueChange for a single parameter
} uspa_sub_t;

typedef struct _uspa_sub_ref {
    amxc_llist_it_t lit;
    uspa_sub_t* sub;
    char* ref_path;
} uspa_sub_ref_t;

// Struct to keep track of notifications that need to be retried.
// There can be multiple for the same subscription instance.
typedef struct _uspa_sub_retry {
    amxc_llist_it_t lit;                // Linked list iterator to find struct in list of retries
    uspl_tx_t* usp_tx;                  // Pointer to struct containing protobuf to retransmit
    amxp_timer_t* retry_timer;          // Timer to retry notification
    amxp_timer_t* expiration_timer;     // Timer to let notification expire
    uint32_t retry_time;                // Time until next notification
    uint32_t retry_attempts;            // Counter to keep track of how many times the notification has been retried
} uspa_sub_retry_t;

// Wrapper struct to add con to linked list
typedef struct _uspa_contr_con {
    amxc_llist_it_t lit;                // To track struct in cons list of uspa_contr_instance_t
    uspi_con_t* con;                    // Active connection that can be used by the controller
    char* contr_mtp_inst;               // Path of the controller MTP instance in use
} uspa_contr_con_t;

// Struct to track data about controller instances
typedef struct _uspa_contr_instance {
    amxc_htable_it_t hit;               // To track instance in hash table of instances
    amxc_llist_t cons;                  // To track connections used by controller
    bool enabled;                       // To track if controller is enabled
    uspa_sub_retry_t* obr_retry;        // For OnBoardRequest retry mechanism
} uspa_contr_instance_t;

#ifdef __cplusplus
}
#endif

#endif // __USPA_H__
