/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPA_CONTROLLER_MTP_MQTT_H__)
#define __USPA_CONTROLLER_MTP_MQTT_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "uspa.h"

/**
   @brief
   Gets the instance path of an enabled controller MTP based on an MQTT reference path.

   This function first gets the value of LocalAgent.Controller.*.MTP.[MQTT.Reference == reference].
   and checks whether the first found MTP is enabled. If it is enabled, the instance path is
   returned. Else NULL is returned.

   @note
   An MQTT reference should only be used by a single Controller MTP.

   @note
   The returned string is dynamically allocated and must be freed.

   @param reference an instance path to an MQTT Client
   @return the path to an enabled Controller MTP or NULL if it disabled or not found
 */
char* uspa_controller_mtp_mqtt_get_enabled_mtp(const char* reference);

/**
   @brief
   Get the controller's subscribed MQTT topic based on its EndpointID

   Returns the result of a GET on LocalAgent.Controller.[EndpointID == eid].MTP.*.MQTT.Topic

   @note
   Memory is allocated for the returned string and must be freed by the caller

   @param eid the controller EndpointID
   @return the controller's subscribed topic if found, NULL otherwise
 */
char* uspa_controller_mtp_mqtt_get_topic(const char* eid);

/**
   @brief
   Initializes the Controller MTP of type MQTT on startup or when an enabled MTP is added.

   @param contr_inst path of a Controller instance
   @param contr_mtp_inst path of a Controller MTP instance
   @return 0 in case the IMTP connection was correctly set up, -1 otherwise
 */
int uspa_controller_mtp_mqtt_init(const char* contr_inst, const char* contr_mtp_inst);

#ifdef __cplusplus
}
#endif

#endif // __USPA_CONTROLLER_MTP_MQTT_H__
