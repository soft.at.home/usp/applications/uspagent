/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__USPA_MSGHANDLER_H__)
#define __USPA_MSGHANDLER_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "uspa.h"
#include "uspa_discovery.h"
#include "usp/uspl_msghandler.h"

typedef int (* fn_reply_new)(uspl_tx_t* usp_tx,
                             amxc_llist_t* resp_list,
                             const char* msg_id);

void PRIVATE uspa_msghandler_send_usp_record(uspi_con_t* con,
                                             uspl_tx_t* usp_tx,
                                             amxc_var_t* mtp_info);

void PRIVATE uspa_msghandler_process_binary_record(unsigned char* pbuf,
                                                   int pbuf_len,
                                                   uspi_con_t* con,
                                                   amxc_var_t* mtp_info);

int PRIVATE uspa_handle_get(uspl_rx_t* usp_rx,
                            uspl_tx_t** usp_tx,
                            const char* acl_file);

int PRIVATE uspa_handle_set(uspl_rx_t* usp_rx,
                            uspl_tx_t** usp_tx,
                            const char* acl_file);

int PRIVATE uspa_handle_add(uspl_rx_t* usp_rx,
                            uspl_tx_t** usp_tx,
                            const char* acl_file);

int PRIVATE uspa_handle_delete(uspl_rx_t* usp_rx,
                               uspl_tx_t** usp_tx,
                               const char* acl_file);

int PRIVATE uspa_handle_get_supported_dm(uspl_rx_t* usp_rx,
                                         uspl_tx_t** usp_tx,
                                         const char* acl_file);

int PRIVATE uspa_handle_notify_resp(uspl_rx_t* usp_rx,
                                    uspl_tx_t** usp_tx,
                                    const char* acl_file);

int PRIVATE uspa_handle_notify(uspl_rx_t* usp_rx,
                               uspl_tx_t** usp_tx,
                               const char* role);

int PRIVATE uspa_handle_operate(uspl_rx_t* usp_rx,
                                uspl_tx_t** usp_tx,
                                const char* acl_file);

int PRIVATE uspa_handle_get_supported_proto(uspl_rx_t* usp_rx,
                                            uspl_tx_t** usp_tx,
                                            const char* acl_file);

int PRIVATE uspa_handle_get_instances(uspl_rx_t* usp_rx,
                                      uspl_tx_t** usp_tx,
                                      const char* acl_file);

int PRIVATE uspa_handle_register(uspl_rx_t* usp_rx,
                                 uspl_tx_t** usp_tx,
                                 const char* acl_file);

int PRIVATE uspa_msghandler_build_reply(uspl_rx_t* usp_rx,
                                        amxc_llist_t* resp_list,
                                        fn_reply_new fn,
                                        uspl_tx_t** usp_tx);

int PRIVATE uspa_msghandler_build_error(uspl_rx_t* usp_rx,
                                        uspl_tx_t** usp_tx,
                                        int err_code);

/**
 * @brief
 * Builds a basic response message based on a provided request message.
 *
 * Extracts the from_id, to_id and msg_id from the incoming request and prepares a uspl_tx_t struct
 * with inverted endpoint ids. The prepared uspl_tx_t struct can then be used to build the specific
 * USP Response or USP Error message.
 *
 * @param usp_rx received USP message
 * @param usp_tx USP response that will eventually be sent
 * @return 0 in case of success, -1 in case of error
 */
int PRIVATE uspa_msghandler_build_reply_basic(uspl_rx_t* usp_rx,
                                              uspl_tx_t** usp_tx);

#ifdef __cplusplus
}
#endif

#endif // __USPA_MSGHANDLER_H__
