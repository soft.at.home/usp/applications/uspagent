# USP Subscriptions

A USP Controller is able to create subscriptions for events using the [Notifications and Subscription Mechanism](https://usp.technology/specification/07-index-messages.html#sec:notifications-and-subscriptions), by adding instances to the [Device.LocalAgent.Subscription.](https://usp-data-models.broadband-forum.org/tr-181-2-16-0-usp.html#D.Device:2.Device.LocalAgent.Subscription.{i}.) data model.

> NOTE:  
> The provided data model link is for version 2.16 of the TR-181 USP data model. It is always best to check if there is a new version yet.

The subscription mechanism is well documented in TR-369 and TR-181 and will not be repeated here. This document will mainly explain and illustrate how the subscription mechanism works for distributed data models and in particular if data models are distributed over different containers.

## Subscriptions for distributed data models

This section will illustrate how subscriptions are created for data models that are local to the `uspagent` i.e. they are reachable on a local bus, and also how subscriptions are created for data models that are available in a container i.e. they are reachable over the UnixDomainSocket (UDS) MTP and not on the local bus.

We will try to simulate a USP Controller that lives somewhere in the cloud, which is interested in creating USP subscriptions on an embedded device (a gateway or a repeater) with a USP Agent. The embedded device will have a USP agent and a number of data model services that are reachable on the local bus. We will refer to this as the host system. The device will also have an LCM container installed, which will connect to the USP Agent using a UnixDomainSocket connection from the USP Service within the container. The container will also have some other data model services running and we will be subscribing to data model events from one of these services. We will refer to this as the container system.

To summarize, we will have 3 systems: a cloud system with the USP controller, a host system with the USP agent and a container system with the USP Service. For illustrative purposes, each of these systems will run in its own docker container, but everything should work exactly the same on an embedded device with an LCM container.

> NOTE:  
> The most important data model elements will be explained for each system in the next few sections. We will start from the current default settings and update or extend the data models according to our needs. It is always possible that the defaults change over time and that this document is not updated accordingly. Therefore, the full data model dumps for all systems will also be provided at the bottom of this document.

### Setting up the cloud system with the USP controller

In this container we will be setting up a USP controller that will create subscriptions using USP Add messages and it will receive the corresponding USP notifications when events occur.

In this container we will be running the following components:

- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint) will be run as a controller with the `uspc` command.
- [tr181-mqtt](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-mqtt) as an MQTT client to send USP requests to our host system and to receive notifications.

Refer to the READMEs of both components to get them installed.

When both plugins are installed, they can be run in the background with the following commands:

```bash
tr181-mqtt -D
uspc -D
```

Make sure the pcb bus is also running in this container.

The default data model values are probably sufficient, but you should double check these values for the controller:

```
LocalController.EndpointID="proto::Controller-a9bf597211ac"
LocalController.MTP.1.MQTT.Reference="MQTT.Client.cpe-mqtt-uspc."
LocalController.MTP.1.MQTT.ResponseTopicConfigured="controller/proto::Controller-a9bf597211ac"
```

The format of the ResponseTopicConfigured will be `controller/<EndpointID>`, where the EndpointID is built based on the hostname of your container. When docker containers are used, this will be a seemingly random value.

In the MQTT data model, you should check the following parameters:

```
MQTT.Client.4.Alias="cpe-mqtt-uspc"
MQTT.Client.4.BrokerAddress="broker.hivemq.com"
MQTT.Client.4.BrokerPort=1883
MQTT.Client.4.Password=""
MQTT.Client.4.Username=""
MQTT.Client.4.Subscription.1.Topic="controller/proto::Controller-a9bf597211ac"
```

Instead of using the public broker, you may want to use a different broker instead. If the broker requires credentials, these also need to be configured. There should be at least one subscription where the Topic matches the ResponseTopicConfigured from above.

There are more parameters in the MQTT data model that can be configured, but they are not explained here. Refer to the MQTT data model documentation if you're using a more complex setup.

Make sure you also read the [usp-endpoint README](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint/-/blob/main/README.md) for information on how to send USP messages. This will be used later on, but will not be explained here.

### Setting up the host system with the USP agent

This container will represent the host system of the embedded device. If you are using a docker container to setup this system, make sure you mount a shared folder to the container. This shared folder will contain the listen socket that will be set up by the USP Agent, that the USP Service will connect to.

If you are testing this with a real device and installing the container using LCM, you need to make sure there is also a folder mounted to the container when it is being set up. How to do this is out of the scope of this document.

We will run the following processes here:

- A system bus for which we have an ambiorix bus backend. We recommend either pcb or ubus at this point.
- [tr181-mqtt](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-mqtt): Our MQTT client for receiving messages from the USP controller and sending responses + notifications back
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): The process to configure our LocalAgent data model
- [tr181-uds](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-uds/): The process to track unix domain sockets.
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): The USP agent that will handle all incoming/outgoing USP requests and responses. Furthermore, it will also serve as a broker to forward USP messages to our container system. Therefore, it is also known as the USP Broker.
- [acl-manager](https://gitlab.com/prpl-foundation/components/ambiorix/applications/acl-manager): The process responsible for Access Control Lists. We don't want just any controller to access our data models!
- Any other ambiorix plugin that provides a data model. We will use this data model to test the subscriptions.

Follow the READMEs of the listed components to get all of them installed and make sure a system bus is running.

> WARNING:  
> Run all plugins with sudo privileges to avoid issues with permissions. We will be using environment variables for the setup, so make sure to run things as `sudo -E` to expose the environment variables to the root user (or export and run everything directly as the root user).

When everything is installed, we can again run `tr181-mqtt` and check the data model. 

```
MQTT.Client.1.
MQTT.Client.1.Alias="cpe-default"
MQTT.Client.1.BrokerAddress="broker.hivemq.com"
MQTT.Client.1.BrokerPort=1883
MQTT.Client.1.Password=""
MQTT.Client.1.Username=""
MQTT.Client.1.Subscription.1.Topic="agent/proto::Agent-748cb38f0275"
```

In this example we will use the default client to communicate with the USP controller. Make sure the `BrokerAddress` and `BrokerPort` are the same as on the controller side. On the agent side, there must be a subscription on `Topic` `agent/<EndpointID>`. This subscription will automatically be added after we run `tr181-localagent` and the `uspagent`.

The `tr181-uds` process will manage the `UnixDomainSockets` data model, which contains a list of all unix domain sockets that need to be created by the `uspagent`. Before running the process, you should set 2 environment variables:

```bash
export BROKER_AGENT_SOCK=/home/user/sockets/broker_agent_path
export BROKER_CONTROLLER_SOCK=/home/user/sockets/broker_controller_path
```

> NOTE:  
> Use the same directory that will be shared with the container system. On target, the shared directory will be `/var/run/usp/`.

Then run `tr181-uds` and check the data model to see if the sockets are configured as expected. You should see somthing similar to this:

```
UnixDomainSockets.
UnixDomainSockets.UnixDomainSocketNumberOfEntries=2
UnixDomainSockets.UnixDomainSocket.1.
UnixDomainSockets.UnixDomainSocket.1.Alias="broker-agent"
UnixDomainSockets.UnixDomainSocket.1.Mode="Listen"
UnixDomainSockets.UnixDomainSocket.1.Path="/home/user/sockets/broker_agent_path"
UnixDomainSockets.UnixDomainSocket.2.
UnixDomainSockets.UnixDomainSocket.2.Alias="broker-controller"
UnixDomainSockets.UnixDomainSocket.2.Mode="Listen"
UnixDomainSockets.UnixDomainSocket.2.Path="/home/user/sockets/broker_controller_path"
```

Next we will run `tr181-localagent` and make some adjustments to its data model. If you look at the data model, you should see something similar to this:

```
LocalAgent.MTP.1.
LocalAgent.MTP.1.Alias="mtp-uds"
LocalAgent.MTP.1.Enable=true
LocalAgent.MTP.1.EnableMDNS=true
LocalAgent.MTP.1.Protocol="UDS"
LocalAgent.MTP.1.Status="Down"
LocalAgent.MTP.1.UDS.
LocalAgent.MTP.1.UDS.UnixDomainSocketRef="Device.UnixDomainSockets.UnixDomainSocket.1"
```

You will see a reference to the agent socket that needs to be created by the `uspagent`. There is no pointer to the controller socket. The `uspagent` will determine the controller socket by replacing the word `agent` in the agent socket path to `controller`.

> NOTE:  
> The missing link to the controller socket could be seen as a defect in the specification. It's best if you don't think too much about this, because overthinking it won't help.

Next we will add an MTP to communicate over MQTT. This can be done from `pcb-cli` or `ubus-cli` with

```
LocalAgent.MTP.+{Alias = "mtp-mqtt", Enable = false, Protocol = "MQTT"}
LocalAgent.MTP.mtp-mqtt.MQTT.Reference="MQTT.Client.cpe-default."
LocalAgent.MTP.mtp-mqtt.Enable = true
```

We will also configure our cloud controller with the right credentials to contact our host system.

```
LocalAgent.Controller.+{Alias = "controller-cloud", EndpointID = "proto::Controller-a9bf597211ac", Enable = false, AssignedRole = "LocalAgent.ControllerTrust.Role.2"}
LocalAgent.Controller.controller-cloud.MTP+{Alias = "mtp-mqtt", Enable = true, Protocol = "MQTT"}
LocalAgent.Controller.controller-cloud.MTP.mtp-mqtt.MQTT.{AgentMTPReference = "LocalAgent.MTP.1.", Topic = "controller/proto::Controller-a9bf597211ac"}
LocalAgent.Controller.controller-cloud.Enable = true
```

> WARNING:  
> Pay close attention to the EndpointID of the controller you are adding. It will be a different value for your controller, so make sure to update it accordingly.

Note that our LocalAgent data model already has another controller pre-configured:

```
LocalAgent.Controller.1.Alias="uspe"
LocalAgent.Controller.1.AssignedRole="Device.LocalAgent.ControllerTrust.Role.2"
LocalAgent.Controller.1.EndpointID="proto::uspe"
```

This is the controller that will run in the container system. We will get back to this later.

Finally we will run our `uspagent` process. This one will read the configurations from the `LocalAgent` data model and connect the needed MTPs. At this stage you should check whether the socket has been created in your shared directory

```bash
$ ls ~/sockets/
broker_agent_path  broker_controller_path
```

and check the Status of the MQTT client

```
MQTT.Client.1.Status="Connected"
```

Before moving on to the container system, let's also run an extra ambiorix plugin that exposes a data model. We will use this data model to test the subscription mechanism. As an example we will use the `Phonebook` data model

```odl
%define {
    object Phonebook {
        object Contact[] {
            string FirstName;
            string LastName;

            object PhoneNumber[] {
                string Phone;
            }

            object E-Mail[] {
                string E-Mail;
            }
        }
    }
}
```

You can run this data model with the following command:

```bash
amxrt -D phonebook_definition.odl`
```

### Setting up the container system with the USP Service

The final part of our setup will be a docker container that represents an LCM container that could be installed on your embedded target. It will contain a USP Service that exposes an additional data model to the host system.

We will run the following processes within this container:

- A system bus for which we have an ambiorix bus backend. We recommend either pcb or ubus at this point.
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): Here the usp-endpoint will represent a USP Service in a container. We will run the process with `uspe`.
- [greeter_plugin](https://gitlab.com/prpl-foundation/components/ambiorix/examples/datamodel/greeter_plugin): An example plugin that exposes a data model. We will subscribe to changes in this data model.
- [tr181-uds](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-uds/) (optional): The process to track unix domain sockets.

Before installing the `usp-endpoint` we want to make a small adjustment to the `odl/endpoint/uspe_defaults.odl` file which has the following content:

```odl
%populate {
    object LocalAgent {
        parameter Enable = true;
        object MTP {
            instance add (0, "mtp-uds-host") {
                extend with mib UDS;

                parameter Enable = true;
                parameter Protocol = "UDS";

                object UDS {
                    parameter UnixDomainSocketRef = "";
                }
            }

            instance add (0, "mtp-uds-container") {
                extend with mib UDS;

                parameter Enable = true;
                parameter Protocol = "UDS";

                object UDS {
                    parameter UnixDomainSocketRef = "";
                }
            }
        }
    }
}
```

When we run `uspe` with these default settings, it will connect to the host sockets based on the environment variables `BROKER_CONTROLLER_SOCK` and `BROKER_AGENT_SOCK`. You should set these to the socket paths in the shared directory between the containers e.g.

```bash
export BROKER_AGENT_SOCK=/home/user/sockets/broker_agent_path
export BROKER_CONTROLLER_SOCK=/home/user/sockets/broker_controller_path
```

When these environment variables are not defined, `uspe` will fallback to default socket paths in `/var/run/imtp/broker_agent_path` and `/var/run/imtp/broker_controller_path`, which is fine when an embedded target with an LCM container is used.

> NOTE:  
> To be fully in line with TR-369 we should also run `tr181-uds` to expose the host sockets in the UnixDomainSockets data model. However, as you can see in the data model above, there are no links from the LocalAgent data model to the UnixDomainSockets data model, since UnixDomainSocketRefs are empty. One could consider this an oversight in the specification, but don't worry too much about this. I'm sure there is a good reason for it.

We will also add a default EndpointID for the endpoint in the container:

```odl
%populate {
    object LocalAgent {
        parameter EndpointID = "proto::uspe";
    }
}
```

Note that this EndpointID corresponds with the EndpointID of a controller we have configured in the host system.

When these things are updated, we can install the `usp-endpoint` with the regular `make && sudo make install` command.

> NOTE:  
> `uspe` will also set up its own listen sockets inside the container at `/var/run/usp/endpoint_agent_path` and `/var/run/usp/endpoint_controller_path`. Any ambiorix plugin running inside the container will automatically connect to these sockets and register its data model on the controller socket i.e. `/var/run/usp/endpoint_controller_path`. The USP Service (uspe) will forward this registration to the host system via the host controller socket. That way, the host system knows which data models are exposed by the container system and it knows where to forward which messages. More information about the usp-endpoint can be found in its [documentation](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint/-/blob/main/doc/mtp_uds_configuration.md).

Now we can run the USP Service in the container with the `uspe` command and, because of its configuration, it will connect to the listen socket of the host on startup.

Before we can create subscriptions, there is one last configuration step we need to do. The USP controller in the cloud will create a subscription with a USP Add message on `LocalAgent.Subscription.` with some data model paths in the `ReferenceList` parameter. This message will be sent to the USP agent on the host system which will evaluate the different paths in the `ReferenceList`. If one of these paths is a data model managed by the container system, the USP agent will forward the USP Add message for that path to the container. When it does this, it will act as a controller. This means that the USP agent of the host needs to be configured as a valid controller in the `LocalAgent.` data model of the container system. This can be done from `pcb-cli` or `ubus-cli` with the following commands:

```
LocalAgent.Controller.+{Alias = "uspagent", EndpointID = "proto::Agent-748cb38f0275", Enable = true}
LocalAgent.Controller.uspagent.MTP+{Alias = "mtp-uds", Enable = true, Protocol = "UDS"}
```

> NOTE  
> Our `uspagent` is actually both an agent and a controller. With the above commands, we added a controller with Alias `uspagent` to the data model, which could be a bit confusing, however we decided to give it the same name as the process that is running on the host system.
> Also note that you will very likely need to use a different `EndpointID` for it depending on your container hostname.

Alternatively, you can add the Controller instance by updating the `odl/endpoint/uspe_defaults.odl` file, installing it and restarting `uspe`.

After this is done, the configuration is complete. You should now also install and run the `greeter` plugin. If everything goes right, its data model will automatically be registered at the USP agent.

This can be checked in the host system with `pcb-cli` or `ubus-cli` in protected mode:

```
user - pcb:/var/run/pcb_sys - [pcb-cli] (0)
 > protected
> connections
> !addon select mod_ba connection
> access pcb:/var/run/pcb_sys protected
> mode-cli
> !addon select mod_ba cli

user - pcb:/var/run/pcb_sys - [pcb-cli] (0)
 > Discovery.?
Discovery.
Discovery.ServiceNumberOfEntries=1
Discovery.Service.
Discovery.Service.1.
Discovery.Service.1.Alias="cpe-Service-1"
Discovery.Service.1.EndpointID="proto::uspe"
Discovery.Service.1.RegistrationNumberOfEntries=1
Discovery.Service.1.Registration.
Discovery.Service.1.Registration.1.
Discovery.Service.1.Registration.1.Path="Greeter."
```

## Creating a subscription and receiving USP notifications

We will now create a USP subscription using `uspc` in the cloud system.

Run `pcb_cli` and invoke the following method:

```bash
LocalController.usp_add("proto::Agent-748cb38f0275", {allow_partial: true, requests: [{object_path: "LocalAgent.Subscription.", parameters: [{param: "Enable", value: "True"}, {param: "ReferenceList", value: "Phonebook.,Greeter."}, {param: "NotifType", value: "ValueChange"}]}]})
```

> NOTE:  
> Again pay attention to the EndpointID used as the first argument of the `usp_add` method.

On the host system you should see this subscription in the `LocalAgent.Subscription.` data model:

```
user - pcb:/var/run/pcb_sys - [pcb-cli] (0)
 > LocalAgent.Subscription.?
LocalAgent.Subscription.
LocalAgent.Subscription.1.
LocalAgent.Subscription.1.Alias="cpe-Subscription-1"
LocalAgent.Subscription.1.CreationDate="0001-01-01T00:00:00Z"
LocalAgent.Subscription.1.Enable=true
LocalAgent.Subscription.1.ID="cpe-ID-1"
LocalAgent.Subscription.1.LastValue=""
LocalAgent.Subscription.1.NotifExpiration=0
LocalAgent.Subscription.1.NotifRetry=false
LocalAgent.Subscription.1.NotifType="ValueChange"
LocalAgent.Subscription.1.Persistent=false
LocalAgent.Subscription.1.Recipient="LocalAgent.Controller.4."
LocalAgent.Subscription.1.ReferenceList="Phonebook.,Greeter."
LocalAgent.Subscription.1.TimeToLive=0
```

Notice that the ReferenceList contains the `Phonebook.` path and `Greeter.` path. Let's add a `Contact` to the `Phonebook` data model and set the `FirstName` parameter to trigger a `ValueChange` notification.

```
user - pcb:/var/run/pcb_sys - [pcb-cli] (0)
 > Phonebook.Contact.+
Phonebook.Contact.1.

 > Phonebook.Contact.1.FirstName="Mufasa"
Phonebook.Contact.1.
Phonebook.Contact.1.FirstName="Mufasa"
```

If you have logging enabled at the controller side in the cloud system, you should now see the notification arriving at the controller:

```
{
    notification_case = 4,
    send_resp = false,
    subscription_id = "cpe-ID-1",
    value_change = {
        param_path = "Phonebook.Contact.1.FirstName",
        param_value = "Mufasa"
    }
}
```

Let's now move to the container system and check the `LocalAgent.Subscription.` data model there.

```
 > LocalAgent.Subscription.?
LocalAgent.Subscription.1.
LocalAgent.Subscription.1.Alias="cpe-Subscription-1"
LocalAgent.Subscription.1.CreationDate="0001-01-01T00:00:00Z"
LocalAgent.Subscription.1.Enable=true
LocalAgent.Subscription.1.ID="cpe-ID-1"
LocalAgent.Subscription.1.NotifExpiration=0
LocalAgent.Subscription.1.NotifRetry=false
LocalAgent.Subscription.1.NotifType="ValueChange"
LocalAgent.Subscription.1.Persistent=false
LocalAgent.Subscription.1.Recipient="Device.LocalAgent.Controller.1"
LocalAgent.Subscription.1.ReferenceList="Greeter."
LocalAgent.Subscription.1.TimeToLive=0
```

Note that the ReferenceList in the Subscription of the container only contains the `Greeter.` path. The subscription for the `Phonebook.` path was not forwarded to the container, because it is part of the host system. If we now change the `Greeter.State` parameter, we will again see notifications arriving at the controller in the cloud system.

On the container system:
```
user - pcb:/var/run/pcb_sys - [pcb-cli] (0)
 > Greeter.State=Start
Greeter.
Greeter.State="Start"
```

On the cloud system:
```
{
    notification_case = 4,
    send_resp = false,
    subscription_id = "cpe-ID-1",
    value_change = {
        param_path = "Greeter.State",
        param_value = "Start"
    }
}

{
    notification_case = 4,
    send_resp = false,
    subscription_id = "cpe-ID-1",
    value_change = {
        param_path = "Greeter.State",
        param_value = "Running"
    }
}
```

Finally, notice that cleaning up the subscription on the host system will also clean up the subscription in the container system:

On the host system:
```
user - pcb:/var/run/pcb_sys - [pcb-cli] (0)
 > LocalAgent.Subscription.*.-
LocalAgent.Subscription.1.
```

On the container system:
```
user - pcb:/var/run/pcb_sys - [pcb-cli] (0)
 > LocalAgent.Subscription.?
No data found
```

Any further changes to the `Greeter.` or `Phonebook.` data models will not result in more USP notifications.

This concludes the explanation of how USP subscriptions work with data models that are distributed over one or multiple containers.

## Ambiorix applications subscribing to remote (UDS accessible) data models

The following subsections will explain how ambiorix processes can subscribe to events for remote data models. When we say `remote` in this context, we mean a data model that is not reachable on the local bus, but is accessible over the UDS MTP (via the USP backend). We will again be referring to a host and container system like in the previous sections, which we assume you have already read. We will show how to create a subscription from the host system to a data model in the container system and vice-versa.

The cloud system will not be used in this section, because all subscriptions will happen on the local buses and UDS connections.

### Subscribing from the host system to the container system

In the host system, make sure that the UDS MTP is still configured to set up listen sockets in the shared directory, for example:

```
LocalAgent.MTP.1.UDS.UnixDomainSocketRef="Device.UnixDomainSockets.UnixDomainSocket.1"
UnixDomainSockets.
UnixDomainSockets.UnixDomainSocketNumberOfEntries=2
UnixDomainSockets.UnixDomainSocket.1.
UnixDomainSockets.UnixDomainSocket.1.Alias="broker-agent"
UnixDomainSockets.UnixDomainSocket.1.Mode="Listen"
UnixDomainSockets.UnixDomainSocket.1.Path="/home/user/sockets/broker_agent_path"
UnixDomainSockets.UnixDomainSocket.2.
UnixDomainSockets.UnixDomainSocket.2.Alias="broker-controller"
UnixDomainSockets.UnixDomainSocket.2.Mode="Listen"
UnixDomainSockets.UnixDomainSocket.2.Path="/home/user/sockets/broker_controller_path"
```

Check that `tr181-localagent`, `uspagent` and the `phonebook` process (or a different data model process) are running.

> REMINDER  
> Run all processes as root user to avoid permission issues.

In the container system, make sure that `uspe` and `greeter` are running and that you still have a Controller instance for the "host controller" i.e. the `uspagent` on the host acting as a controller.

```
LocalAgent.Controller.1.Alias="uspagent"
LocalAgent.Controller.1.EndpointID="proto::Agent-748cb38f0275"
LocalAgent.Controller.1.MTP.1.Protocol="UDS"
```

We will use an interactive CLI application to illustrate that ambiorix processes are able to subscribe to host and container data models using the connection to the `uspagent`. For this purpose, we will use [amx-cli](https://gitlab.com/prpl-foundation/components/ambiorix/applications/amx-cli) and [mod-ba-cli](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amx_cli/mod-ba-cli), so make sure both are installed.

Next we will add an extra `.init` and `.conf` file to run `amx-cli` just the way we want.

On the host system, add a new `/etc/amx/cli/controller-host.init` file with the following contents

```
!amx silent true

# Load Ambiorix Bus Agnostic CLI
!addon load mod_ba /usr/lib/amx/amx-cli/mod-ba-cli.so

# Load USP back-end
backend add /usr/bin/mods/amxb/mod-amxb-usp.so

# Open connection to USP endpoint
connection open usp:/home/$(USER)/sockets/broker_agent_path

# Select connection
connections
select usp:/home/$(USER)/sockets/broker_agent_path

# Set mode cli
!addon select mod_ba cli

# Disable ACL verification by default

!amx variable acl-enable=false
!amx variable acl-dir="/cfg/etc/acl/"

# Define some aliases
!amx alias printenv "!amx variable"
!amx alias setenv "!amx variable"
!amx alias mode-cli "!addon select mod_ba cli"
!amx alias connections "!addon select mod_ba connection"
!amx alias pcb-connection "connections; select pcb:/var/run/pcb_sys; mode-cli;"
!amx alias ubus-connection "connections; select ubus:; mode-cli;"
!amx alias any-connection "connections; select *; mode-cli;"
!amx alias ubus-protected "connections; access ubus: protected; mode-cli"
!amx alias ubus-public "connections; access ubus: public; mode-cli"
!amx alias pcb-protected "connections; access pcb:/var/run/pcb_sys protected; mode-cli"
!amx alias pcb-public "connections; access pcb:/var/run/pcb_sys public; mode-cli"
!amx alias exit "!amx exit"
!amx alias quit "!amx exit"

# Reset history
!history clear
!history load /tmp/ba_cli.history

!amx silent false
```

> WARNING  
> Don't forget to replace `$(USER)` with your username or replace the entire path to the socket with whatever you chose for your shared socket directory.

Then add a new `/etc/amx/cli/controller-host.conf` file with the following contents:

```json
{
    "no-logo": false,
    "no-colors": false,
    "no-self": false,
    "no-self-config": true,
    "self": {
        "no-mod-amx": false,
        "no-mod-help": false,
        "no-mod-history": false,
        "no-mod-addon": false,
        "no-mod-record": false,
        "mod-amx": {
            "no-help": false,
            "no-alias": false,
            "no-prompt": false,
            "no-variable": false,
            "no-__describe": false
        }, 
        "mod-help": {
            "no-__execute": false,
            "no-__complete": false,
            "no-__describe": false
        },
        "mod-history": {
            "no-help": false,
            "no-clear": false,
            "no-show": false,
            "no-save": false,
            "no-load": false,
            "no-__complete_load": false,
            "no-__complete_save": false,
            "no-__complete_help": false,
            "no-__describe": false
        },
        "mod-addon": {
            "no-help": false,
            "no-load": false,
            "no-remove": false,
            "no-select": false,
            "no-list": false,
            "no-__complete_load": false,
            "no-__complete_remove": false,
            "no-__complete_select": false,
            "no-__complete_help": false,
            "no-__describe": false
        },
        "mod-record": {
            "no-help": false,
            "no-start": false,
            "no-stop": false,
            "no-play": false,
            "no-__complete_start": false,
            "no-__complete_play": false,
            "no-__complete_help": false,
            "no-__describe": false
        }
    },
    "usp": {
        "EndpointID": "proto::controller-host"
    }
}
```

You can actually choose which config options you want, but make sure the file contains a `usp` section with an `EndpointID`.

Finally add a symbolic link `/usr/bin/controller-host -> amx-cli`. If you now run `controller-host`, you will actually run `amx-cli` but the `controller-host.init` and `controller-host.conf` files will be loaded. The `.init` file is needed to load the necessary shared objects and connect to the `uspagent` agent socket automatically, while the `.conf` file is necessary to ensure that a sensible `EndpointID` is sent to the `uspagent` upon connection to its listen socket.

Our `controller-host` cli application will act like a USP controller when it communicates with the `uspagent`, so there needs to be a `LocalAgent.Controller.` instance for it in the host data model. Luckily we already have one pre-configured and its main parameters are listed here below:

```
LocalAgent.Controller.2.Alias="controller-host"
LocalAgent.Controller.2.AssignedRole="Device.LocalAgent.ControllerTrust.Role.2"
LocalAgent.Controller.2.Enable="true"
LocalAgent.Controller.2.EndpointID="proto::controller-host"
LocalAgent.Controller.2.MTP.1.Alias="mtp-uds"
LocalAgent.Controller.2.MTP.1.Enable="true"
LocalAgent.Controller.2.MTP.1.Protocol="UDS"
```

Let's start by running `sudo controller-host` and using the cli application to create a subscription for a local data model, for example `Phonebook.`.

```
root - usp:/home/user/sockets/uspa.sock - [pcb-cli] (0)
 > Phonebook.?&
Added subscription for Phonebook.
```

Notice that this new subscription resulted in an extra instance in the `LocalAgent.Subscription.` data model, which we can also see from our cli:

```
root - usp:/home/user/sockets/uspa.sock - [pcb-cli] (0)
 > LocalAgent.Subscription.?
LocalAgent.Subscription.1.
LocalAgent.Subscription.1.Alias="cpe-Subscription-1"
LocalAgent.Subscription.1.CreationDate="0001-01-01T00:00:00Z"
LocalAgent.Subscription.1.Enable="true"
LocalAgent.Subscription.1.ID="cpe-ID-1"
LocalAgent.Subscription.1.NotifExpiration="0"
LocalAgent.Subscription.1.NotifRetry="false"
LocalAgent.Subscription.1.NotifType="AmxNotification"
LocalAgent.Subscription.1.Persistent="false"
LocalAgent.Subscription.1.Recipient="LocalAgent.Controller.2."
LocalAgent.Subscription.1.ReferenceList="Phonebook."
LocalAgent.Subscription.1.TimeToLive="0"
```

The main thing to note here is that a Subscription instance was added with `NotifType` set to `AmxNotification`. This is a custom notification type that was added to ensure Ambiorix notifications can be sent using USP, because the exising notification types are not generic enough. Also note that the `Recipient` was automatically set to `LocalAgent.Controller.2.`, which corresponds to our `controller-host` Controller. The `uspagent` needs this information to know where to send notifications.

If we now make changes to the `Phonebook.` data model, we will see the data model events in our cli application.

```
root - usp:/home/user/sockets/uspa.sock - [pcb-cli] (0)
 > Phonebook.Contact.+{FirstName = "James", LastName = "Bond"}
Phonebook.Contact.1.

<  dm:instance-added> Phonebook.Contact.1.LastName = Bond
<                   > Phonebook.Contact.1.FirstName = James

<    dm:object-added> Phonebook.Contact.1.PhoneNumber.

<    dm:object-added> Phonebook.Contact.1.E-Mail.

root - usp:/home/user/sockets/uspa.sock - [pcb-cli] (0)
 > Phonebook.Contact.1.{FirstName = "Sean", LastName = "Connery"}
Phonebook.Contact.1.
Phonebook.Contact.1.FirstName="Sean"
Phonebook.Contact.1.LastName="Connery"

<  dm:object-changed> Phonebook.Contact.1.LastName = Bond -> Connery
<                   > Phonebook.Contact.1.FirstName = James -> Sean

root - usp:/home/user/sockets/uspa.sock - [pcb-cli] (0)
 > Phonebook.Contact.1.-                                         
Phonebook.Contact.1.
Phonebook.Contact.1.PhoneNumber.
Phonebook.Contact.1.E-Mail.

<  dm:object-removed> Phonebook.Contact.1.PhoneNumber.

<  dm:object-removed> Phonebook.Contact.1.E-Mail.

<dm:instance-removed> Phonebook.Contact.1.
```

Let's now create a subscription for the `Greeter.` data model which lives in the container.

```
root - usp:/home/user/sockets/uspa.sock - [pcb-cli] (0)
 > Greeter.?&
Added subscription for Greeter.
```

This resulted in a new Subscription instance in the host data model

```
LocalAgent.Subscription.2.Alias="cpe-Subscription-2"
LocalAgent.Subscription.2.CreationDate="0001-01-01T00:00:00Z"
LocalAgent.Subscription.2.Enable="true"
LocalAgent.Subscription.2.ID="cpe-ID-2"
LocalAgent.Subscription.2.NotifExpiration="0"
LocalAgent.Subscription.2.NotifRetry="false"
LocalAgent.Subscription.2.NotifType="AmxNotification"
LocalAgent.Subscription.2.Persistent="false"
LocalAgent.Subscription.2.Recipient="LocalAgent.Controller.2."
LocalAgent.Subscription.2.ReferenceList="Greeter."
LocalAgent.Subscription.2.TimeToLive="0"
```

but because the targeted path is a container data model, the `uspagent` forwarded the subscription to the container as well. We can see this in the container system with `pcb-cli/ubus-cli` for example:

```
user - pcb:/var/run/pcb_sys - [pcb-cli] (0)
 > LocalAgent.Subscription.?
LocalAgent.Subscription.1.
LocalAgent.Subscription.1.Alias="cpe-Subscription-1"
LocalAgent.Subscription.1.CreationDate="0001-01-01T00:00:00Z"
LocalAgent.Subscription.1.Enable=true
LocalAgent.Subscription.1.ID="cpe-ID-2"
LocalAgent.Subscription.1.NotifExpiration=0
LocalAgent.Subscription.1.NotifRetry=false
LocalAgent.Subscription.1.NotifType="AmxNotification"
LocalAgent.Subscription.1.Persistent=false
LocalAgent.Subscription.1.Recipient="Device.LocalAgent.Controller.1"
LocalAgent.Subscription.1.ReferenceList="Greeter."
LocalAgent.Subscription.1.TimeToLive=0
```

If we now change the `Greeter.` data model, we will see the Ambiorix events for this as well:

```
root - usp:/home/user/sockets/uspa.sock - [pcb-cli] (0)
 > Greeter.State=Start
Greeter.
Greeter.State="Start"

<  dm:object-changed> Greeter.State = Idle -> Start

<  dm:object-changed> Greeter.State = Start -> Running
```

Finally, notice that if we exit our `controller-host` application, the subscriptions are automatically cleaned up. This can be verified in both the host and container system with `pcb-cli/ubus-cli`:

```
user - pcb:/var/run/pcb_sys - [pcb-cli] (0)
 > LocalAgent.Subscription.?
No data found
```

### Subscribing from the container system to the host system

Subscribing from the container to a host data model will work very similar to subscribing from the host to a container data model. We will again be using `amx-cli` and `mod-ba-cli` to demonstrate this, so make sure they are installed.

Then add a new `/etc/amx/cli/controller-container.init` to the container system.

```
!amx silent true

# Load Ambiorix Bus Agnostic CLI
!addon load mod_ba /usr/lib/amx/amx-cli/mod-ba-cli.so

# Loading USP back-end
backend add /usr/bin/mods/amxb/mod-amxb-usp.so

# Open connection to USP endpoint
connection open usp:/var/run/usp/endpoint_agent_path

# Select connection
connections
select usp:/var/run/usp/endpoint_agent_path

# Set mode cli
!addon select mod_ba cli

# Disable ACL verification by default

!amx variable acl-enable=false
!amx variable acl-dir="/cfg/etc/acl/"

# Define some aliases
!amx alias printenv "!amx variable"
!amx alias setenv "!amx variable"
!amx alias mode-cli "!addon select mod_ba cli"
!amx alias connections "!addon select mod_ba connection"
!amx alias pcb-connection "connections; select pcb:/var/run/pcb_sys; mode-cli;"
!amx alias ubus-connection "connections; select ubus:; mode-cli;"
!amx alias any-connection "connections; select *; mode-cli;"
!amx alias ubus-protected "connections; access ubus: protected; mode-cli"
!amx alias ubus-public "connections; access ubus: public; mode-cli"
!amx alias pcb-protected "connections; access pcb:/var/run/pcb_sys protected; mode-cli"
!amx alias pcb-public "connections; access pcb:/var/run/pcb_sys public; mode-cli"
!amx alias exit "!amx exit"
!amx alias quit "!amx exit"

# Reset history
!history clear
!history load /tmp/ba_cli.history

!amx silent false
```

Note that this file is identical to `controller-host.init` from the previous section, except that the socket URI has changed to `usp:/var/run/usp/endpoint_agent_path`. Remember that `uspe` will set up its own listen sockets inside the container, where each ambiorix plugin can connect to, so this is the socket we will use for our cli application as well.

Then add a `/etc/amx/cli/controller-container.conf` file to the container system:

```json
{
    "no-logo": false,
    "no-colors": false,
    "no-self": false,
    "no-self-config": true,
    "self": {
        "no-mod-amx": false,
        "no-mod-help": false,
        "no-mod-history": false,
        "no-mod-addon": false,
        "no-mod-record": false,
        "mod-amx": {
            "no-help": false,
            "no-alias": false,
            "no-prompt": false,
            "no-variable": false,
            "no-__describe": false
        }, 
        "mod-help": {
            "no-__execute": false,
            "no-__complete": false,
            "no-__describe": false
        },
        "mod-history": {
            "no-help": false,
            "no-clear": false,
            "no-show": false,
            "no-save": false,
            "no-load": false,
            "no-__complete_load": false,
            "no-__complete_save": false,
            "no-__complete_help": false,
            "no-__describe": false
        },
        "mod-addon": {
            "no-help": false,
            "no-load": false,
            "no-remove": false,
            "no-select": false,
            "no-list": false,
            "no-__complete_load": false,
            "no-__complete_remove": false,
            "no-__complete_select": false,
            "no-__complete_help": false,
            "no-__describe": false
        },
        "mod-record": {
            "no-help": false,
            "no-start": false,
            "no-stop": false,
            "no-play": false,
            "no-__complete_start": false,
            "no-__complete_play": false,
            "no-__complete_help": false,
            "no-__describe": false
        }
    },
    "usp": {
        "EndpointID": "proto::controller-container"
    }
}
```

This is again identical to the `controller-host.conf` file, except that we chose a different EndpointID.

Finally add a symbolic link `/usr/bin/controller-container -> amx-cli` inside the container system such that the init and conf file will be loaded when we run `controller-container`.

Check that `tr181-localagent`, `uspagent` and `phonebook` are still running on the host system and that `uspe` and `greeter` are still running in the container system. Then you can start `controller-container` and you should first take a look at the LocalAgent.Controller data model:

```
LocalAgent.Controller.2.Alias="cpe-Controller-2"
LocalAgent.Controller.2.EndpointID="proto::controller-container"
LocalAgent.Controller.2.MTP.1.Protocol="UDS"
```

As you can see, a Controller instance was automatically added to the data model with the EndpointID that was specified in the config file. This is a convenience feature we have enabled for `uspe`, because each ambiorix plugin still needs to be represented by a Controller instance in the data model if it wants to receive USP notifications. Note that we can only do this inside the container, because we know that every process inside the container that connects to `/var/run/usp/endpoint_agent_path` is trusted. On the host side, we cannot do this, because the socket is shared with other containers that may be untrusted.

> NOTE  
> A future improvement would be to let the `uspagent` set up multiple sockets: one that is shared with the containers and one that is local to the host system. If every ambiorix process connects to the one local on the host system, we can also automatically add Controller instances for them.

We can now again subscribe to the `Greeter.` data model, which is local from the container point of view.

```
root - usp:/var/run/usp/endpoint_agent_path - [pcb-cli] (0)
 > Greeter.?&
Added subscription for Greeter.
```

This will add a `LocalAgent.Subscription.` instance in the container data model.

```
root - usp:/var/run/usp/endpoint_agent_path - [pcb-cli] (0)
 > LocalAgent.Subscription.?
LocalAgent.Subscription.
LocalAgent.Subscription.1.
LocalAgent.Subscription.1.Alias="cpe-Subscription-1"
LocalAgent.Subscription.1.CreationDate="0001-01-01T00:00:00Z"
LocalAgent.Subscription.1.Enable="true"
LocalAgent.Subscription.1.ID="cpe-ID-1"
LocalAgent.Subscription.1.NotifExpiration="0"
LocalAgent.Subscription.1.NotifRetry="false"
LocalAgent.Subscription.1.NotifType="AmxNotification"
LocalAgent.Subscription.1.Persistent="false"
LocalAgent.Subscription.1.Recipient="LocalAgent.Controller.2."
LocalAgent.Subscription.1.ReferenceList="Greeter."
LocalAgent.Subscription.1.TimeToLive="0"
```

Updating a parameter will show the ambiorix events.

```
root - usp:/var/run/usp/endpoint_agent_path - [pcb-cli] (0)
 > Greeter.State=Start
Greeter.
Greeter.State="Start"

<  dm:object-changed> Greeter.State = Idle -> Start

<  dm:object-changed> Greeter.State = Start -> Running
```

We can also subscribe to the remote `Phonebook.` data model

```
root - usp:/var/run/usp/endpoint_agent_path - [pcb-cli] (0)
 > Phonebook.?&
Added subscription for Phonebook.
```

which will add another instance to the `LocalAgent.Subscription.` data model.

```
LocalAgent.Subscription.2.
LocalAgent.Subscription.2.Alias="cpe-Subscription-2"
LocalAgent.Subscription.2.CreationDate="0001-01-01T00:00:00Z"
LocalAgent.Subscription.2.Enable="true"
LocalAgent.Subscription.2.ID="cpe-ID-2"
LocalAgent.Subscription.2.NotifExpiration="0"
LocalAgent.Subscription.2.NotifRetry="false"
LocalAgent.Subscription.2.NotifType="AmxNotification"
LocalAgent.Subscription.2.Persistent="false"
LocalAgent.Subscription.2.Recipient="LocalAgent.Controller.3."
LocalAgent.Subscription.2.ReferenceList="Phonebook."
LocalAgent.Subscription.2.TimeToLive="0"
```

and also an instance in the data model of the host system with the `Recipient` set to the `uspe` Controller

```
user - pcb:/var/run/pcb_sys - [pcb-cli] (0)
 > LocalAgent.Subscription.?
LocalAgent.Subscription.
LocalAgent.Subscription.1.
LocalAgent.Subscription.1.Alias="cpe-Subscription-1"
LocalAgent.Subscription.1.CreationDate="0001-01-01T00:00:00Z"
LocalAgent.Subscription.1.Enable=true
LocalAgent.Subscription.1.ID="cpe-ID-2"
LocalAgent.Subscription.1.LastValue=""
LocalAgent.Subscription.1.NotifExpiration=0
LocalAgent.Subscription.1.NotifRetry=false
LocalAgent.Subscription.1.NotifType="AmxNotification"
LocalAgent.Subscription.1.Persistent=false
LocalAgent.Subscription.1.Recipient="LocalAgent.Controller.1."
LocalAgent.Subscription.1.ReferenceList="Phonebook."
LocalAgent.Subscription.1.TimeToLive=0

user - pcb:/var/run/pcb_sys - [pcb-cli] (0)
 > LocalAgent.Controller.1.?0
LocalAgent.Controller.1.
LocalAgent.Controller.1.Alias="uspe"
LocalAgent.Controller.1.AssignedRole="Device.LocalAgent.ControllerTrust.Role.2"
LocalAgent.Controller.1.ControllerCode=""
LocalAgent.Controller.1.Credential=""
LocalAgent.Controller.1.Enable=true
LocalAgent.Controller.1.EndpointID="proto::uspe"
LocalAgent.Controller.1.InheritedRole=""
LocalAgent.Controller.1.MTPNumberOfEntries=1
LocalAgent.Controller.1.PeriodicNotifInterval=60
LocalAgent.Controller.1.PeriodicNotifTime="0001-01-01T00:00:00Z"
LocalAgent.Controller.1.ProvisioningCode=""
LocalAgent.Controller.1.Status="Up"
LocalAgent.Controller.1.USPNotifRetryIntervalMultiplier=2000
LocalAgent.Controller.1.USPNotifRetryMinimumWaitInterval=5
```

Creating a new instance will again show the ambiorix events

```
root - usp:/var/run/usp/endpoint_agent_path - [pcb-cli] (0)
 > Phonebook.Contact.+{FirstName = "Jack", LastName = "Sparrow"}
Phonebook.Contact.2.

<  dm:instance-added> Phonebook.Contact.2.LastName = Sparrow
<                   > Phonebook.Contact.2.FirstName = Jack

<    dm:object-added> Phonebook.Contact.2.PhoneNumber.

<    dm:object-added> Phonebook.Contact.2.E-Mail.
```

Finally notice that the subscriptions are cleaned up again when exiting from the cli application

```
user - pcb:/var/run/pcb_sys - [pcb-cli] (0)
 > LocalAgent.Subscription.?
No data found
```

## Full data model dumps

Below are the data model dumps for the 3 systems. Note that a local broker was used instead of a public broker.

On the cloud system:
```
MQTT.Client.4.
MQTT.Client.4.Alias="cpe-mqtt-uspc"
MQTT.Client.4.BrokerAddress="sah2mqtt"
MQTT.Client.4.BrokerPort=1883
MQTT.Client.4.CleanSession=true
MQTT.Client.4.CleanStart=true
MQTT.Client.4.ClientID="hmq_8cxeq_34676572_e799a6e11df150d80100b83ff9b69df4"
MQTT.Client.4.ConnectRetryIntervalMultiplier=2000
MQTT.Client.4.ConnectRetryMaxInterval=5120
MQTT.Client.4.ConnectRetryTime=5
MQTT.Client.4.Enable=true
MQTT.Client.4.ForceReconnect=false
MQTT.Client.4.Interface=""
MQTT.Client.4.KeepAliveTime=60
MQTT.Client.4.MessageRetryTime=5
MQTT.Client.4.Name="mtp-mqtt-controller"
MQTT.Client.4.Password="xxx"
MQTT.Client.4.ProtocolVersion="5.0"
MQTT.Client.4.ResponseInformation=""
MQTT.Client.4.Status="Connected"
MQTT.Client.4.SubscriptionNumberOfEntries=1
MQTT.Client.4.TransportProtocol="TCP/IP"
MQTT.Client.4.Username="xxx"
MQTT.Client.4.Stats.
MQTT.Client.4.Stats.BrokerConnectionEstablished="2023-03-23T14:50:10.267680592Z"
MQTT.Client.4.Stats.ConnectionErrors=0
MQTT.Client.4.Stats.LastPublishMessageReceived="0001-01-01T00:00:00Z"
MQTT.Client.4.Stats.LastPublishMessageSent="0001-01-01T00:00:00Z"
MQTT.Client.4.Stats.MQTTMessagesReceived=0
MQTT.Client.4.Stats.MQTTMessagesSent=0
MQTT.Client.4.Stats.PublishErrors=0
MQTT.Client.4.Stats.PublishReceived=32
MQTT.Client.4.Stats.PublishSent=0
MQTT.Client.4.Stats.SubscribeSent=6
MQTT.Client.4.Stats.UnSubscribeSent=0
MQTT.Client.4.Subscription.1.
MQTT.Client.4.Subscription.1.Alias="cpe-Subscription-1"
MQTT.Client.4.Subscription.1.Enable=true
MQTT.Client.4.Subscription.1.QoS=0
MQTT.Client.4.Subscription.1.Status="Subscribed"
MQTT.Client.4.Subscription.1.Topic="controller/proto::Controller-a9bf597211ac"

LocalController.
LocalController.AgentNumberOfEntries=0
LocalController.Enable=true
LocalController.EndpointID="proto::Controller-a9bf597211ac"
LocalController.MTPNumberOfEntries=1
LocalController.SoftwareVersion="1.0.8"
LocalController.SupportedProtocols="MQTT,IMTP"
LocalController.UpTime=138867
LocalController.MTP.1.
LocalController.MTP.1.Alias="mtp-mqtt-controller"
LocalController.MTP.1.Enable=true
LocalController.MTP.1.Error=""
LocalController.MTP.1.Protocol="MQTT"
LocalController.MTP.1.Status="Up"
LocalController.MTP.1.MQTT.
LocalController.MTP.1.MQTT.PublishQoS=0
LocalController.MTP.1.MQTT.Reference="MQTT.Client.cpe-mqtt-uspc."
LocalController.MTP.1.MQTT.ResponseTopicConfigured="controller/proto::Controller-a9bf597211ac"
LocalController.MTP.1.MQTT.ResponseTopicDiscovered=""
```

On the host system:
```
MQTT.Client.1.
MQTT.Client.1.Alias="cpe-default"
MQTT.Client.1.BrokerAddress="sah2mqtt"
MQTT.Client.1.BrokerPort=1883
MQTT.Client.1.CleanSession=true
MQTT.Client.1.CleanStart=true
MQTT.Client.1.ClientID="B36D4718-C7F8-69DB-07B5-5CA5C65274C9"
MQTT.Client.1.ConnectRetryIntervalMultiplier=2000
MQTT.Client.1.ConnectRetryMaxInterval=5120
MQTT.Client.1.ConnectRetryTime=5
MQTT.Client.1.Enable=true
MQTT.Client.1.ForceReconnect=false
MQTT.Client.1.Interface=""
MQTT.Client.1.KeepAliveTime=60
MQTT.Client.1.MessageRetryTime=5
MQTT.Client.1.Name=""
MQTT.Client.1.Password="xxx"
MQTT.Client.1.ProtocolVersion="5.0"
MQTT.Client.1.ResponseInformation=""
MQTT.Client.1.Status="Connected"
MQTT.Client.1.SubscriptionNumberOfEntries=2
MQTT.Client.1.TransportProtocol="TCP/IP"
MQTT.Client.1.Username="xxx"
MQTT.Client.1.Stats.
MQTT.Client.1.Stats.BrokerConnectionEstablished="2023-03-23T08:58:21.645347677Z"
MQTT.Client.1.Stats.ConnectionErrors=0
MQTT.Client.1.Stats.LastPublishMessageReceived="0001-01-01T00:00:00Z"
MQTT.Client.1.Stats.LastPublishMessageSent="0001-01-01T00:00:00Z"
MQTT.Client.1.Stats.MQTTMessagesReceived=0
MQTT.Client.1.Stats.MQTTMessagesSent=0
MQTT.Client.1.Stats.PublishErrors=0
MQTT.Client.1.Stats.PublishReceived=6
MQTT.Client.1.Stats.PublishSent=0
MQTT.Client.1.Stats.SubscribeSent=1
MQTT.Client.1.Stats.UnSubscribeSent=0
MQTT.Client.1.Subscription.1.
MQTT.Client.1.Subscription.1.Alias="cpe-Subscription-1"
MQTT.Client.1.Subscription.1.Enable=true
MQTT.Client.1.Subscription.1.QoS=0
MQTT.Client.1.Subscription.1.Status="Subscribed"
MQTT.Client.1.Subscription.1.Topic="agent/proto::Agent-748cb38f0275"
MQTT.Client.1.UserProperty.1.
MQTT.Client.1.UserProperty.1.Alias="cpe-UserProperty-1"
MQTT.Client.1.UserProperty.1.Enable=true
MQTT.Client.1.UserProperty.1.Name="usp-endpoint-id"
MQTT.Client.1.UserProperty.1.PacketType="CONNECT"
MQTT.Client.1.UserProperty.1.Value="proto::Agent-748cb38f0275"

LocalAgent.
LocalAgent.AdvertisedDeviceSubtypes=""
LocalAgent.CertificateNumberOfEntries="0"
LocalAgent.ControllerNumberOfEntries="3"
LocalAgent.EndpointID="proto::Agent-748cb38f0275"
LocalAgent.MTPNumberOfEntries="2"
LocalAgent.MaxSubscriptionChangeAdoptionTime="0"
LocalAgent.RequestNumberOfEntries="0"
LocalAgent.SoftwareVersion="dev_standardize_imtp"
LocalAgent.SubscriptionNumberOfEntries="3"
LocalAgent.SupportedFingerprintAlgorithms=""
LocalAgent.SupportedProtocols="MQTT,UDS"
LocalAgent.SupportedThresholdOperator="Rise,Fall,Eq,NotEq"
LocalAgent.ThresholdNumberOfEntries="0"
LocalAgent.UpTime="157801"
LocalAgent.Controller.1.
LocalAgent.Controller.1.Alias="uspe"
LocalAgent.Controller.1.AssignedRole="Device.LocalAgent.ControllerTrust.Role.2"
LocalAgent.Controller.1.ControllerCode=""
LocalAgent.Controller.1.Credential=""
LocalAgent.Controller.1.Enable="1"
LocalAgent.Controller.1.EndpointID="proto::uspe"
LocalAgent.Controller.1.InheritedRole=""
LocalAgent.Controller.1.MTPNumberOfEntries="1"
LocalAgent.Controller.1.PeriodicNotifInterval="60"
LocalAgent.Controller.1.PeriodicNotifTime="0001-01-01T00:00:00Z"
LocalAgent.Controller.1.ProvisioningCode=""
LocalAgent.Controller.1.USPNotifRetryIntervalMultiplier="2000"
LocalAgent.Controller.1.USPNotifRetryMinimumWaitInterval="5"
LocalAgent.Controller.1.X_SOFTATHOME-COM_SendOnBoardRequest="0"
LocalAgent.Controller.1.E2ESession.
LocalAgent.Controller.1.E2ESession.CurrentRetryCount="0"
LocalAgent.Controller.1.E2ESession.Enable="0"
LocalAgent.Controller.1.E2ESession.MaxRetransmitTries="0"
LocalAgent.Controller.1.E2ESession.PayloadSecurity="TLS"
LocalAgent.Controller.1.E2ESession.SegmentedPayloadChunkSize="0"
LocalAgent.Controller.1.E2ESession.SessionExpiration="0"
LocalAgent.Controller.1.E2ESession.SessionRetryIntervalMultiplier="2000"
LocalAgent.Controller.1.E2ESession.SessionRetryMinimumWaitInterval="5"
LocalAgent.Controller.1.E2ESession.Status=""
LocalAgent.Controller.1.MTP.1.
LocalAgent.Controller.1.MTP.1.Alias="mtp-uds"
LocalAgent.Controller.1.MTP.1.Enable="1"
LocalAgent.Controller.1.MTP.1.Protocol="UDS"
LocalAgent.Controller.1.MTP.1.MQTT.
LocalAgent.Controller.1.MTP.1.MQTT.AgentMTPReference=""
LocalAgent.Controller.1.MTP.1.MQTT.Topic=""
LocalAgent.Controller.1.MTP.1.UDS.
LocalAgent.Controller.1.MTP.1.UDS.USPServiceRef=""
LocalAgent.Controller.1.MTP.1.UDS.UnixDomainSocketRef=""
LocalAgent.Controller.1.TransferCompletePolicy.
LocalAgent.Controller.1.TransferCompletePolicy.ResultTypeFilter=""
LocalAgent.Controller.2.
LocalAgent.Controller.2.Alias="controller-host"
LocalAgent.Controller.2.AssignedRole="Device.LocalAgent.ControllerTrust.Role.2"
LocalAgent.Controller.2.ControllerCode=""
LocalAgent.Controller.2.Credential=""
LocalAgent.Controller.2.Enable="1"
LocalAgent.Controller.2.EndpointID="proto::controller-host"
LocalAgent.Controller.2.InheritedRole=""
LocalAgent.Controller.2.MTPNumberOfEntries="1"
LocalAgent.Controller.2.PeriodicNotifInterval="60"
LocalAgent.Controller.2.PeriodicNotifTime="0001-01-01T00:00:00Z"
LocalAgent.Controller.2.ProvisioningCode=""
LocalAgent.Controller.2.USPNotifRetryIntervalMultiplier="2000"
LocalAgent.Controller.2.USPNotifRetryMinimumWaitInterval="5"
LocalAgent.Controller.2.X_SOFTATHOME-COM_SendOnBoardRequest="0"
LocalAgent.Controller.2.E2ESession.
LocalAgent.Controller.2.E2ESession.CurrentRetryCount="0"
LocalAgent.Controller.2.E2ESession.Enable="0"
LocalAgent.Controller.2.E2ESession.MaxRetransmitTries="0"
LocalAgent.Controller.2.E2ESession.PayloadSecurity="TLS"
LocalAgent.Controller.2.E2ESession.SegmentedPayloadChunkSize="0"
LocalAgent.Controller.2.E2ESession.SessionExpiration="0"
LocalAgent.Controller.2.E2ESession.SessionRetryIntervalMultiplier="2000"
LocalAgent.Controller.2.E2ESession.SessionRetryMinimumWaitInterval="5"
LocalAgent.Controller.2.E2ESession.Status=""
LocalAgent.Controller.2.MTP.1.
LocalAgent.Controller.2.MTP.1.Alias="mtp-uds"
LocalAgent.Controller.2.MTP.1.Enable="1"
LocalAgent.Controller.2.MTP.1.Protocol="UDS"
LocalAgent.Controller.2.MTP.1.MQTT.
LocalAgent.Controller.2.MTP.1.MQTT.AgentMTPReference=""
LocalAgent.Controller.2.MTP.1.MQTT.Topic=""
LocalAgent.Controller.2.MTP.1.UDS.
LocalAgent.Controller.2.MTP.1.UDS.USPServiceRef=""
LocalAgent.Controller.2.MTP.1.UDS.UnixDomainSocketRef=""
LocalAgent.Controller.2.TransferCompletePolicy.
LocalAgent.Controller.2.TransferCompletePolicy.ResultTypeFilter=""
LocalAgent.Controller.3.
LocalAgent.Controller.3.Alias="controller-cloud"
LocalAgent.Controller.3.AssignedRole="LocalAgent.ControllerTrust.Role.2"
LocalAgent.Controller.3.ControllerCode=""
LocalAgent.Controller.3.Credential=""
LocalAgent.Controller.3.Enable="1"
LocalAgent.Controller.3.EndpointID="proto::Controller-a9bf597211ac"
LocalAgent.Controller.3.InheritedRole=""
LocalAgent.Controller.3.MTPNumberOfEntries="1"
LocalAgent.Controller.3.PeriodicNotifInterval="60"
LocalAgent.Controller.3.PeriodicNotifTime="0001-01-01T00:00:00Z"
LocalAgent.Controller.3.ProvisioningCode=""
LocalAgent.Controller.3.USPNotifRetryIntervalMultiplier="2000"
LocalAgent.Controller.3.USPNotifRetryMinimumWaitInterval="5"
LocalAgent.Controller.3.X_SOFTATHOME-COM_SendOnBoardRequest="0"
LocalAgent.Controller.3.E2ESession.
LocalAgent.Controller.3.E2ESession.CurrentRetryCount="0"
LocalAgent.Controller.3.E2ESession.Enable="0"
LocalAgent.Controller.3.E2ESession.MaxRetransmitTries="0"
LocalAgent.Controller.3.E2ESession.PayloadSecurity="TLS"
LocalAgent.Controller.3.E2ESession.SegmentedPayloadChunkSize="0"
LocalAgent.Controller.3.E2ESession.SessionExpiration="0"
LocalAgent.Controller.3.E2ESession.SessionRetryIntervalMultiplier="2000"
LocalAgent.Controller.3.E2ESession.SessionRetryMinimumWaitInterval="5"
LocalAgent.Controller.3.E2ESession.Status=""
LocalAgent.Controller.3.MTP.1.
LocalAgent.Controller.3.MTP.1.Alias="mtp-mqtt"
LocalAgent.Controller.3.MTP.1.Enable="1"
LocalAgent.Controller.3.MTP.1.Protocol="MQTT"
LocalAgent.Controller.3.MTP.1.MQTT.
LocalAgent.Controller.3.MTP.1.MQTT.AgentMTPReference="LocalAgent.MTP.2."
LocalAgent.Controller.3.MTP.1.MQTT.Topic="controller/proto::Controller-a9bf597211ac"
LocalAgent.Controller.3.MTP.1.UDS.
LocalAgent.Controller.3.MTP.1.UDS.USPServiceRef=""
LocalAgent.Controller.3.MTP.1.UDS.UnixDomainSocketRef=""
LocalAgent.Controller.3.TransferCompletePolicy.
LocalAgent.Controller.3.TransferCompletePolicy.ResultTypeFilter=""
LocalAgent.ControllerTrust.
LocalAgent.ControllerTrust.BannedRole=""
LocalAgent.ControllerTrust.ChallengeNumberOfEntries="0"
LocalAgent.ControllerTrust.CredentialNumberOfEntries="0"
LocalAgent.ControllerTrust.RoleNumberOfEntries="4"
LocalAgent.ControllerTrust.TOFUAllowed="0"
LocalAgent.ControllerTrust.TOFUInactivityTimer="0"
LocalAgent.ControllerTrust.UntrustedRole="Device.LocalAgent.ControllerTrust.Role.1"
LocalAgent.ControllerTrust.Role.1.
LocalAgent.ControllerTrust.Role.1.Alias="untrusted"
LocalAgent.ControllerTrust.Role.1.Enable="1"
LocalAgent.ControllerTrust.Role.1.Name="untrusted"
LocalAgent.ControllerTrust.Role.1.PermissionNumberOfEntries="0"
LocalAgent.ControllerTrust.Role.2.
LocalAgent.ControllerTrust.Role.2.Alias="operator"
LocalAgent.ControllerTrust.Role.2.Enable="1"
LocalAgent.ControllerTrust.Role.2.Name="operator"
LocalAgent.ControllerTrust.Role.2.PermissionNumberOfEntries="1"
LocalAgent.ControllerTrust.Role.2.Permission.1.
LocalAgent.ControllerTrust.Role.2.Permission.1.Alias="pre-mapper-defaults"
LocalAgent.ControllerTrust.Role.2.Permission.1.CommandEvent="rwxn"
LocalAgent.ControllerTrust.Role.2.Permission.1.Enable="1"
LocalAgent.ControllerTrust.Role.2.Permission.1.InstantiatedObj="rwxn"
LocalAgent.ControllerTrust.Role.2.Permission.1.Obj="rwxn"
LocalAgent.ControllerTrust.Role.2.Permission.1.Order="1"
LocalAgent.ControllerTrust.Role.2.Permission.1.Param="rwxn"
LocalAgent.ControllerTrust.Role.2.Permission.1.Targets="Services.,DeviceInfo.,Time.,UserInterface.,InterfaceStack.,DSL.,FAST.,Optical.,Cellular.,ATM.,PTM.,Ethernet.,USB.,HPNA.,MoCA.,Ghn.,HomePlug.,UPA.,WiFi.,ZigBee.,Bridging.,PPP.,IP.,LLDP.,IPsec.,GRE.,L2TPv3.,VXLAN.,MAP.,CaptivePortal.,Routing.,Routing.,NeighborDiscovery.,RouterAdvertisement.,IPv6rd.,DSLite.,QoS.,LANConfigSecurity.,Hosts.,DNS.,NAT.,PCP.,DHCPv4.,DHCPv6.,IEEE8021x.,Users.,SmartCardReaders.,UPnP.,DLNA.,Firewall.,PeriodicStatistics.,FaultMgmt.,Security.,FAP.,BulkData.,LocalAgent.,STOMP.,Standby.,XMPP.,IEEE1905.,MQTT.,DynamicDNS.,LEDs.,BASAPM.,LMAP.,WWC.,PDU.,FWE.,SoftwareModules.,ProxiedDevice.,IoTCapability.,Node.,Phonebook."
LocalAgent.ControllerTrust.Role.3.
LocalAgent.ControllerTrust.Role.3.Alias="admin"
LocalAgent.ControllerTrust.Role.3.Enable="1"
LocalAgent.ControllerTrust.Role.3.Name="admin"
LocalAgent.ControllerTrust.Role.3.PermissionNumberOfEntries="0"
LocalAgent.ControllerTrust.Role.4.
LocalAgent.ControllerTrust.Role.4.Alias="guest"
LocalAgent.ControllerTrust.Role.4.Enable="1"
LocalAgent.ControllerTrust.Role.4.Name="guest"
LocalAgent.ControllerTrust.Role.4.PermissionNumberOfEntries="0"
LocalAgent.MTP.1.
LocalAgent.MTP.1.Alias="mtp-uds"
LocalAgent.MTP.1.Enable="1"
LocalAgent.MTP.1.EnableMDNS="1"
LocalAgent.MTP.1.Protocol="UDS"
LocalAgent.MTP.1.Status="Up"
LocalAgent.MTP.1.MQTT.
LocalAgent.MTP.1.MQTT.PublishQoS="0"
LocalAgent.MTP.1.MQTT.Reference=""
LocalAgent.MTP.1.MQTT.ResponseTopicConfigured=""
LocalAgent.MTP.1.MQTT.ResponseTopicDiscovered=""
LocalAgent.MTP.1.MQTT.X_SOFTATHOME-COM_AutoReconnect="0"
LocalAgent.MTP.1.UDS.
LocalAgent.MTP.1.UDS.UnixDomainSocketRef="Device.UnixDomainSockets.UnixDomainSocket.1"
LocalAgent.MTP.2.
LocalAgent.MTP.2.Alias="mtp-mqtt"
LocalAgent.MTP.2.Enable="1"
LocalAgent.MTP.2.EnableMDNS="1"
LocalAgent.MTP.2.Protocol="MQTT"
LocalAgent.MTP.2.Status="Up"
LocalAgent.MTP.2.MQTT.
LocalAgent.MTP.2.MQTT.PublishQoS="0"
LocalAgent.MTP.2.MQTT.Reference="MQTT.Client.1"
LocalAgent.MTP.2.MQTT.ResponseTopicConfigured="agent/proto::Agent-748cb38f0275"
LocalAgent.MTP.2.MQTT.ResponseTopicDiscovered=""
LocalAgent.MTP.2.MQTT.X_SOFTATHOME-COM_AutoReconnect="0"
LocalAgent.MTP.2.UDS.
LocalAgent.MTP.2.UDS.UnixDomainSocketRef=""
LocalAgent.Subscription.11.
LocalAgent.Subscription.11.Alias="cpe-Subscription-11"
LocalAgent.Subscription.11.CreationDate="0001-01-01T00:00:00Z"
LocalAgent.Subscription.11.Enable="1"
LocalAgent.Subscription.11.ID="cpe-ID-11"
LocalAgent.Subscription.11.NotifExpiration="0"
LocalAgent.Subscription.11.NotifRetry="0"
LocalAgent.Subscription.11.NotifType="AmxNotification"
LocalAgent.Subscription.11.Persistent="0"
LocalAgent.Subscription.11.Recipient="LocalAgent.Controller.2."
LocalAgent.Subscription.11.ReferenceList="Greeter."
LocalAgent.Subscription.11.TimeToLive="0"
LocalAgent.Subscription.12.
LocalAgent.Subscription.12.Alias="cpe-Subscription-12"
LocalAgent.Subscription.12.CreationDate="0001-01-01T00:00:00Z"
LocalAgent.Subscription.12.Enable="1"
LocalAgent.Subscription.12.ID="cpe-ID-3"
LocalAgent.Subscription.12.NotifExpiration="0"
LocalAgent.Subscription.12.NotifRetry="0"
LocalAgent.Subscription.12.NotifType="AmxNotification"
LocalAgent.Subscription.12.Persistent="0"
LocalAgent.Subscription.12.Recipient="LocalAgent.Controller.1."
LocalAgent.Subscription.12.ReferenceList="Phonebook."
LocalAgent.Subscription.12.TimeToLive="0"
LocalAgent.Subscription.13.
LocalAgent.Subscription.13.Alias="cpe-Subscription-13"
LocalAgent.Subscription.13.CreationDate="0001-01-01T00:00:00Z"
LocalAgent.Subscription.13.Enable="1"
LocalAgent.Subscription.13.ID="cpe-ID-13"
LocalAgent.Subscription.13.NotifExpiration="0"
LocalAgent.Subscription.13.NotifRetry="0"
LocalAgent.Subscription.13.NotifType="AmxNotification"
LocalAgent.Subscription.13.Persistent="0"
LocalAgent.Subscription.13.Recipient="LocalAgent.Controller.2."
LocalAgent.Subscription.13.ReferenceList="Phonebook."
LocalAgent.Subscription.13.TimeToLive="0"

Discovery.
Discovery.ServiceNumberOfEntries=1
Discovery.Service.
Discovery.Service.3.
Discovery.Service.3.Alias="cpe-Service-3"
Discovery.Service.3.EndpointID="proto::uspe"
Discovery.Service.3.RegistrationNumberOfEntries=1
Discovery.Service.3.Registration.
Discovery.Service.3.Registration.1.
Discovery.Service.3.Registration.1.Path="Greeter."

Phonebook.
Phonebook.Contact.
Phonebook.Contact.3.
Phonebook.Contact.3.FirstName="Jack"
Phonebook.Contact.3.LastName="Sparrow"
```

On the container system:
```
LocalAgent.
LocalAgent.AdvertisedDeviceSubtypes=""
LocalAgent.CertificateNumberOfEntries="0"
LocalAgent.ControllerNumberOfEntries="3"
LocalAgent.Enable="true"
LocalAgent.EndpointID="proto::uspe"
LocalAgent.MTPNumberOfEntries="2"
LocalAgent.MaxSubscriptionChangeAdoptionTime="0"
LocalAgent.RequestNumberOfEntries="0"
LocalAgent.SoftwareVersion="1.2.3"
LocalAgent.SubscriptionNumberOfEntries="2"
LocalAgent.SupportedFingerprintAlgorithms=""
LocalAgent.SupportedProtocols="MQTT,UDS"
LocalAgent.SupportedThresholdOperator="Rise,Fall,Eq,NotEq"
LocalAgent.UpTime="283"
LocalAgent.Certificate.
LocalAgent.Controller.
LocalAgent.Controller.1.
LocalAgent.Controller.1.Alias="controller-host"
LocalAgent.Controller.1.AssignedRole=""
LocalAgent.Controller.1.ControllerCode=""
LocalAgent.Controller.1.Credential=""
LocalAgent.Controller.1.Enable="true"
LocalAgent.Controller.1.EndpointID="proto::Agent-748cb38f0275"
LocalAgent.Controller.1.InheritedRole=""
LocalAgent.Controller.1.PeriodicNotifInterval="0"
LocalAgent.Controller.1.PeriodicNotifTime="0001-01-01T00:00:00Z"
LocalAgent.Controller.1.ProvisioningCode=""
LocalAgent.Controller.1.USPNotifRetryIntervalMultiplier="2000"
LocalAgent.Controller.1.USPNotifRetryMinimumWaitInterval="5"
LocalAgent.Controller.1.BootParameter.
LocalAgent.Controller.1.E2ESession.
LocalAgent.Controller.1.E2ESession.CurrentRetryCount="0"
LocalAgent.Controller.1.E2ESession.Enable="false"
LocalAgent.Controller.1.E2ESession.MaxRetransmitTries="0"
LocalAgent.Controller.1.E2ESession.PayloadSecurity="TLS"
LocalAgent.Controller.1.E2ESession.SegmentedPayloadChunkSize="0"
LocalAgent.Controller.1.E2ESession.SessionExpiration="0"
LocalAgent.Controller.1.E2ESession.SessionRetryIntervalMultiplier="2000"
LocalAgent.Controller.1.E2ESession.SessionRetryMinimumWaitInterval="5"
LocalAgent.Controller.1.E2ESession.Status=""
LocalAgent.Controller.1.MTP.
LocalAgent.Controller.1.MTP.1.
LocalAgent.Controller.1.MTP.1.Alias=""
LocalAgent.Controller.1.MTP.1.Enable="true"
LocalAgent.Controller.1.MTP.1.Protocol="UDS"
LocalAgent.Controller.1.MTP.1.MQTT.
LocalAgent.Controller.1.MTP.1.MQTT.AgentMTPReference=""
LocalAgent.Controller.1.MTP.1.MQTT.Topic=""
LocalAgent.Controller.1.MTP.1.UDS.
LocalAgent.Controller.1.MTP.1.UDS.USPServiceRef=""
LocalAgent.Controller.1.MTP.1.UDS.UnixDomainSocketRef=""
LocalAgent.Controller.1.TransferCompletePolicy.
LocalAgent.Controller.1.TransferCompletePolicy.ResultTypeFilter=""
LocalAgent.Controller.2.
LocalAgent.Controller.2.Alias="cpe-Controller-2"
LocalAgent.Controller.2.AssignedRole=""
LocalAgent.Controller.2.ControllerCode=""
LocalAgent.Controller.2.Credential=""
LocalAgent.Controller.2.Enable="true"
LocalAgent.Controller.2.EndpointID="proto::greeter"
LocalAgent.Controller.2.InheritedRole=""
LocalAgent.Controller.2.PeriodicNotifInterval="0"
LocalAgent.Controller.2.PeriodicNotifTime="0001-01-01T00:00:00Z"
LocalAgent.Controller.2.ProvisioningCode=""
LocalAgent.Controller.2.USPNotifRetryIntervalMultiplier="2000"
LocalAgent.Controller.2.USPNotifRetryMinimumWaitInterval="5"
LocalAgent.Controller.2.BootParameter.
LocalAgent.Controller.2.E2ESession.
LocalAgent.Controller.2.E2ESession.CurrentRetryCount="0"
LocalAgent.Controller.2.E2ESession.Enable="false"
LocalAgent.Controller.2.E2ESession.MaxRetransmitTries="0"
LocalAgent.Controller.2.E2ESession.PayloadSecurity="TLS"
LocalAgent.Controller.2.E2ESession.SegmentedPayloadChunkSize="0"
LocalAgent.Controller.2.E2ESession.SessionExpiration="0"
LocalAgent.Controller.2.E2ESession.SessionRetryIntervalMultiplier="2000"
LocalAgent.Controller.2.E2ESession.SessionRetryMinimumWaitInterval="5"
LocalAgent.Controller.2.E2ESession.Status=""
LocalAgent.Controller.2.MTP.
LocalAgent.Controller.2.MTP.1.
LocalAgent.Controller.2.MTP.1.Alias=""
LocalAgent.Controller.2.MTP.1.Enable="true"
LocalAgent.Controller.2.MTP.1.Protocol="UDS"
LocalAgent.Controller.2.MTP.1.MQTT.
LocalAgent.Controller.2.MTP.1.MQTT.AgentMTPReference=""
LocalAgent.Controller.2.MTP.1.MQTT.Topic=""
LocalAgent.Controller.2.MTP.1.UDS.
LocalAgent.Controller.2.MTP.1.UDS.USPServiceRef=""
LocalAgent.Controller.2.MTP.1.UDS.UnixDomainSocketRef=""
LocalAgent.Controller.2.TransferCompletePolicy.
LocalAgent.Controller.2.TransferCompletePolicy.ResultTypeFilter=""
LocalAgent.Controller.3.
LocalAgent.Controller.3.Alias="cpe-Controller-3"
LocalAgent.Controller.3.AssignedRole=""
LocalAgent.Controller.3.ControllerCode=""
LocalAgent.Controller.3.Credential=""
LocalAgent.Controller.3.Enable="true"
LocalAgent.Controller.3.EndpointID="proto::controller-container"
LocalAgent.Controller.3.InheritedRole=""
LocalAgent.Controller.3.PeriodicNotifInterval="0"
LocalAgent.Controller.3.PeriodicNotifTime="0001-01-01T00:00:00Z"
LocalAgent.Controller.3.ProvisioningCode=""
LocalAgent.Controller.3.USPNotifRetryIntervalMultiplier="2000"
LocalAgent.Controller.3.USPNotifRetryMinimumWaitInterval="5"
LocalAgent.Controller.3.BootParameter.
LocalAgent.Controller.3.E2ESession.
LocalAgent.Controller.3.E2ESession.CurrentRetryCount="0"
LocalAgent.Controller.3.E2ESession.Enable="false"
LocalAgent.Controller.3.E2ESession.MaxRetransmitTries="0"
LocalAgent.Controller.3.E2ESession.PayloadSecurity="TLS"
LocalAgent.Controller.3.E2ESession.SegmentedPayloadChunkSize="0"
LocalAgent.Controller.3.E2ESession.SessionExpiration="0"
LocalAgent.Controller.3.E2ESession.SessionRetryIntervalMultiplier="2000"
LocalAgent.Controller.3.E2ESession.SessionRetryMinimumWaitInterval="5"
LocalAgent.Controller.3.E2ESession.Status=""
LocalAgent.Controller.3.MTP.
LocalAgent.Controller.3.MTP.1.
LocalAgent.Controller.3.MTP.1.Alias=""
LocalAgent.Controller.3.MTP.1.Enable="true"
LocalAgent.Controller.3.MTP.1.Protocol="UDS"
LocalAgent.Controller.3.MTP.1.MQTT.
LocalAgent.Controller.3.MTP.1.MQTT.AgentMTPReference=""
LocalAgent.Controller.3.MTP.1.MQTT.Topic=""
LocalAgent.Controller.3.MTP.1.UDS.
LocalAgent.Controller.3.MTP.1.UDS.USPServiceRef=""
LocalAgent.Controller.3.MTP.1.UDS.UnixDomainSocketRef=""
LocalAgent.Controller.3.TransferCompletePolicy.
LocalAgent.Controller.3.TransferCompletePolicy.ResultTypeFilter=""
LocalAgent.ControllerTrust.
LocalAgent.ControllerTrust.BannedRole=""
LocalAgent.ControllerTrust.ChallengeNumberOfEntries="0"
LocalAgent.ControllerTrust.CredentialNumberOfEntries="0"
LocalAgent.ControllerTrust.RoleNumberOfEntries="0"
LocalAgent.ControllerTrust.TOFUAllowed="false"
LocalAgent.ControllerTrust.TOFUInactivityTimer="0"
LocalAgent.ControllerTrust.UntrustedRole=""
LocalAgent.ControllerTrust.Challenge.
LocalAgent.ControllerTrust.Credential.
LocalAgent.ControllerTrust.Role.
LocalAgent.MTP.
LocalAgent.MTP.1.
LocalAgent.MTP.1.Alias="mtp-uds-host"
LocalAgent.MTP.1.Enable="true"
LocalAgent.MTP.1.EnableMDNS="true"
LocalAgent.MTP.1.Error=""
LocalAgent.MTP.1.Protocol="UDS"
LocalAgent.MTP.1.Status="Up"
LocalAgent.MTP.1.UDS.
LocalAgent.MTP.1.UDS.UnixDomainSocketRef=""
LocalAgent.MTP.2.
LocalAgent.MTP.2.Alias="mtp-uds-container"
LocalAgent.MTP.2.Enable="true"
LocalAgent.MTP.2.EnableMDNS="true"
LocalAgent.MTP.2.Error=""
LocalAgent.MTP.2.Protocol="UDS"
LocalAgent.MTP.2.Status="Up"
LocalAgent.MTP.2.UDS.
LocalAgent.MTP.2.UDS.UnixDomainSocketRef=""
LocalAgent.Request.
LocalAgent.Subscription.
LocalAgent.Subscription.1.
LocalAgent.Subscription.1.Alias="cpe-Subscription-1"
LocalAgent.Subscription.1.CreationDate="0001-01-01T00:00:00Z"
LocalAgent.Subscription.1.Enable="true"
LocalAgent.Subscription.1.ID="cpe-ID-1"
LocalAgent.Subscription.1.NotifExpiration="0"
LocalAgent.Subscription.1.NotifRetry="false"
LocalAgent.Subscription.1.NotifType="AmxNotification"
LocalAgent.Subscription.1.Persistent="false"
LocalAgent.Subscription.1.Recipient="LocalAgent.Controller.3."
LocalAgent.Subscription.1.ReferenceList="Greeter."
LocalAgent.Subscription.1.TimeToLive="0"
LocalAgent.Subscription.3.
LocalAgent.Subscription.3.Alias="cpe-Subscription-3"
LocalAgent.Subscription.3.CreationDate="0001-01-01T00:00:00Z"
LocalAgent.Subscription.3.Enable="true"
LocalAgent.Subscription.3.ID="cpe-ID-3"
LocalAgent.Subscription.3.NotifExpiration="0"
LocalAgent.Subscription.3.NotifRetry="false"
LocalAgent.Subscription.3.NotifType="AmxNotification"
LocalAgent.Subscription.3.Persistent="false"
LocalAgent.Subscription.3.Recipient="LocalAgent.Controller.3."
LocalAgent.Subscription.3.ReferenceList="Phonebook."
LocalAgent.Subscription.3.TimeToLive="0"

Greeter.
Greeter.MaxHistory=10
Greeter.NumberOfHistoryEntries=3
Greeter.State="Running"
Greeter.History.1.
Greeter.History.1.From="odl parser"
Greeter.History.1.Message="Welcome to the Greeter App"
Greeter.History.1.NumberOfInfoEntries=0
Greeter.History.1.Retain=true
Greeter.History.2.
Greeter.History.2.From="me"
Greeter.History.2.Message="hello"
Greeter.History.2.NumberOfInfoEntries=0
Greeter.History.2.Retain=false
Greeter.History.3.
Greeter.History.3.From="me"
Greeter.History.3.Message="hello"
Greeter.History.3.NumberOfInfoEntries=0
Greeter.History.3.Retain=false
Greeter.Statistics.
Greeter.Statistics.AddHistoryCount=0
Greeter.Statistics.DelHistoryCount=0
Greeter.Statistics.EventCount=4
```
