# USP agent quick start guide

In this quick start guide we will show you how the data model can be configured to start communicating with your controller as soon as possible. For more detailed information, refer to the other documents in this repository or the official documentation.

In this guide we will show you how to use MQTT as the MTP to communicate with the USP agent. Let's start by adding an MQTT client.

```
MQTT.Client.+{Alias="my-client", BrokerAddress="broker.hivemq.com"}
```

Update the `Alias`, `BrokerAddress`, `BrokerPort`, `Username`, `Password`, `TransportProtocol` and any other parameters to your liking. Next we will add a Controller instance:

```
LocalAgent.Controller.+{Alias = "my-controller", EndpointID = "proto::Controller-b9bf597211ac", Enable = true, AssignedRole = "LocalAgent.ControllerTrust.Role.2."}
LocalAgent.Controller.my-controller.MTP+{Alias = "my-mtp", Enable = true, Protocol = "MQTT"}
LocalAgent.Controller.my-controller.MTP.my-mtp.MQTT.{AgentMTPReference = "LocalAgent.MTP.1.", Topic = "controller/proto::Controller-b9bf597211ac"}
```

Pay attention to the `EndpointID` that is chosen for your controller. The `AssignedRole` will correspond with the `operator` role, which has access to most data models. Finally we need to add an MTP instance for the LocalAgent itself:

```
LocalAgent.MTP.+{Alias = "my-mtp", Enable = false, Protocol = "MQTT"}
LocalAgent.MTP.my-mtp.MQTT.Reference="MQTT.Client.my-client."
LocalAgent.MTP.my-mtp.Enable=true
```

Your agent should now be connected to the MQTT broker and ready to communicate with the configured controller.
