# USP Agent

[[_TOC_]]

## Data model

The USP Agent data model is defined in the [TR-181 under Device.LocalAgent.](https://usp-data-models.broadband-forum.org/tr-181-2-15-0-usp.html#D.Device:2.Device.LocalAgent.). As you may have noticed, it cannot be found in this repository, but is instead implemented by the [tr181-localagent plugin](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent). It was moved to a separate component to avoid a lot of issues related to mapping the object under Device. Make sure `tr181-localagent` is always running when you want to use the USP agent.

## Communication setup

### MQTT

The `USP agent` can use the MQTT protocol to communicate with `USP controllers`. In order to send and receive messages using MQTT, the agent communicates with the [tr181-mqtt](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-mqtt) plugin. This plugin implements the [TR-181 MQTT data model](https://usp-data-models.broadband-forum.org/tr-181-2-14-0-usp.html#D.Device:2.Device.MQTT.).

The default MQTT clients that are used by the USP agent can be found in the mqtt client [defaults file](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent/-/blob/main/odl/mqtt-defaults/01-mqtt-client-localagent.odl). This file will be installed in the `/etc/amx/tr181-mqtt/defaults/odl/` directory on target. When the `tr181-mqtt` client starts, it will populate the data model with these clients and other possible clients that are found in the defaults directory.

USP endpoints that communicate with each other must use USP protobuf messages as explained in the [messages section of TR-369](https://usp.technology/specification/07-index-messages.html#sec:messages). When an external controller sends a protobuf message over MQTT it will arrive at the `tr181-mqtt` plugin. This plugin then needs to forward the received message to the `uspagent`. Because the `uspagent` and `tr181-mqtt` client are separate processes, they need to have a system for inter-process communication. To facilitate this message exchange, the processes communicate over the Ambiorix [USP back-end](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp). In short, the USP back-end encapsulates binary protobuf messages inside TLVs and sends these TLVs over a unix domain socket. This means that a unix domain socket connection must be established between the MQTT client and USP agent. In general you don't need to worry about this, because this connection setup will be handled automatically on startup.

The agent has an endpoint identifier, used in the USP record, to uniquely identify the agent endpoint. There are multiple authority schemes that can be used to create an id in the format 'authority-scheme ":" [authority-id] ":" instance-id'.
The authority scheme can be selected in the config section of the tr181-localagent [odl file](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent/-/blob/main/odl/tr181-localagent.odl). 
Currently supported schemes are: "oui", "proto" (default)
```text
%config {
    authority-scheme = "oui";
}
```

#### Data model configuration

The LocalAgent data model will be populated with some default values based on all files present in the [defaults folder](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent/-/tree/main/odl/defaults). It will contain one or more configured MTPs to use, for example:

```text
%populate {
    object LocalAgent {
        object MTP {
            instance add (0, "example-mtp") {
                parameter Enable = true;
                parameter Protocol = MQTT;

                object MQTT {
                    parameter Reference = "MQTT.Client.example-client.";
                }
            }
        }
    }
}
```

These settings configure an MTP instance with MQTT as the communication protocol. It is important that the Reference parameter corresponds to an existing MQTT client in the MQTT data model. This reference value will be checked by `mod-dmext` and an error will occur when the reference does not exist. For the moment a separate MTP instance and MQTT client reference must be used for each controller that you want to communicate with. This may change in the future, because having multiple identical MTP instances for different controllers doesn't always provide any extra value. Note that [TR-181 Issue 2 Amendment 15](https://usp-data-models.broadband-forum.org/tr-181-2-15-0-usp.html#D.Device:2.Device.LocalAgent.MTP.{i}.MQTT) doesn't even allow more than 1 MTP of type MQTT, which is a limitation on its own. We do not respect this limitation and allow more than 1 MTP of type MQTT to be configured, which may for example be needed if different MQTT brokers are used.

The defaults files will also contain one or more configured controllers:

```text
%populate {
    object LocalAgent {
        object Controller {
            instance add (0, "example-controller", EndpointID = "proto::example-controller") {
                parameter Enable = true;
                parameter AssignedRole = "LocalAgent.ControllerTrust.Role.1";
                object MTP {
                    instance add (0, "example-mtp", Protocol = "MQTT") {
                        parameter Enable = true;
                        object MQTT {
                            parameter AgentMTPReference = "LocalAgent.MTP.1.";
                            parameter Topic = "example-topic";
                        }
                    }
                }
            }
        }
    }
}
```

For the controller it is important that the `EndpointID` is correctly defined, because this will be used to respond to USP messages from that controller. It is also important that the controller is enabled and that it has an `AssignedRole` with sufficient permissions. The controller itself also has an MTP table with all the MTPs that can be used for contacting the controller. The configured MTP must of course be enabled. In case the MTP is MQTT there is also a `AgentMTPReference` to the LocalAgent.MTP holding the MQTT client reference. This reference will be used for communicating with the controller. The `Topic` parameter can be used to define the topic that the controller is subscribed to. This topic will be used to publish responses for the selected controller. If the Topic parameter is not set, it will be determined based on the configured controller `EndpointID`. In case the `EndpointID` is `proto::foo`, responses will be published on `controller/proto::foo`.

### UDS

When the UDS MTP is used for communicating with the USP agent, an MTP of type UDS must be added to the `LocalAgent.` data model. The details for the UDS connection can be found in [the configuration guidelines of the USP Endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint/-/blob/main/doc/mtp_imtp_configuration.md). You can refer to this document for more information. You will need to configure an MTP of type UDS in the data model and will need to have [tr181-uds](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-uds/) installed and running.

### Access control verification

The last important section in the defaults files is the ControllerTrust section. This part of the data model defines all roles that a controller can have and what their permissions are. The defaults could for example look like this:

```text
%populate {
    object LocalAgent {
        object ControllerTrust {
            parameter UntrustedRole = "LocalAgent.ControllerTrust.Role.1";
            object Role {
                instance add (0, "untrusted", Name = "untrusted") {
                    parameter Enable = true;
                }
                instance add (0, "operator", Name = "operator") {
                    parameter Enable = true;
                }
                instance add (0, "admin", Name = "admin") {
                    parameter Enable = true;
                }
                instance add (0, "guest", Name = "guest") {
                    parameter Enable = true;
                }
            }
        }
    }
}
```

Based on the USP specification, it is technically required to list all Permissions for the various roles inside the data model as well (under LocalAgent.ControllerTrust.Role.{i}.Permission.), however we decided not to do this to prevent the data model from becoming bloated. The ACLs for various roles will instead be written in ACL files that are only read when needed. It is however still possible to override Permissions for a certain role at run time by adding Permission instances to the data model. New instances will result in extra ACL files that will be merged with the existing ACL files. Just make sure that the `Order` of the Permission is high enough, such that it will override existing permissions.

Permissions listed in ACL files can target overlapping parts of the data model. To ensure that the right permissions are always used, all ACL files for a given role must be merged together to a single file. This is done by the [acl-manager](https://gitlab.com/prpl-foundation/components/ambiorix/applications/acl-manager). It is important that the `acl-manager` is always running together with the USP agent to ensure that the ACL files are merged properly.

**NOTE** If the USP agent receives a message from a controller that is not listed in LocalAgent.Controller. table, it should still handle this message and assume the controller has the UntrustedRole. This is currently not supported in our agent, because it uses the LocalAgent.Controller.{i}.MTP. information for responding to requests of controllers.

For more information regarding access control verification refer to:

- [The USP specification](https://usp.technology/specification/security/#roles_access_control)
- [The ambiorix documentation on ACLs](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxa/-/blob/main/doc/acl.md)

## Troubleshooting

### No logs are seen

There should be trace logs available in `/var/log/messages` or `/var/log/syslog` depending on your setup. The log file will probably contain a large amount of log messages from various components. To filter out all irrelevant messages and only show the USP and MQTT messages, you can use `grep`:

```bash
cat /var/log/messages | grep -e tr181-mqtt -e uspagent
```

You can also follow the logs for new messages:

```bash
tail -f /var/log/messages | grep -e tr181-mqtt -e uspagent
```

If no logs are found, make sure that mod-sahtrace is installed. Verify that the module exists with:

```
ls /etc/amx/modules/mod_sahtrace.odl
ls /usr/lib/amx/modules/mod-sahtrace.so
```

### Unable to establish connection between USP agent and MQTT client

The USP back-end connection between the USP agent and MQTT client is established by invoking the (protected) RPC `CreateListenSocket` for one of the `MQTT.Client.{i}.` instances. After the listen socket is created, the USP agent will connect to it. When the connection setup fails, you will see the following message in the trace logs:

`"Could not invoke CreateListenSocket"`

When this happens, make sure you check that the USP back-end is installed:

```bash
ls /usr/bin/mods/amxb/mod-amxb-usp.so
```

If the shared object is missing, the USP back-end is not installed. If the `.so` is present, you should verify if the directory `/var/run/mqtt/` exists, because this directory is used to create the sockets. Normally, it should be created on startup of the agent when using the startup script.

### mod-dmext is not installed

If you get the following error:

```bash
[IMPORT-DBG] - dlopen - uspagent.so (0x55b7601031b0)
[IMPORT-DBG] - file not found mod-dmext.so
ERROR  : 17 - Import file not found !!! "mod-dmext.so"@/etc/amx/uspagent/uspagent.odl:line 27
[IMPORT-DBG] - symbols used of uspagent = 0
INFO -- No ODL file(s) provided - tried to load //etc/amx/uspagent/uspagent.odl
ERROR -- Failed parsing //etc/amx/uspagent/uspagent.odl
REASON -- 
```

It means `mod-dmext` was not installed and the `uspagent` will be unable to load the module. Make sure you install it.

### Invalid MQTT client reference

`mod-dmext` will check if the defined `Reference` clients in the USP agent data model actually refer to existing MQTT client instances. If this is not the case, you will get the following error on startup of the `uspagent`:

```bash
ERROR  : 21 - invalid path - Reference@/etc/amx/uspagent/uspagent_defaults.odl:line 11
ERROR  : 1 - Error found in /etc/amx/uspagent/uspagent_defaults.odl@/etc/amx/uspagent/uspagent-storage.odl:line 1
```

### Invalid Agent MTP Reference

`mod-dmext` will check if the defined `AgentMTPReference` parameter in the USP agent data model actually refer to existing LocalAgent.MTP instances. If this is not the case, you will get the following error on startup of the `uspagent`:

```bash
ERROR  : 21 - invalid path - AgentMTPReference@/etc/amx/uspagent/uspagent_defaults.odl:line 11
ERROR  : 1 - Error found in /etc/amx/uspagent/uspagent_defaults.odl@/etc/amx/uspagent/uspagent-storage.odl:line 1
```

(your line numbers may vary)

### Unstable MQTT broker

When using an unstable MQTT broker, it will often send disconnect messages to connected MQTT clients. When a client gets disconnected, you can reconnect it by toggling the `Enable` parameter. This can be done with `pcb_cli`, `pcb-cli` or `ubus-cli` depending on your system

```bash
MQTT.Client.<your-client>.Enable=0
MQTT.Client.<your-client>.Enable=1
```

After reconnecting, check the `Status` parameter of the client and its subscription

```bash
MQTT.Client.<your-client>.Status?
MQTT.Client.<your-client>.Subscription.<your-subscription>.Status?
```

## Requests that used to work are now failing

If your USP request used to work in the past, but is now failing, it is possible that your controller has insufficient access rights. Verify the following things:

- The `acl-manager` is running
- Your controller is listed in the `LocalAgent.Controller.` data model
- Check the value of `LocalAgent.Controller.<your-controller>.AssignedRole` to see which role in `LocalAgent.ControllerTrust.Role.{i}.` it corresponds to.
- Check if there is a symbolic link from `/etc/acl/merged` to `/var/etc/acl/merged`. There has been an update to the `acl-manager` to create this symbolic link such that the `merged` directory is created in a writeable partition of the gateway. By default `/etc/` is not writable. If this symbolic link exists, make sure that the target directory `/var/etc/acl/merged` also exists. This directory will be created if you start the `acl-manager` using its init script i.e. `/etc/init.d/acl-manager start`, but it will not exist if you run the `acl-manager` from its symlink to `amxrt`, because then the init script is not run.
- Check if there is a merged ACL file for your role in `/etc/acl/merged/<your-role>.json`. 
- Make sure you are not trying to access protected parts of the data model (which is not allowed)
