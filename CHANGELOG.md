# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v5.2.2 - 2025-01-17(10:26:54 +0000)

### Other

- [USP] Failed to forward subscription to USP service

## Release v5.2.1 - 2025-01-13(15:07:50 +0000)

### Other

- [USP] installing a container with an invalid path returns error code 7003 iso 7026

## Release v5.2.0 - 2025-01-09(16:14:40 +0000)

### Other

- [USP][Del_Message] No response from the agent after sending Del message request with an invalid Object

## Release v5.1.3 - 2025-01-09(16:05:12 +0000)

### Other

- Index is missing from USP ObjectDeletion Notify message

## Release v5.1.2 - 2025-01-06(11:44:19 +0000)

### Other

- [USP Agent] USP subscription does not support wildcard

## Release v5.1.1 - 2024-12-17(15:56:49 +0000)

### Other

- [USP] No notifications are sent

## Release v5.1.0 - 2024-12-12(10:03:07 +0000)

### Other

- [USP] Boot event must be sent when NTP is synchronized

## Release v5.0.3 - 2024-12-10(08:51:30 +0000)

### Other

- - [USP] No notifications are sent

## Release v5.0.2 - 2024-11-22(10:19:25 +0000)

### Fixes

- [USP] The correct backend ctx must be used

## Release v5.0.1 - 2024-11-18(12:26:24 +0000)

### Other

- [USP] Crash if PeriodicNotifInterval/PeriodicNotifTime changes

## Release v5.0.0 - 2024-11-07(13:59:05 +0000)

## Release v4.6.13 - 2024-10-10(11:56:22 +0000)

### Other

- USP broker socket should be accessible to non-root users

## Release v4.6.12 - 2024-09-30(05:52:29 +0000)

### Other

- CI: Disable squashing of open source commits
- [AMX] Disable commit squashing

## Release v4.6.11 - 2024-09-10(07:52:44 +0000)

### Fixes

- Better shutdown script

## Release v4.6.10 - 2024-07-22(14:36:17 +0000)

### Other

- [AMXB] Introduce depth parameter for subscriptions

## Release v4.6.9 - 2024-07-18(10:29:59 +0000)

### Other

- [TR181-Device]Bidirectional communication support between UBUS and IMTP

## Release v4.6.8 - 2024-06-03(18:20:07 +0000)

## Release v4.6.7 - 2024-05-22(07:14:32 +0000)

### Other

- [Security][ambiorix] Some libraries are not compiled with Fortify-Source

## Release v4.6.6 - 2024-04-10(07:13:12 +0000)

### Changes

- Make amxb timeouts configurable

## Release v4.6.5 - 2024-04-03(09:22:56 +0000)

### Fixes

- [USP] Set or Delete response does not return error codes

## Release v4.6.4 - 2024-03-27(16:35:28 +0000)

### Other

- [USP] Extend documentation

## Release v4.6.3 - 2024-03-18(11:45:12 +0000)

### Fixes

- [USP] PeriodicNotifInterval changes are only applied after reboot

## Release v4.6.2 - 2024-03-14(10:50:36 +0000)

### Fixes

- [CDROUTER][USP] Delete Message - Allow Partial True fails

## Release v4.6.1 - 2024-02-16(13:54:14 +0000)

### Other

- [USP] Extend unit tests

## Release v4.6.0 - 2024-02-15(12:28:16 +0000)

### New

- [USP] Support of OnChange! Event

## Release v4.5.7 - 2024-02-15(10:55:51 +0000)

### Fixes

- MTP status remains down

## Release v4.5.6 - 2024-01-11(14:22:40 +0000)

### Other

- [USP] Add quick start guide to uspagent

## Release v4.5.5 - 2023-11-27(15:54:16 +0000)

### Other

- [LCM] Error code is not forwarded correctly from LCM to USP in case of InstallDU using a non-existent EE

## Release v4.5.4 - 2023-11-09(15:32:32 +0000)

### Changes

- Add with sub-object parameters must be supported

## Release v4.5.3 - 2023-11-07(15:44:20 +0000)

### Fixes

- LocalAgent.MTP.{i}.Status not updated properly

## Release v4.5.2 - 2023-11-07(14:54:50 +0000)

### Changes

- [USP] Move USP backend location

## Release v4.5.1 - 2023-10-25(08:12:46 +0000)

### Fixes

- [USP] Register rollback does too much

## Release v4.5.0 - 2023-09-26(11:41:17 +0000)

### New

- Update USP error codes for registration

## Release v4.4.4 - 2023-09-25(08:42:01 +0000)

### Fixes

- Fix license headers in files

## Release v4.4.3 - 2023-09-25(07:57:58 +0000)

### Fixes

- [USP] onboard request not automatically sent by IB4 when MTP is re-enabled

## Release v4.4.2 - 2023-09-14(15:38:52 +0000)

### Fixes

- Only send BulkData reports to subscribed controller

## Release v4.4.1 - 2023-09-09(06:27:16 +0000)

### Fixes

- Only send BulkData reports to subscribed controller

## Release v4.4.0 - 2023-09-05(14:46:55 +0000)

### New

- [USP] Our uspagent should use the USPServices data model

## Release v4.3.1 - 2023-09-01(09:06:01 +0000)

### Changes

- Trigger Device.Boot! event from uspagent

## Release v4.3.0 - 2023-08-31(14:09:42 +0000)

### New

- Retry onboarding in case it is not confirmed

## Release v4.2.3 - 2023-08-31(13:09:36 +0000)

### Fixes

- [USP] Remove handshake from connect

## Release v4.2.2 - 2023-08-17(13:06:49 +0000)

### Fixes

- [USP] messages are not published sometimes

## Release v4.2.1 - 2023-07-26(08:38:49 +0000)

### Other

- [AMX] Replace ubus-cli in debuginfo script

## Release v4.2.0 - 2023-07-20(12:11:13 +0000)

### New

- [USP] Add config flag to ignore partial

## Release v4.1.0 - 2023-07-20(07:56:58 +0000)

### New

- [USP] GSDM should return whether commands are (a)sync

## Release v4.0.4 - 2023-07-18(11:31:45 +0000)

### Other

- [USP] Bus timeout when fetching Device object on boot

## Release v4.0.3 - 2023-07-14(15:01:00 +0000)

### Other

- [KPN][USP] Boot! event is not as expected

## Release v4.0.2 - 2023-07-14(14:53:13 +0000)

### Fixes

- Prevent segmentation fault after IMTP disconnect

## Release v4.0.1 - 2023-07-14(14:33:23 +0000)

### Fixes

- Retry IMTP connection in case of failure

## Release v4.0.0 - 2023-07-11(13:07:38 +0000)

### Breaking

- [IMTP] Implement IMTP communication as specified in TR-369

## Release v3.13.1 - 2023-06-27(07:50:47 +0000)

### Other

- [CR9HF] - Multiple uspagent sessions running

## Release v3.13.0 - 2023-06-15(09:12:41 +0000)

### New

- [USP] Support Error Code: 7025 'Object exists with duplicate key'

## Release v3.12.1 - 2023-06-14(06:53:56 +0000)

### Changes

- [USP] allow_partial=false must be rejected

## Release v3.12.0 - 2023-06-09(08:33:17 +0000)

### New

- [Security][USP] Add ACLs for get supported dm to USP agent

## Release v3.11.0 - 2023-05-26(07:58:44 +0000)

### New

- [Security][USP] Add ACLs for get instances to USP agent

## Release v3.10.0 - 2023-05-23(08:59:27 +0000)

### New

- [Security][USP] Add ACLs for subscriptions to USP agent

## Release v3.9.1 - 2023-05-11(13:05:50 +0000)

### Fixes

- HGWKPN-2285 [USP][Set Message] Error code not as expected

## Release v3.9.0 - 2023-05-11(10:39:21 +0000)

### New

- [USP] It must be possible to use protected methods

## Release v3.8.2 - 2023-05-10(07:55:42 +0000)

### Fixes

- [USP] Agent stuck doing blocking amxb_resolves

## Release v3.8.1 - 2023-05-08(07:16:31 +0000)

### Fixes

- [USP] Missing index number in ObjectCreation notifications

## Release v3.8.0 - 2023-05-02(11:22:33 +0000)

### New

- [USP] uspagent must be able to broker amx subscriptions

## Release v3.7.5 - 2023-04-24(17:08:09 +0000)

### Fixes

- Improve ACL handling with adds

## Release v3.7.4 - 2023-04-13(12:12:25 +0000)

### Other

- [USP] Extend subscription unit tests

## Release v3.7.3 - 2023-04-13(11:44:29 +0000)

### Fixes

- [USP][Regression] Cannot find IMTP con for enabled controller

## Release v3.7.2 - 2023-04-12(09:58:12 +0000)

### Changes

- [USP] Add requests with search paths will be allowed

## Release v3.7.1 - 2023-04-12(07:53:46 +0000)

### Fixes

- [USP] Disabling second controller breaks IMTP connection for first

## Release v3.7.0 - 2023-04-06(09:59:47 +0000)

### New

- Send ForceReconnect based on AutoReconnect parameter

## Release v3.6.6 - 2023-03-28(09:25:04 +0000)

### Fixes

- Discovery object should not be protected

## Release v3.6.5 - 2023-03-24(09:37:52 +0000)

### Other

- [USP] Documentation should be written for adding subscriptions to containers

## Release v3.6.4 - 2023-03-22(08:28:00 +0000)

### Fixes

- Must be able to forward notifications on IMTP

## Release v3.6.3 - 2023-03-21(09:42:34 +0000)

### Fixes

- [KPN][USP] Boot! event notification is not as expected

## Release v3.6.2 - 2023-03-10(10:33:16 +0000)

### Fixes

- Correctly save last value of parameter subscriptions

## Release v3.6.1 - 2023-03-09(12:02:22 +0000)

### Fixes

- [USP] Dynamically adding LocalAgent.MTP. instances does not work

## Release v3.6.0 - 2023-03-02(08:57:03 +0000)

### New

- [USP][CDROUTER] The NotifExpiration limit is not respected

## Release v3.5.13 - 2023-02-20(08:36:56 +0000)

### Fixes

- [USP] Parameter paths must be fetched with depth=0

## Release v3.5.12 - 2023-02-17(10:16:19 +0000)

### Other

- Add tr181-mqtt/tr181-localagent/uspagent into processmonitor

## Release v3.5.11 - 2023-02-13(11:31:11 +0000)

### Other

- [USP] Update unit tests after recent changes

## Release v3.5.10 - 2023-02-01(09:39:32 +0000)

### Fixes

- Remove doc target from uspagent

## Release v3.5.9 - 2023-01-17(11:42:21 +0000)

### Fixes

- [USP Agent][Amx]USP request returns 'invalid path' all the time

## Release v3.5.8 - 2023-01-11(16:18:43 +0000)

### Changes

- [KPN][USP] max_depth has no effect on the Get Message

## Release v3.5.7 - 2023-01-09(11:06:05 +0000)

### Fixes

- [USP] Get requests with valid search expressions must return successful

## Release v3.5.6 - 2023-01-09(10:09:27 +0000)

### Fixes

- [Bulkdata][USP] Controller parameter must be set

## Release v3.5.5 - 2023-01-09(09:07:57 +0000)

### Fixes

- LocalAgent.MTP.i.Status must be Up if MQTT client is connected

## Release v3.5.4 - 2023-01-06(13:11:27 +0000)

### Fixes

- [uspagent] dm:OperationComplete created too late

## Release v3.5.3 - 2022-12-07(08:55:07 +0000)

### Fixes

- [USP] Allow invoking commands without braces

## Release v3.5.2 - 2022-11-25(07:52:16 +0000)

## Release v3.5.1 - 2022-11-24(09:34:39 +0000)

### Fixes

- [AMX] Apply new amxd_path_setf formatting

## Release v3.5.0 - 2022-11-22(16:05:11 +0000)

### New

- [Security][USP] Add ACLs for operate to USP agent

## Release v3.4.6 - 2022-11-17(08:51:56 +0000)

### Fixes

- USP agent requires LocalAgent data model

## Release v3.4.5 - 2022-11-15(12:54:18 +0000)

### Fixes

- [USP][tr181-localagent]Local Agent MTP status is down even if connected and subscribed

## Release v3.4.4 - 2022-11-10(12:02:47 +0000)

### Fixes

- No response must be send when send_resp is false

## Release v3.4.3 - 2022-10-26(13:54:51 +0000)

### Changes

- [ACS][V12] Setting of VOIP in a single SET does not enable VoiceProfile

## Release v3.4.2 - 2022-10-26(06:54:58 +0000)

### Changes

- uspagent must indicate it is interested in USP messages

## Release v3.4.1 - 2022-10-20(08:24:34 +0000)

### Changes

- [ACS][V12] Setting of VOIP in a single SET does not enable VoiceProfile

## Release v3.4.0 - 2022-10-11(15:14:32 +0000)

### New

- [USPAgent][MQTT Client] It must be possible to share a single MQTT Client with multiple USP Controller instances

## Release v3.3.2 - 2022-10-10(10:06:32 +0000)

### Fixes

- Strip Device. prefix from AssignedRole

## Release v3.3.1 - 2022-10-10(06:23:25 +0000)

### Fixes

- [USP] IMTP Status parameter is Down when it should be Up

## Release v3.3.0 - 2022-09-29(14:25:12 +0000)

### New

- [USP] IMTP should use LocalAgent.Subscription for subscriptions

### Fixes

- [USP] uspagent does not respond correctly with EndpointID

## Release v3.2.0 - 2022-09-15(13:34:45 +0000)

### New

- MQTT ClientID must have the same value as LocalAgent.EndpointID

## Release v3.1.2 - 2022-09-15(07:42:07 +0000)

### Other

- [USP] LIBDIR should be STAGING_LIBDIR to avoid conflicts

## Release v3.1.1 - 2022-09-13(14:40:21 +0000)

### Fixes

- [USP] [Add_Msg] The AddResp does not contain the elements in the unique keymap under the OperationSuccess

## Release v3.1.0 - 2022-09-12(11:49:30 +0000)

### New

- [USP] Unit tests need to be added for the MTP of type IMTP

## Release v3.0.2 - 2022-09-12(11:20:36 +0000)

### Fixes

- [USP][CDROUTER] Agent does not reject messages that do not contain their to_id in the USP record

## Release v3.0.1 - 2022-09-07(13:08:35 +0000)

### Fixes

- [USP] Latest version of USP breaks MQTT communication

## Release v3.0.0 - 2022-09-06(07:20:22 +0000)

### New

- Add MTP of type IMTP

## Release v2.4.1 - 2022-08-25(10:29:24 +0000)

### Fixes

- [ACS][V12] ValueChange Subscriptions only sent for last parameter in ReferenceList

## Release v2.4.0 - 2022-08-25(08:21:40 +0000)

### New

- [USP] The Periodic! event should be ported from previous code base

## Release v2.3.1 - 2022-08-24(09:22:20 +0000)

### Fixes

- Slots can be triggered when subscription was already removed

## Release v2.3.0 - 2022-08-03(11:18:45 +0000)

### New

- Value change on Device.DeviceInfo.SoftwareVersion after firmware upgrade

## Release v2.2.11 - 2022-07-22(06:52:24 +0000)

### Fixes

- [USP] ACL files cannot be retrieved when role paths don't end with a dot

## Release v2.2.10 - 2022-07-14(14:16:26 +0000)

### Changes

- [USP] A dot must be added to reference paths

## Release v2.2.9 - 2022-07-14(11:34:21 +0000)

### Changes

- [USP] Make forwarding of output arguments in a USP message more generic

## Release v2.2.8 - 2022-07-08(07:04:26 +0000)

### Changes

- [USP] It must be possible to set string parameters starting with a plus

## Release v2.2.7 - 2022-06-24(07:28:41 +0000)

### Fixes

- [USP] tr181-mqtt function CreateListenSocket should be called on MQTT object

## Release v2.2.6 - 2022-06-10(07:51:07 +0000)

### Fixes

- [USP][CDROUTER] The Agent responds with an invalid supported protocol to the controller

## Release v2.2.5 - 2022-06-07(15:38:14 +0000)

### Fixes

- [USP] MQTT.Client.{i}.ForceReconnect() can time out

### Changes

- Add CommandKey and Requestor to args of async operations

## Release v2.2.4 - 2022-06-03(07:58:01 +0000)

### Fixes

- Missing function to handle get_instances

## Release v2.2.3 - 2022-06-02(12:44:22 +0000)

### Other

- [packages][debian] Correct makefile

## Release v2.2.2 - 2022-06-02(08:52:28 +0000)

### Other

- The USP Agent must be opensourced to gitlab.com/soft.at.home

## Release v2.2.1 - 2022-05-19(12:39:21 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v2.2.0 - 2022-05-19(11:26:50 +0000)

### New

- [USP Agent] Implement GetInstances message

## Release v2.1.4 - 2022-05-10(06:08:22 +0000)

### Fixes

- [USP] No response from USP agent

## Release v2.1.3 - 2022-05-03(06:07:14 +0000)

### Fixes

- [USP] USP commands with braces are invoked synchronously

## Release v2.1.2 - 2022-04-25(11:03:38 +0000)

### Fixes

- [USP] USP notifications contain swapped EndpointIDs

## Release v2.1.1 - 2022-04-21(13:21:55 +0000)

### Fixes

- [USP] Device prefix prevents finding the IMTP context

## Release v2.1.0 - 2022-04-05(11:40:42 +0000)

### New

- [Device] Support Sending Boot! event

## Release v2.0.0 - 2022-03-28(15:09:41 +0000)

### Breaking

- [USPAgent] Split agent in datamodel part and function part

## Release v1.3.2 - 2022-03-24(09:25:28 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v1.3.1 - 2022-03-15(11:59:49 +0000)

### Fixes

- [ACL][USP] ACL files must be located in writable directory
- [USP] [Set_msg] No response from the agent after sending a Set message

## Release v1.3.0 - 2022-02-14(14:32:37 +0000)

### New

- Handle allow partial set

### Fixes

- Cannot create USP Error messages with parameter errors

### Other

- [USPAgent] The USP Agent must be opensourced to gitlab.com/soft.at.home

## Release v1.2.0 - 2022-01-27(18:10:10 +0000)

### New

- It must be possible to generate a unique endpoind ID for a USP endpoint

### Other

- Add extra logging after acl verification

## Release v1.1.3 - 2022-01-11(10:54:05 +0000)

### Other

- Error with notifications when installing 2 applications in the same time

## Release v1.1.2 - 2021-12-20(09:34:35 +0000)

### Other

- [USP Agent] Add unittest for on board request

## Release v1.1.1 - 2021-12-20(09:16:04 +0000)

### Other

- [USP] Agent must publish responses on controller's reply-to topic

## Release v1.1.0 - 2021-12-16(10:38:16 +0000)

### New

- Implement OnBoardRequest

## Release v1.0.12 - 2021-12-15(12:14:25 +0000)

### Fixes

- [USP] Missing notification after async operation

## Release v1.0.11 - 2021-12-13(11:57:41 +0000)

### Changes

- [USP] Agent must publish responses on controller's reply-to topic

## Release v1.0.10 - 2021-12-08(08:56:10 +0000)

### Fixes

- [USP Agent] USP messages on sop should be send over pcb sysbus with Device prefix still present

## Release v1.0.9 - 2021-12-06(16:04:57 +0000)

### Other

- Issue: amx/usp/applications/uspagent#41 [DOC] Ambiorix events must contain a separate htable for parameters

## Release v1.0.8 - 2021-12-06(08:20:00 +0000)

### Other

- [USPAgent] Support Controller.BootParameter.{i}. feature

## Release v1.0.7 - 2021-11-30(12:01:15 +0000)

### Other

- [USP Agent] USP messages on sop should be send over pcb sysbus with Device prefix still present

## Release v1.0.6 - 2021-11-26(14:31:47 +0000)

### Fixes

- [USP] Incorrect subscription filter for Events

## Release v1.0.5 - 2021-11-25(10:51:14 +0000)

### Fixes

- [USP] The correct bus context must be used for operations

## Release v1.0.4 - 2021-11-22(12:05:27 +0000)

### Fixes

- Get supported protocol message should be handled on requests instead of responses

## Release v1.0.3 - 2021-11-19(16:45:07 +0000)

## Release v1.0.2 - 2021-11-19(16:33:00 +0000)

### Other

- [USP Agent] Retrieve correct mtp information when a notify retry happens

## Release v1.0.1 - 2021-11-19(16:21:54 +0000)

### Other

- Change startup order

## Release v1.0.0 - 2021-11-15(10:11:58 +0000)

### Fixes

- Avoid segmentation fault on exit

### Other

- Update documentation
- Handle subscriptions of objects under Device.

## Release 0.6.5 - 2021-10-19(08:00:02 +0000)

### New

- [USP agent] Need to implement the TR181 custom events
- [USP] Bus access from public requests should be restricted [new]

### Fixes

- [USP Agent] Fill in the Recipient parameter automatically when a subscription is created


## Release v0.6.4 - 2021-10-12(14:07:50 +0000)

### Other

- [USP][ACL] Operator must have access while mapper is missing
- [USP][ACL] Operator must have access while mapper is missing

## Release v0.6.3 - 2021-10-12(11:17:31 +0000)

### Fixes

- Add unit tests to uspagent

## Release v0.6.2 - 2021-10-11(14:52:53 +0000)

### Fixes

- [AMX][USP] Prevent double initialization in uspagent

## Release v0.6.1 - 2021-10-08(06:37:32 +0000)

### New

- [ACL] Add default ACL configuration per services

### Breaking

- [USP][ACL] Add access control verification to USP agent

### Changed

- [odl defaults] [USPAgent] [MQTT Client] Support directory of default odl files

### Other

- [USP] Add README and configuration guidelines for USP agent

## Release v0.6.0 - 2021-09-20(06:34:44 +0000)

### New

- Issue:  NET-2985 [USPAgent][Device] Handle Device.LocalAgent USP messages

### Fixes

- [USP] USP agent should use first bus context

## Release v0.5.0 - 2021-09-16(13:54:47 +0000)

### New

- [USPAgent] Implementation of the ControllerTrust featureDev controllertrust

## Release v0.4.0 - 2021-09-16(07:17:36 +0000)

### New

- [USP] Configure defaults for communicating with orange controller

### Fixes

- [USP] The USP Agent and MQTT client should have the USP backend as run time dependency

## Release v0.3.4 - 2021-09-09(12:07:58 +0000)

### Fixes

- [AMX] Debian packages for ambiorix plugins should create symlinks to amxrt

## Release v0.3.3 - 2021-08-31(06:09:25 +0000)

### Fixes

- [USP] LocalAgent Reference parameters should use the same action callbacks

## Release v0.3.2 - 2021-08-30(08:14:55 +0000)

### Other

- [USP Agent] MTP can not be retrieved when sending subscription notify
- [USP Agent]Making a change watched by a removed subscription results in a crash

## Release v0.3.1 - 2021-08-24(12:14:49 +0000)

### Other

- USP components should have debian packages

## Release v0.3.0 - 2021-08-23(12:25:33 +0000)

### New

- [USP][CDROUTER] Implement Get Supported Protocol response in USP agent
- [USP][CDROUTER] USP Agent should provide a reply-to topic in MQTT messages

### Fixes

- Change component type from plugin to application

## Release v0.2.2 - 2021-07-12(12:22:51 +0000)

### Added

- [USP][CDROUTER] USP MQTT Publish messages includes invalid Content Type property (empty)

