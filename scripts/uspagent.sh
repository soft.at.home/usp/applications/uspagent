#!/bin/sh

. /usr/lib/amx/scripts/amx_init_functions.sh

prepare(){
    if [ ! -d /var/run/imtp ]; then
        mkdir -p /var/run/imtp/
    fi
    if [ ! -d /var/run/mqtt ]; then
        mkdir -p /var/run/mqtt/
    fi
    if [ ! -d /etc/config/uspagent ]; then
        mkdir -p /etc/config/uspagent/odl
    fi
}

check_if_already_running(){
    if [ -s /var/run/uspagent.pid ] && [ -d "/proc/$(cat /var/run/uspagent.pid)/fdinfo" ]; then
        echo "uspagent is already started"
        exit 0
    fi
}

name="uspagent"
datamodel_root="Discovery"

case $1 in
    boot)
        check_if_already_running
        prepare
        process_boot ${name} -D
        ;;
    start)
        check_if_already_running
        prepare
        process_start ${name} -D
        ;;
    stop)
        process_stop ${name}
        ;;
    shutdown)
        process_shutdown ${name}
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    fail)
        /etc/init.d/tr181-localagent stop
        /etc/init.d/tr181-mqtt stop
        /etc/init.d/tr181-mqtt start
        /etc/init.d/tr181-localagent start
        /etc/init.d/uspagent start
        ;;
    debuginfo)
        process_debug_info ${datamodel_root}
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|debuginfo|restart]"
        ;;
esac
